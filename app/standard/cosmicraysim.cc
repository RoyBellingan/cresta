
#include "G4Headers.hh"
#include "ROOTHeaders.hh"
#include "CorePackage.hh"
#include "DBPackage.hh"
#include "SystemHeaders.hh"
#include "ConstructionPackage.hh"
#include "utils/path.hh"

#include <filesystem>

using namespace std;
using namespace CRESTA;


// Run Inputs
std::string gRunTag = "cosmicoutput";
int gRunID = 0;
int gSubRunID = 0;

long int gSeed = -1; ///< Seed. -1 means generate from time+pid

/// Mode : Interactive
bool gInteractive = false; //< Run interactive viewer

// Mode : generate N events
int gNEvents = -1; ///< Number of events to generate regardless of trigger

// Mode : generate N triggers
int gNTriggers = -1; ///< Number of triggered events to generate

// Mode : Generate t exposure
int gExposureTime = -1; ///< Number of seconds exposure to generate

int gProcessingChunks = 100000; ///< By default what the chunk size should be

// User Defined Geometry files
std::vector<std::string> gGeomtryFiles;

std::vector<std::string> gParameterOverrides;
std::vector<std::string> gParameterValues;

///-------------------------------------------------------------------------------------------
void PrintHelpScreen() {
  std::cout << "USAGE" << std::endl << std::endl;

  std::cout << " -n nevents : Events to generate" << std::endl;
  std::cout << " -j ntriggs : Triggers to generate" << std::endl;
  std::cout << " -t exposur : Exposure to generate" << std::endl;
  std::cout << " -c chunksz : Event chunk size for exposure/trigger mode" << std::endl;
  std::cout << " -s seed    : Seed. Default is -1, meaning get from time+pid" << std::endl;
  std::cout << " -o outtag  : Output File Tag. Will create file : outtag.run.subrun.root " << std::endl;
  std::cout << " -i         : Flag. Run in interactive mode." << std::endl;
  std::cout << " -g geofile : Load a geometry JSON file. Can use multiple times." << std::endl;
  std::cout << " --run    r : Set Run ID Manually" << std::endl;
  std::cout << " --subrun r : Set Sub Run ID Manually" << std::endl << std::endl;

  exit(0);

}


///-------------------------------------------------------------------------------------------
int main(int argc, char** argv) {

  curExecutablePath(argv[0]);

  // Print Splash Screen
  DB::PrintSplashScreen();
  G4RunManager* runManager = new G4RunManager;

  // Get User Inputs
  std::cout << "************************************************************** " << std::endl;
  std::cout << "APP: Getting User Inputs" << std::endl;

  // Loop over all arguments
  for (int i = 1; i < argc; ++i) {

    // N Events input " -n nevents"
    if (std::strcmp(argv[i], "-n") == 0) {
      gNEvents = std::stoi(argv[++i]);

      // N Triggers Input " -j ntriggers"
    } else if (std::strcmp(argv[i], "-j") == 0) {
      gNTriggers = std::stoi(argv[++i]);

      // Exposure Time Input " -t exposure"
    } else if (std::strcmp(argv[i], "-t") == 0) {
      gExposureTime = std::stoi(argv[++i]);

      // Chunk Input " -c chunksize"
    } else if (std::strcmp(argv[i], "-c") == 0) {
      gProcessingChunks = std::stoi(argv[++i]);

      // Seed Input " -s seedval"
    } else if (std::strcmp(argv[i], "-s") == 0) {
      gSeed = std::stol(argv[++i]);

      // Seed Input " -s seedval"
    } else if (std::strcmp(argv[i], "--run") == 0) {
      gRunID = std::stoi(argv[++i]);

      // Seed Input " -s seedval"
    } else if (std::strcmp(argv[i], "--subrun") == 0) {
      gSubRunID = std::stoi(argv[++i]);

      // Run Tag " -s outputtag"
    } else if (std::strcmp(argv[i], "-o") == 0) {
      gRunTag = std::string(argv[++i]);

      // Interactive Mode Flag "-i"
    } else if (std::strcmp(argv[i], "-i") == 0) {
      gInteractive = true;


    } else if (std::strcmp(argv[i], "-p") == 0){


      std::string strinput = std::string(argv[++i]);
      size_t pos = strinput.rfind("=");
      if (pos == std::string::npos){
	std::cout << "Parameter override format ' -p PARAMETER=VALUE '" << std::endl;
	throw;
      }
      std::string strfield = strinput.substr(0, pos);
      std::string strval = strinput.substr(pos+1,strinput.size());

      gParameterOverrides.push_back(strfield);
      gParameterValues.push_back(strval);



      // Geometry input " -g geofile1 -g geofile2 "
    } else if (std::strcmp(argv[i], "-g") == 0) {

      std::vector<std::string> geoms = DataBase::Expand(std::string(argv[++i]));

      for (uint j = 0; j < geoms.size(); j++) {
        gGeomtryFiles.push_back(geoms[j]);

        // We need to check that the file exists
        ifstream f(gGeomtryFiles.back());
        if(!f.good()){
          std::cout << " Cannot file geometry file : " << gGeomtryFiles.back() << std::endl;
          throw;
        }

      }

      // We need at least one geometry file
      if(gGeomtryFiles.size()==0){
            std::cout << std::endl << "You must include at least one geometry file! " << std::endl << std::endl;
            PrintHelpScreen();
            throw;
      }



      // Print Splash Screen
    } else if (std::strcmp(argv[i], "-h") == 0) {
      PrintHelpScreen();


      // Uknown command
    } else {
      std::cout << "Unknown COMMAND : " << argv[i] << std::endl;
      throw;
    }
  }


// Setup Seed
  std::cout << "************************************************************** " << std::endl;
  std::cout << "APP: Getting Seed Info " << std::endl;

// Choose the Random engine
  CLHEP::RanecuEngine* rand = new CLHEP::RanecuEngine;
  if (gSeed < 0) {
    std::cout << "APP: --> Getting seed from time+pid." << std::endl;
    long tl = std::time(0);
    long pid = getpid();
    std::cout << "APP: --> pid : " << pid << std::endl;
    std::cout << "APP: --> time : " << tl << std::endl;
    gSeed = int( tl + (pid + 1.));
    std::cout << "APP: --> seed : " << gSeed << std::endl;
  } else {
    std::cout << "APP: --> User specified seed. " << std::endl;
    std::cout << "APP: --> seed = " << gSeed << std::endl;
  }

  rand->setSeed(gSeed);
  G4Random::setTheEngine(rand);


// Setup the Database
  std::cout << "************************************************************** " << std::endl;
  std::cout << "APP: Loading Default Database " << std::endl;
  DB *rdb = DB::Get();
  rdb->Load(DB::GetDataPath());
  if (gGeomtryFiles.size() > 0) {
    for (uint i = 0; i < gGeomtryFiles.size(); i++) {
      std::cout << "APP: Loading Geometry file : " << gGeomtryFiles[i] << std::endl;
      rdb->LoadFile(gGeomtryFiles[i]);
    }
  }
  rdb->Finalise();

  std::cout << "APP: Loading database overrides" << std::endl;
  for (size_t i = 0; i < gParameterOverrides.size(); i++){
    rdb->AddGlobalParameter(gParameterOverrides[i], gParameterValues[i]);
  }

  std::cout << "************************************************************** " << std::endl;
  std::cout << "APP: Loaded Global Parameters " << std::endl;
  DBEvaluator evals;
  evals.PrintConstants();

  try {
    std::cout << "************************************************************** " << std::endl;
    std::cout << "APP: Setting up Geant4 Managers \n" << std::endl;

    std::cout << "************************************************************** " << std::endl;
    std::cout << "APP: Setting up WorldConstruction " << std::endl;
    runManager->SetUserInitialization(WorldConstruction::Build());

    std::cout << "************************************************************** " << std::endl;
    std::cout << "APP: Setting up Physics " << std::endl;
    runManager->SetUserInitialization(PhysicsConstruction::Build());

    std::cout << "************************************************************** " << std::endl;
    std::cout << "APP: Setting up ActionInitialization " << std::endl;
    runManager->SetUserInitialization(ActionConstruction::Build());
  }  catch (std::exception& e) {
    std::cerr << e.what();
	//std::cerr <<
	exit(2);
  }
  // Initialize G4 kernel
  runManager->Initialize();

#ifdef G4VIS_USE
  // Initialize visualization
  G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();
#endif

// Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  std::cout << "************************************************************** " << std::endl;
  std::cout << "APP: Assigning Output Information" << std::endl;
  std::cout << "APP: --> Output Tag       : " << gRunTag   << std::endl;
  std::cout << "APP: --> Output Run ID    : " << gRunID    << std::endl;
  std::cout << "APP: --> Sub Run Start ID : " << gSubRunID << std::endl;
  Analysis::Get()->SetOutputTag(gRunTag);
  Analysis::Get()->SetOutputRun(gRunID);
  Analysis::Get()->SetOutputSubRun(gSubRunID);
  Analysis::Get()->MakeRunFolder();

  

// ----------------------------------------------------------------------
// INTERACTIVE MODE
// ----------------------------------------------------------------------
  if (gInteractive) {

    std::cout << "************************************************************** " << std::endl;
    std::cout << "APP: Running Interactive Mode" << std::endl;
    Analysis::Get()->SetInteractive(true);

#ifdef G4UI_USE
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);

#ifdef G4VIS_USE
    UImanager->ApplyCommand("/control/execute " + DB::GetDataPath() + "/init_vis.mac");
#else
    UImanager->ApplyCommand("/control/execute " + DB::GetDataPath() + "/init.mac");
#endif
    // start the session here: make the Geant4 prompt Idle>
    // available to the user
    ui->SessionStart();
    delete ui;
#endif
#ifdef G4VIS_USE
    delete visManager;
#endif


    // ----------------------------------------------------------------------
    // Batch Processing
    // ----------------------------------------------------------------------
  } else {

    std::cout << "************************************************************** " << std::endl;
    std::cout << "APP: Running Batch Mode" << std::endl;
    Analysis::Get()->InitialSetup();

    // Run initial configuration commands
    DBTable tbl = rdb->GetTable("GLOBAL", "config");
    if (tbl.Has("batchcommands")) {

      // Get users pre macro file
      std::string commands = tbl.GetS("batchcommands");

      // Execute the macro
      if (!commands.empty()) {
        G4String command = "/control/execute ";
        UImanager->ApplyCommand(command + commands);
      }
    }


    // *********************************
    // N Generated mode
    // *********************************
    if (gNEvents > 0 && gNTriggers < 0 && gExposureTime < 0) {
      std::cout << "************************************************************** " << std::endl;
      std::cout << "APP: Running Generator" << std::endl;
      std::cout << "APP: --> NEvents : " << gNEvents << std::endl;


      Analysis::Get()->SetMode(Analysis::kEventMode);
      Analysis::Get()->SetRequiredEvents(gNEvents);
      std::cout << "APP: Starting Run : " << gNEvents << std::endl;
      UImanager->ApplyCommand("/run/beamOn " + std::to_string(gNEvents));

      // *********************************
      // N Triggers mode
      // *********************************
    } else if (gNEvents < 0 && gNTriggers > 0 && gExposureTime < 0) {

      std::cout << "************************************************************** " << std::endl;
      std::cout << "APP: Running NTriggers" << std::endl;
      std::cout << "APP: --> Desired Triggers : " << gNTriggers << std::endl;
      std::cout << "APP: --> Processing Chunk : " << gProcessingChunks << std::endl;

      // Set Run Details
      Analysis::Get()->SetMode(Analysis::kTriggerMode);
      Analysis::Get()->SetRequiredTriggers(gNTriggers);
      Analysis::Get()->SetChunkSize(gProcessingChunks);

      // Start while loop
      int exptriggers = 0;
      while (exptriggers < gNTriggers){

        // Run a single chunk
        UImanager->ApplyCommand("/run/beamOn " + std::to_string(gProcessingChunks));

        // Update Triggers
        exptriggers = Analysis::Get()->GetNSavedEvents();
      }

      std::cout << "Acquired above desired triggers : " << exptriggers << std::endl;

      // *********************************
      // Exposure Time Mode
      // *********************************
    } else if (gNEvents < 0 && gNTriggers < 0 && gExposureTime > 0) {

      std::cout << "************************************************************** " << std::endl;
      std::cout << "APP: Running Exposure" << std::endl;
      std::cout << "APP: --> Desired Exposure : " << gExposureTime << " s" << std::endl;
      std::cout << "APP: --> Processing Chunk : " << gProcessingChunks << std::endl;

      // Set run details
      Analysis::Get()->SetMode(Analysis::kTimeExposureMode);
      Analysis::Get()->SetRequiredExposure(gExposureTime);
      Analysis::Get()->SetChunkSize(gProcessingChunks);

      // Get the total rate
      double singleexp = 0;
      while (singleexp < gExposureTime) {

        // Save beam
        UImanager->ApplyCommand("/run/beamOn " + std::to_string(gProcessingChunks));

        // Get new exposure time
        singleexp = Analysis::Get()->GetExposureTime();
      }

      // *********************************
      // User Error
      // *********************************
    }  else {

      std::cout << "Can only run one of the nevents/ntriggers/exposure at once in batch mode!" << std::endl;
      std::cout << "NEvents   : -n nevents" << std::endl;
      std::cout << "NTriggers : -l ntriggers" << std::endl;
      std::cout << "Exposure  : -t exposure" << std::endl;
      throw;
    }
  }

  std::cout << "************************************************************** " << std::endl;
  std::cout << "APP: Cleaning Up!" << std::endl;

// Job termination
// Free the store: user actions, physics_list and detector_description are
// owned and deleted by the run manager, so they should not be deleted
// in the main() program !
  delete runManager;

  std::cout << "************************************************************** " << std::endl;


  return 0;
}
