#pragma once
#include <filesystem>

/**
 * @brief curExecutablePath will return if set (else will exit with a warning) the path
 * @param path a ptr to the path you want to be returned
 * @return
 */
std::filesystem::path          curExecutablePath(const char* path = nullptr);

__attribute__((noreturn)) void printMissingEviromentError(std::string msg);
