#include "path.hh"
#include <source_location>

using namespace std;

std::filesystem::path curExecutablePath(const char* path) {
	static std::filesystem::path set;
	if (path) {
		auto fs = std::filesystem::path(path);
		//will remove /../ or //
		set = std::filesystem::weakly_canonical(fs);
	}
	if (set.empty()) {
		//Path never got set in curExecutablePath !
		exit(2);
	}
	return set;
}

void replace(string& original, const string& search, const string& replace) {
	original.replace(original.find(search), search.size(), replace);
}

void printMissingEviromentError(std::string msg) {
	//we know the level of backrecurse needed
	std::string location      = std::filesystem::path(std::source_location::current().file_name()).parent_path().parent_path().parent_path();
	std::string projectBinDir = curExecutablePath().parent_path().parent_path();
	//to make code more portable we will try to replace the user home with ~
	if (auto h = getenv("HOME"); h) {
		replace(location, h, "~");
		replace(projectBinDir, h, "~");
	}

	string boldRed   = "\033[1;31m";
	string resetType = "\033[0m";

	std::string skel = boldRed + R"(

********************

%s

********************
WARNING! are you sure to have the following enviroment variables set ? 
You normally would put those in the
~/.bashrc
file of the user launching the program

Where
@PROJECT_BINARY_DIR@ is the path where the executable (this file) is present
@PROJECT_SOURCE_DIR@ is the source folder


export CRESTA="@PROJECT_BINARY_DIR@/"
export CRESTA_LIB_DIR="@PROJECT_BINARY_DIR@/lib/"
export CRESTA_INC_DIR="@PROJECT_BINARY_DIR@/include/"
export CRESTA_DATA_PATH="@PROJECT_SOURCE_DIR@/data/"
export CRESTA_SIMULATION_PATH="@PROJECT_SOURCE_DIR@/simulations/"
export PATH="$CRESTA/bin/:$PATH"
export LD_LIBRARY_PATH="$CRESTA/lib/:@CADMESH_DIR@/lib/:$LD_LIBRARY_PATH"
export CRESTA_OBJECTS_PATH="$CRESTA:$CRESTA_OBJECTS_PATH"


Which composed with the current enviroment should be more or less be like that

export CRESTA=%s/
export CRESTA_LIB_DIR=%s/lib/
export CRESTA_INC_DIR=%s/include/
export CRESTA_DATA_PATH=%s/data/
export CRESTA_SIMULATION_PATH=%s/simulations/
export PATH="$CRESTA/bin/:$PATH"
export LD_LIBRARY_PATH="$CRESTA/lib/:@CADMESH_DIR@/lib/:$LD_LIBRARY_PATH"
export CRESTA_OBJECTS_PATH="$CRESTA:$CRESTA_OBJECTS_PATH"

Program will terminate as is impossible to continue
)" + resetType;

	printf(skel.c_str(), msg.c_str(), projectBinDir.c_str(), projectBinDir.c_str(), projectBinDir.c_str(), location.c_str(), location.c_str());

	exit(2);
}
