#ifndef __DYNAMIC_PROCESSOR_FACTORY_HH__
#define __DYNAMIC_PROCESSOR_FACTORY_HH__
#include <dirent.h>
#include <dlfcn.h>
#include <iostream>

#include "DBPackage.hh"
#include "ROOTHeaders.hh"
#include "G4Headers.hh"

#include "VProcessor.hh"
#include "VDetector.hh"
#include "VGeometry.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Core { // CRESTA Core Sub Namespace

/// Detector Factory used to create SD from tables
class DynamicObjectLoader {
public:

	// Main Constructors/Destructors for Singleton
	DynamicObjectLoader();
	~DynamicObjectLoader();
	static DynamicObjectLoader* glblDSF;
	static DynamicObjectLoader* Get();

	// Available functions that can be found in the working area
	typedef std::string (*CRESTA_PrintManifest_ptr)(void);
	typedef VProcessor* (*CRESTA_ConstructProcessor_ptr)(DBTable*);
	typedef VDetector* (*CRESTA_ConstructDetector_ptr)(DBTable*);
	typedef VGeometry* (*CRESTA_ConstructGeometry_ptr)(DBTable*);
	typedef G4VUserPrimaryGeneratorAction* (*CRESTA_LoadFluxGenerator_ptr)(std::string);

	// A plugin manifest for available dynamic pointers
	struct PluginManifest {
		void* dllib;
		std::string soloc;

		CRESTA_PrintManifest_ptr CRESTA_PrintManifest;
		CRESTA_ConstructProcessor_ptr CRESTA_ConstructProcessor;
		CRESTA_ConstructDetector_ptr CRESTA_ConstructDetector;
		CRESTA_ConstructGeometry_ptr CRESTA_ConstructGeometry;
		CRESTA_LoadFluxGenerator_ptr CRESTA_LoadFluxGenerator;
		PluginManifest();
		~PluginManifest();
	};

	std::vector<PluginManifest> Manifests;
	std::vector<std::string> fLoadedManifests;
	size_t NObjects;
	size_t NManifests;

	void LoadPlugins();
	void LoadPlugins_OSX();
	void LoadPlugins_LINUX();

	VProcessor* ConstructDynamicProcessor(DBTable tbl);
	VDetector* ConstructDynamicDetector(DBTable tbl);

	G4VUserPrimaryGeneratorAction* LoadDynamicFluxGenerator(std::string type);

	VGeometry* ConstructDynamicGeometry(DBTable tbl);

}; // - class DYNAMIC PROCESSOR FACTORY
}; // - namespace Core
}; // - namespace CRESTA
#endif
