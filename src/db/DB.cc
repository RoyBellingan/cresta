// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with NUISANCE.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "DB.hh"
#include "utils/path.hh"
#include "filesystem"

namespace CRESTA { // Main CRESTA Namespace
namespace DataBase { // CRESTA Database Sub Namespace
// ---------------------------------------------------------------------------

void DB::PrintSplashScreen() {

  std::cout << "**************************************************************" << std::endl;
  std::cout << "                            %@@@                              " << std::endl;
  std::cout << "                          @@@ %.@@&                           " << std::endl;
  std::cout << "                        &@@,(@@@@@ @@@                        " << std::endl;
  std::cout << "                     @@@ @@@    *@@& @@&                      " << std::endl;
  std::cout << "                  &@@.#@@*         @@@ &@@.                   " << std::endl;
  std::cout << "                @@@ @@@              ,@@& @@&                 " << std::endl;
  std::cout << "             @@@.%@@,                   @@@ &@@.              " << std::endl;
  std::cout << "           @@@ @@@       @@@@@@@@@@@@@@   .@@& @@@            " << std::endl;
  std::cout << "         @@@.%@@,      (@@@@@@(     *@@       @@@ %@@,        " << std::endl;
  std::cout << "     .@@@ @@@        @@@@@&                     @@@ @@@       " << std::endl;
  std::cout << "    ,@@@@@.         .@@@@&                        &@@@@@@     " << std::endl;
  std::cout << "    @@@@@@          ,@@@@&    @@@@@@@@@           #@@@@@      " << std::endl;
  std::cout << "     (@@(.@@%       .@@@@@        #@@@@         @@@ @@@       " << std::endl;
  std::cout << "        @@@ @@@      %@@@@@       #@@@@      %@@*/@@/         " << std::endl;
  std::cout << "          *@@%.@@&    .@@@@@@@@@@@@@@@@    @@@ @@@            " << std::endl;
  std::cout << "              @@@ @@@     ,@@@@@@@@@@@@, &@@,#@@/              " << std::endl;
  std::cout << "               ,@@&.@@&               @@@ @@@                 " << std::endl;
  std::cout << "                  @@@ @@@.         &@@.#@@*                   " << std::endl;
  std::cout << "                    ,@@& @@&     @@@ @@@                      " << std::endl;
  std::cout << "                       @@@ &@@@@@.%@@*                        " << std::endl;
  std::cout << "                         .@@@ @ @@@                           " << std::endl;
  std::cout << "                            @@@@,                             " << std::endl;
  std::cout << "**************************************************************" << std::endl;
  std::cout << "*   CRESTA : GEOPTIC Cosmic Rays Simulation Package          *" << std::endl;
  std::cout << "*                                                            *" << std::endl;
  std::cout << "*   Chris Steer, Patrick Stowell, Lee Thompson               *" << std::endl;
  std::cout << "*   contact : p.stowell@sheffield.ac.uk                      *" << std::endl;
  std::cout << "**************************************************************" << std::endl;
}

DB::DB()
{
  CreateDataBase("default");
  SelectDataBase("default");
  fDefaultTables = &(fAllTables["default"]);
}

DB* DB::Get() {
  return fPrimary == 0 ? fPrimary = new DB() : fPrimary; };

DB::~DB()
{
}

void DB::CreateDataBase(std::string dataid)
{
  std::cout << "DB : Creating Database : " << dataid << std::endl;
  fAllTables[dataid] = std::vector<DBTable>();
}

void DB::SelectDataBase(std::string dataid)
{
  std::cout << "DB : Selecting Database : " << dataid << std::endl;
  fCurrentTables = &(fAllTables[dataid]);
}

int DB::Load(std::string filename)
{
  if(!std::filesystem::exists(filename)){
    std::runtime_error e("The path " + filename + " does not exists!");
    throw e;
  }
  // Try to get file info assuming the name is literal
  struct stat s;
  stat(filename.c_str(), &s);
  std::string f = filename;
  std::string suf = ".db";

  if (S_ISDIR(s.st_mode)) {
    return LoadAll(filename);
  } else if (f.rfind(suf) == (f.size() - suf.size())) {
    return LoadFile(filename);
  } else {

    return 1;
  }

  std::cout << "Cannot Load Data Tables : " << filename << std::endl;
  throw;
  return 0;
}

int DB::LoadFile(std::string filename)
{
  std::cout << "DB : Loading '" << filename << "'' ... ";
  // Try to read and parse it as databse or JSON file.
  std::vector<DBTable> contents = DBJSONParser::parsevals(filename);

  std::vector<DBTable>::iterator itbl;
  for (itbl = contents.begin(); itbl != contents.end(); itbl++) {
    DBTable table = (*itbl);
    AddTable(table);
    // (*fCurrentTables).push_back(table);
  }

  std::cout << " Complete. (Loaded " << contents.size() << " Tables)" << std::endl;
  Finalise();
  return 1;
}

int DB::LoadAll(std::string dirname, std::string pattern)
{
  std::cout << "DB : Loading all DB files from " << dirname << pattern << std::endl;
  pattern = dirname + "/*"; //pattern;
  glob_t g;

  if (glob(pattern.c_str(), 0, 0, &g) == 0) {
    for (unsigned i = 0; i < g.gl_pathc; i++) {
      std::string path(g.gl_pathv[i]);
      //std::cout << "DB: Loading " << path << " ... ";
      if (!Load(path)) {
        std::cout << "Load Failed!" << std::endl;
        globfree(&g);
        return 0;
      }
    }
  }
  globfree(&g);
  return 1;
}

void DB::Finalise() {

  // To finalise the database we want to expand all databases
  // that have the entry "clone_table" in them.
  for (uint i = 0; i < (*fCurrentTables).size(); i++) {
    DBTable tbl = (*fCurrentTables)[i];
    if (!tbl.Has("clone")) continue;

    std::cout << "Found a table clone! " << tbl.GetIndexName() << std::endl;

    // Get the table to be cloned
    std::string clonename = tbl.GetS("clone");
    std::cout << "Name of table clone " << clonename << std::endl;
    DBTable clonetbl = GetTable(tbl.GetTableName(), clonename);

    std::cout << "Build a table" << std::endl;

    // Make new table from clone
    DBTable newtbl = DBTable(tbl.GetJSON());

    std::cout << "Copied existing clone table from DB" << std::endl;

    // Loop over fields and update

    newtbl.UpdateFields(&clonetbl);
    std::cout << "Finished running update" << std::endl;
    // newtbl->SetIndexName(tbl->GetIndexName());
    (*fCurrentTables)[i] = newtbl;

  }
}

bool DB::HasTable(std::string tablename, std::string index) {
  for (uint i = 0; i < (*fCurrentTables).size(); i++) {
    if (tablename.compare((*fCurrentTables)[i].GetTableName()) != 0) continue;
    if (index.compare((*fCurrentTables)[i].GetIndexName()) != 0) continue;
    return true;
  }
  if (fCurrentTables != fDefaultTables) {
    for (uint i = 0; i < (*fDefaultTables).size(); i++) {
      if (tablename.compare((*fDefaultTables)[i].GetTableName()) != 0) continue;
      if (index.compare((*fDefaultTables)[i].GetIndexName()) != 0) continue;
      return true;
    }
  }
  return false;
}


DBTable  DB::GetTable (std::string tablename, std::string index) {
  for (uint i = 0; i < (*fCurrentTables).size(); i++) {
    if (tablename.compare((*fCurrentTables)[i].GetTableName()) != 0) continue;
    if (index.compare((*fCurrentTables)[i].GetIndexName()) != 0) continue;
    return (*fCurrentTables)[i].Clone();
  }
  if (fCurrentTables != fDefaultTables) {
    for (uint i = 0; i < (*fDefaultTables).size(); i++) {
      if (tablename.compare((*fDefaultTables)[i].GetTableName()) != 0) continue;
      if (index.compare((*fDefaultTables)[i].GetIndexName()) != 0) continue;
      return (*fDefaultTables)[i].Clone();
    }
  }
  throw DBNotFoundError(tablename, index, "");
  return fNullTable;
}

DBTable* DB::GetLink  (std::string tablename, std::string index) {
  for (uint i = 0; i < (*fCurrentTables).size(); i++) {
    if (tablename.compare((*fCurrentTables)[i].GetTableName()) != 0) continue;
    if (index.compare((*fCurrentTables)[i].GetIndexName()) != 0) continue;
    return &(*fCurrentTables)[i];
  }
  if (fCurrentTables != fDefaultTables) {
    for (uint i = 0; i < (*fDefaultTables).size(); i++) {
      if (tablename.compare((*fDefaultTables)[i].GetTableName()) != 0) continue;
      if (index.compare((*fDefaultTables)[i].GetIndexName()) != 0) continue;
      return &(*fDefaultTables)[i];
    }
  }
  throw DBNotFoundError(tablename, index, "");
  return 0;
}


bool DB::HasTables (std::string tablename) {
  std::vector<DBTable> tableset;
  for (uint i = 0; i < (*fCurrentTables).size(); i++) {
    if (tablename.compare((*fCurrentTables)[i].GetTableName()) != 0) continue;
    return true;
  }
  if (fCurrentTables != fDefaultTables) {
    for (uint i = 0; i < (*fDefaultTables).size(); i++) {
      if (tablename.compare((*fDefaultTables)[i].GetTableName()) != 0) continue;
      return true;
    }
  }
  return false;
}


std::vector<DBTable>  DB::GetTableGroup (std::string tablename) {

  std::vector<DBTable> tableset;
  for (uint i = 0; i < (*fCurrentTables).size(); i++) {
    if (tablename.compare((*fCurrentTables)[i].GetTableName()) != 0) continue;
    std::string index = (*fCurrentTables)[i].GetIndexName();
    tableset.push_back((*fCurrentTables)[i].Clone());
  }
  if (fCurrentTables != fDefaultTables) {
    for (uint i = 0; i < (*fDefaultTables).size(); i++) {
      if (tablename.compare((*fDefaultTables)[i].GetTableName()) != 0) continue;
      std::string index = (*fDefaultTables)[i].GetIndexName();
      tableset.push_back((*fDefaultTables)[i].Clone());
    }
  }
  return tableset;
}

std::vector<DBTable*> DB::GetLinkGroup  (std::string tablename) {
  std::vector<DBTable*> tableset;
  for (uint i = 0; i < (*fCurrentTables).size(); i++) {
    if (tablename.compare((*fCurrentTables)[i].GetTableName()) != 0) continue;
    std::string index = (*fCurrentTables)[i].GetIndexName();
    tableset.push_back(&(*fCurrentTables)[i]);
  }
  if (fCurrentTables != fDefaultTables) {
    for (uint i = 0; i < (*fDefaultTables).size(); i++) {
      if (tablename.compare((*fDefaultTables)[i].GetTableName()) != 0) continue;
      std::string index = (*fDefaultTables)[i].GetIndexName();
      tableset.push_back(&(*fDefaultTables)[i]);
    }
  }
  return tableset;
}

void DB::AddTable(DBTable tbl) {
  std::string name = tbl.GetTableName();
  std::string index = tbl.GetIndexName();
  for (uint i = 0; i < (*fCurrentTables).size(); i++) {
    if (name.compare((*fCurrentTables)[i].GetTableName()) != 0) continue;
    if (index.compare((*fCurrentTables)[i].GetIndexName()) != 0) continue;
    (*fCurrentTables)[i] = tbl;
    return;
  }
  fCurrentTables->push_back(tbl);
}


DB* DB::fPrimary = NULL;

std::string checkPath(std::string env){
	const char* chars = getenv(env.c_str());
	if(!chars){
	  printMissingEviromentError("Impossible to find the enviromet variable "+ env);
	}
	auto path = std::string(chars);
	if(path.find('~') != std::string::npos){
	  std::runtime_error e("A ~ (tilde) was found in the file path for " + env + "\n" + path + "\nRecheck the enviromental variable, be sure there are no extra \" (quote) in the bashrc (or where is defined)"
						 "\nTry doing \necho " + env + "\nAnd be sure is an absolute path the result \nIf the ~ is still present replaced it by hand"
						 "\nRemember each time you edit the file to OPEN A NEW SHELL and use it, old shell are NOT UPDATED");
	  throw e;
	}
	// std::cout << "DB : " << dbtype << "_DATA_PATH : " << path << std::endl;
	return path;
}


std::string DB::GetDataPath(std::string dbtype) {
  // std::cout << "DB : Getting database from : " << dbtype << "_DATA_PATH" << std::endl;
  auto env = dbtype + "_DATA_PATH";
  return checkPath(env);
}

std::string DB::GetSimulationPath(std::string dbtype) {
	auto env = dbtype + "_DATA_PATH";
	return checkPath(env);
}
//NOTE: NOT USED
std::vector<std::string> DB::GetDynamicLibraryPath(){
  std::vector<std::string> dynamicdirs;

  char const* envPath = getenv("CRESTA_OBJECTS_PATH");
  if (envPath) {
    std::vector<std::string> paths = DataBase::ParseToStr(std::string(envPath),":");
    for (size_t i = 0; i < paths.size(); i++){
      if (paths[i].length()){
        dynamicdirs.push_back(paths[i]);
      }
    }
  }
  dynamicdirs.push_back("./");
  return dynamicdirs;
}


void DB::AddGlobalParameter(std::string field, std::string value){
  bool found = false;
  std::vector<DBTable*> dtbls = DB::Get()->GetLinkGroup("VARIABLE");
  for (uint i = 0; i < dtbls.size(); i++) {
    if (dtbls[i]->Has(field)) {
      dtbls[i]->Set(field,value);
      found = true;
    }
  }
  if (!found) {
    DBTable* tbl = DB::GetLink("VARIABLE","global");
    tbl->Set(field, value);
  }
}

void DB::PrintConstants(){
  DBEvaluator evals;
  evals.PrintConstants();
}

std::string DB::GetConstantList(){
  DBEvaluator evals;
  return evals.GetConstantList();
}

// ---------------------------------------------------------------------------
} // - DataBase Namespace
} // - CRESTA Namespace
