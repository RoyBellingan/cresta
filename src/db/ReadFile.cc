// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with NUISANCE.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "ReadFile.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace DataBase { // CRESTA Database Sub Namespace
// ---------------------------------------------------------------------------

int ReadFile(const std::string &filename, std::string &filecontents)
{
  bool bzip2 = true;

  const int nmax = 4096;
  char buffer[nmax];
  int nread = 0;

  bzip2 = false;

  if (!bzip2) {
    // Not BZIP2 format, so just dump the file contents raw into the string
    FILE *f = fopen(filename.c_str(), "r");

    if (f == NULL)
      return -1;

    filecontents = "";
    while ( (nread = fread(buffer, sizeof(char), nmax, f)) )
      filecontents.append(buffer, nread);

    fclose(f);
  }

  return 0;
}

std::vector<std::string> Expand(std::string pattern) {
  glob_t g;
  std::vector<std::string> vecs;
  if (glob(pattern.c_str(), 0, 0, &g) == 0) {
    for (unsigned i = 0; i < g.gl_pathc; i++) {
      std::string path(g.gl_pathv[i]);
      vecs.push_back(path);
    }
  }
  globfree(&g);
  return vecs;
}

std::vector<std::string> ParseToStr(std::string str, const char* del) {
  std::istringstream stream(str);
  std::string temp_string;
  std::vector<std::string> vals;

  while (std::getline(stream >> std::ws, temp_string, *del)) {
    if (temp_string.empty()) continue;
    vals.push_back(temp_string);
  }

  return vals;
}

// ---------------------------------------------------------------------------
} // - DataBase namespace
} // - CRESTA namespace
