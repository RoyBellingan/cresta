// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with NUISANCE.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __CRESTA_ReadFile_hh__
#define __CRESTA_ReadFile_hh__
#include "SystemHeaders.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace DataBase { // CRESTA Database Sub Namespace
// ---------------------------------------------------------------------------

/// Reads a text file into a string. Returns 0 if success, negative if file cannot be opened
int ReadFile(const std::string &filename, std::string &filecontents);

/// Uses glob to expand wildcards in files and return vector of strings
std::vector<std::string> Expand(std::string);

/// Parse space seperated string into values
std::vector<std::string> ParseToStr(std::string input, const char* del=" ");

// ---------------------------------------------------------------------------
} // - namespace DataBase
} // - namespace CRESTA
#endif
