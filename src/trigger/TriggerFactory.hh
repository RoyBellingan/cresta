#ifndef __TRIGGER_FACTORY_HH__
#define __TRIGGER_FACTORY_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "DetectorPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Trigger { // CRESTA Trigger Sub Namespace
// ---------------------------------------------------------------------------

/// Globally accessible Factory for producing triggers.
namespace TriggerFactory {
/// Function to create trigger objects from tables
VTrigger* Construct(DBTable table);
/// Function to generate all possible triggers loaded by the user tables
void ConstructTriggers();
} // - namespace TriggerFactory

} // - namespace Trigger
} // - namespace CRESTA
#endif
