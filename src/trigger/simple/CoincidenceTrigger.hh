#ifndef __COINC_TRIGGER_HH__
#define __COINC_TRIGGER_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "DetectorPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Trigger { // CRESTA Trigger Sub Namespace
// ---------------------------------------------------------------------------

/// Simple co-incident trigger class, with
/// a single optional energy and time threshold.
class CoincidenceTrigger : public VTrigger {
public:

  /// Database constructor
  CoincidenceTrigger(DBTable tbl);

  /// Simple Generic C++ Constructor
  CoincidenceTrigger(std::string name,
                std::vector<std::string> processors,
                G4double ethresh);

  /// Destructor
  ~CoincidenceTrigger() {};

  /// Function to setup detector processors from global list
  void SetupProcessors(std::vector<std::string> det);

  /// Trigger Processing
  bool ProcessTrigger(const G4Event* event);


protected:


  std::vector<VProcessor*> fProcessors; ///< Vector of all valid processors
  std::vector<int> fTriggerMask;
  std::vector<double> fTriggerEffs;
  G4int fRequireN;// Requires N detectors to be hit in order to trigger
  G4double fEnergyThreshold; ///< Energy Threshold setting, applied to all processors
  G4double fEfficiency;

};

// ---------------------------------------------------------------------------
} // - namespace TRIGGER
} // - namespace COSMIC
#endif
