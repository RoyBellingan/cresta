#ifndef __PROCESSOR_PACKAGE_HH__
#define __PROCESSOR_PACKAGE_HH__
// HACK way to hide DB includes in later packages and have the user not have to
// worry about....
#include "processor/ProcessorFactory.hh"
#include "processor/tomography/TrueMuonPoCAProcessor.hh"
using namespace CRESTA::Processor;
#endif
