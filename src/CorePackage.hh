#ifndef __CORE_PACKAGE_HH__
#define __CORE_PACKAGE_HH__
// HACK way to hide CORE includes in later packages and have the user not have to
// worry about it....
#include "core/Analysis.hh"
#include "core/DynamicObjectLoader.hh"
#include "core/VDetector.hh"
#include "core/VFluxProcessor.hh"
#include "core/VProcessor.hh"
#include "core/VTrigger.hh"

using namespace CRESTA::Core;

#endif
