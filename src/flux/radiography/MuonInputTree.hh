#ifndef __CRESTA_MuonInputTree_HH__
#define __CRESTA_MuonInputTree_HH__
#include "SystemHeaders.hh"
#include "ROOTHeaders.hh"
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "GeometryPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
//---------------------------------------------------------------------------------

/// Shukla Flux Generator, derived from CRESTA code written by Chris Steer.
class MuonInputTree : public G4VUserPrimaryGeneratorAction
{
public:

    /// Constructor
    MuonInputTree();
    /// Destructor
    ~MuonInputTree();

    /// Generate the primary muon, and apply acceptance filter.
    void GeneratePrimaries(G4Event* anEvent);

    /// Get the Target Box setup
    std::vector<G4Box*> GetTargetBoxes();
    /// Get the Target Box Position Setup
    std::vector<G4ThreeVector> GetTargetBoxPositions();

    // Get Functions
    inline G4double GetMuonTime()     { return fMuonTime;     };
    inline G4double GetMuonEnergy()   { return fMuonEnergy;   };
    inline G4ThreeVector GetMuonMom() { return fMuonDir;      };
    inline G4ThreeVector GetMuonPos() { return fMuonPos;      };
    inline int GetMuonPDG()           { return fMuonPDG;      };
    inline G4double GetExposureTime() { return fExposureTime; };
    inline void SetExposureTime(G4double e) { fExposureTime = e; };

private:

    /// Definitions for particle gun to avoid string comparisons
    std::vector<G4ParticleDefinition*> fParticleDefs;
    G4ParticleGun* fParticleGun; ///< Main particle gun

    // Target box also originally setup from GEO tables.
    bool fCheckTargetBoxes; ///< FLAG : Whether target boxes okay.
    std::vector<G4Box*> fTargetBoxes; ///< Geant4 Box Object for each target
    std::vector<G4ThreeVector> fTargetBoxPositions; ///< Position in world volume for each target
    G4int fTargetBoxesRequireN; ///< Requires at least this number of hits in different target boxes

    // Throws are tracked regardless of acceptance, so integrated time always correct.
    /// Current Integrated Exposure time. Derivide from NThrows and Integrated flux.
    G4double fExposureTime;

    G4double fMuonTime;     ///< MuonTime   Info for auto flux processor
    G4double fMuonEnergy;   ///< MuonEnergy Info for auto flux processor
    G4ThreeVector fMuonDir; ///< MuonDir    Info for auto flux processor
    G4ThreeVector fMuonPos; ///< MuonPos    Info for auto flux processor
    G4double fMuonPDG;      ///< MuonPDG    Info for auto flux processor

  TFile* fFluxInputFile;
  TTree* fFluxInputTree;
  float fMuonRead_x;
  float fMuonRead_y;
  float fMuonRead_z;
  float fMuonRead_t;
  float fMuonRead_px;
  float fMuonRead_py;
  float fMuonRead_pz;
  float fMuonRead_E;
  double fMuonRead_Fluxt;
  int fMuonRead_PDG;
  int fCurrentEventCounter;

  void Draw();

};
//---------------------------------------------------------------------------------




//---------------------------------------------------------------------------------
/// ShuklaFluxProcessor class : Automatically saves the true muon information
/// for each event into the TTree
class MuonInputTreeProcessor : public VFluxProcessor {
public:
    /// Processor can only be created with an associated
    /// shukla generator object.
    MuonInputTreeProcessor(MuonInputTree* gen, bool autosave = true);
    /// Destructor
    ~MuonInputTreeProcessor() {};

    /// Setup Ntuple entries
    bool BeginOfRunAction(const G4Run* run);

    /// Save the information from the generator for this event
    bool ProcessEvent(const G4Event* event);

    /// Return an integrated exposure time in s. Used for
    /// ending the run after so many seconds.
    G4double GetExposureTime();
    void ResetExposureTime(){ fGenerator->SetExposureTime(0.0);};

protected:

    MuonInputTree* fGenerator; ///< Pointer to associated generator

    bool fSave; ///< Flag to save event info automatically

    int fMuonTimeIndex; ///< Time Ntuple Index
    int fMuonEnergyIndex; ///< Energy Ntuple Index
    int fMuonDirXIndex; ///< DirX Ntuple Index
    int fMuonDirYIndex; ///< DirY Ntuple Index
    int fMuonDirZIndex; ///< DirZ Ntuple Index
    int fMuonPosXIndex; ///< PosX Ntuple Index
    int fMuonPosYIndex; ///< PosY Ntuple Index
    int fMuonPosZIndex; ///< PosZ Ntuple Index
    int fMuonPDGIndex;  ///< MPDG Ntuple Index

};

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
#endif
