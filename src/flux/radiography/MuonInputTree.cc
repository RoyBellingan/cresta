#include "MuonInputTree.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
// ---------------------------------------------------------------------------

MuonInputTree::MuonInputTree()
    : G4VUserPrimaryGeneratorAction(),
      fParticleGun(0),
      fMuonTime(0)
{
    std::cout << "FLX: Building MuonInputTree Generator" << std::endl;

    // Setup Table
    DBTable table = DB::Get()->GetTable("MUONFLUXINPUT", "config");

    // Get the particle table
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    // Save particle definitions
    fParticleDefs.push_back(particleTable->FindParticle("mu-"));
    fParticleDefs.push_back(particleTable->FindParticle("mu+"));

    // Setup file
    if (!table.Has("input_file")) {
      std::cout << " Need 'input_file' in MUONFLUXINPUT config" << std::endl;
      throw;
    }

    if (!table.Has("input_tree")) {
      std::cout << " Need 'input_tree' in MUONFLUXINPUT config" << std::endl;
      throw;
    }

    if (!table.Has("input_tag")){
      std::cout << " Need 'input_tag' in MUONFLUXINPUT config" << std::endl;
      throw;
    }

    // Setup TTree Input
    fFluxInputFile = new TFile( table.GetS("input_file").c_str(), "READ");
    fFluxInputTree = (TTree*) fFluxInputFile->Get( table.GetS("input_tree").c_str() );

    // Setup Branches for muon hits
    fFluxInputTree->Show(0);

    std::string readtag = table.GetS("input_tag");
    std::string fluxtag = table.GetS("flux_tag");

    std::cout <<"Reading vals from tag : " << readtag << std::endl;
    fFluxInputTree->SetBranchAddress( (readtag + "_x").c_str(), &fMuonRead_x);
    fFluxInputTree->SetBranchAddress( (readtag + "_y").c_str(), &fMuonRead_y);
    fFluxInputTree->SetBranchAddress( (readtag + "_z").c_str(), &fMuonRead_z);
    fFluxInputTree->SetBranchAddress( (readtag + "_t").c_str(), &fMuonRead_t);
    fFluxInputTree->SetBranchAddress( (readtag + "_px").c_str(), &fMuonRead_px);
    fFluxInputTree->SetBranchAddress( (readtag + "_py").c_str(), &fMuonRead_py);
    fFluxInputTree->SetBranchAddress( (readtag + "_pz").c_str(), &fMuonRead_pz);
    fFluxInputTree->SetBranchAddress( (readtag + "_E").c_str(), &fMuonRead_E);
    fFluxInputTree->SetBranchAddress( (readtag + "_pdg").c_str(), &fMuonRead_PDG);
    fFluxInputTree->SetBranchAddress( (fluxtag + "_t").c_str(), &fMuonRead_Fluxt);

    // Setup current event counter, default is 0. Have to -1 as iteration done at start of loop.
    fCurrentEventCounter = -1;
    if (table.Has("counter_override")) fCurrentEventCounter = table.GetI("counter_override") - 1;

    // Setup Target Boxes
    GetTargetBoxes();

    G4int nofParticles = 1;
    fParticleGun  = new G4ParticleGun(nofParticles);


    // Complete
    std::cout << "FLX: --> Complete." << std::endl;

    // Make a new processor
    Analysis::Get()->SetFluxProcessor(new MuonInputTreeProcessor(this));
}

MuonInputTree::~MuonInputTree()
{
    delete fParticleGun;
}

std::vector<G4Box*> MuonInputTree::GetTargetBoxes() {

    // If presences of target boxes already been set then just return
    if (fCheckTargetBoxes) {
        return fTargetBoxes;
    }

    // If its not set but we have boxes return boxes
    if (fTargetBoxes.size() > 0) {
        fCheckTargetBoxes = true;
        return fTargetBoxes;
    }
    std::cout << "FLX: --> Creating Target boxes" << std::endl;

    // If none set then make it
    std::vector<DBTable> targetlinks = DB::Get()->GetTableGroup("FLUX");
    for (uint i = 0; i < targetlinks.size(); i++) {
        DBTable tbl = targetlinks[i];

        // Select tables with target box names
        std::string index = tbl.GetIndexName();
        if (index.find("target_box_") == std::string::npos) continue;

        // If it has position and size we can use it
        if (!tbl.Has("position") || !tbl.Has("size")) {
            std::cout << "Failed to find/create target box!" << std::endl;
            throw;
        }

        // Create objects
        std::vector<G4double> size = tbl.GetVecG4D("size");
        std::vector<G4double> pos  = tbl.GetVecG4D("position");

        G4Box* box_sol = new G4Box(index, 0.5 * size[0], 0.5 * size[1], 0.5 * size[2]);
        G4ThreeVector box_pos = G4ThreeVector(pos[0], pos[1], pos[2]);

        // Save Box
        fTargetBoxes.push_back(box_sol);
        fTargetBoxPositions.push_back(box_pos);

    }

    // Set flag and return
    fCheckTargetBoxes = true;
    return fTargetBoxes;
}

std::vector<G4ThreeVector> MuonInputTree::GetTargetBoxPositions() {
    // If matching sizes its probs okay to return positions
    if (fTargetBoxes.size() == fTargetBoxPositions.size()) return fTargetBoxPositions;
    std::cout << "TargetBox Positions incorrect" << std::endl;
    throw;
}

void MuonInputTree::GeneratePrimaries(G4Event* anEvent) {

    if (!fCheckTargetBoxes) {
        GetTargetBoxes();
	Draw();
    }

    // Set exit clause to create a dummy muon if do while fails. Straight up.
    bool exitclause = false;
    bool good_event = false;

    G4ThreeVector direction = G4ThreeVector();
    G4ThreeVector position = G4ThreeVector();
    G4double time = 0;
    G4double energy = 0.0;
    G4double fluxtime = 0;
    int pdg;


    // Update current event counter
    fCurrentEventCounter += 1;

    if (fCurrentEventCounter > fFluxInputTree->GetEntries()-1){
      exitclause = true;
    }

    // Get Latest Event
    fFluxInputTree->GetEntry( fCurrentEventCounter );

    // Update values
    direction[0] = fMuonRead_px * MeV;
    direction[1] = fMuonRead_py * MeV;
    direction[2] = fMuonRead_pz * MeV;
    energy = sqrt(direction.mag2() + (105.66*MeV)*(105.66*MeV));
    direction = direction.unit();

    position[0] = fMuonRead_x * m;
    position[1] = fMuonRead_y * m;
    position[2] = fMuonRead_z * m;
    time = fMuonRead_t * s;
    fluxtime = fMuonRead_Fluxt;
    pdg = fMuonRead_PDG;

    int num_target_boxes_hit = 0;

    // If no target boxes defined all events are good
    if (fTargetBoxes.empty()) {
      good_event = true;
    } else {

      // If target boxes defined only save trajectories that hit at least one
      for (uint i = 0; i < fTargetBoxes.size(); i++) {

	//std::cout << "BOX : " << fTargetBoxPositions.at(i)[0] << " " << fTargetBoxPositions.at(i)[1] << " " << fTargetBoxPositions.at(i)[2] << std::endl;
	//std::cout << "POS : " << position[0] << " " << position[1] << " " << position[2] << std::endl;
	//std::cout << "DIR : " << direction[0] << " " << direction[1] << " " << direction[2] << std::endl;

	G4double d = (fTargetBoxes.at(i))->DistanceToIn(
							position - fTargetBoxPositions.at(i), direction);

	//std::cout << "DIS : " << d << " " << (d != kInfinity) << std::endl;

	if (d != kInfinity){
	  good_event = true;
	  break;
	}
      }
    }

    // Update muon values
    if (exitclause or !good_event){

      fMuonDir = G4ThreeVector(0.0,0.0,1.0);
      fMuonPos = G4ThreeVector(0.0,0.0,0.0);
      fMuonEnergy = 999.9;
      fMuonTime = time;
      fExposureTime = fluxtime;
      fMuonPDG = pdg;
    } else {
      //std::cout << "Saving good event " << good_event << position[0] << " " << position[1] << " " << position[2] << std::endl;

      fMuonDir = direction;
      fMuonPos = position;
      fMuonEnergy = energy - 105.66*MeV; // Want kinetic
      fMuonTime = time;
      fExposureTime = fluxtime;
      fMuonPDG = pdg;

      //std::cout << "Setting definition" << std::endl;
      if (fMuonPDG == 13){
	fParticleGun->SetParticleDefinition(fParticleDefs[0]);
      } else if (fMuonPDG == -13){
	fParticleGun->SetParticleDefinition(fParticleDefs[1]);
      } else {
	std::cout << "ERROR : non muon in event list!" << std::endl;
	throw;
      }

      //std::cout << "Creating particle gun " << std::endl;
      // Create Particle Gun
      fParticleGun->SetParticleEnergy(fMuonEnergy); // Kinetic Energy
      fParticleGun->SetParticleTime(fMuonTime);
      fParticleGun->SetParticleMomentumDirection(fMuonDir);
      fParticleGun->SetParticlePosition(fMuonPos);
      fParticleGun->GeneratePrimaryVertex(anEvent);
    }

    Draw();

    return;
}

  void MuonInputTree::Draw() {
    G4VVisManager* vis = Analysis::Get()->GetVisManager();
    if (vis) {

      for (int i = 0; i < fTargetBoxes.size(); i++) {
	G4Box drawbox2 = G4Box("target_box", fTargetBoxes[i]->GetXHalfLength(), fTargetBoxes[i]->GetYHalfLength(), fTargetBoxes[i]->GetZHalfLength());
	G4Transform3D tr2 = G4Transform3D(G4RotationMatrix(), fTargetBoxPositions[i]);
	G4Colour colour2(1., 0., 0.);
	G4VisAttributes attribs2(colour2);
	vis->Draw(drawbox2, attribs2, tr2);
      }
    }
  }

//---------------------------------------------------------------------------------


//------------------------------------------------------------------
MuonInputTreeProcessor::MuonInputTreeProcessor(MuonInputTree* gen, bool autosave) :
    VFluxProcessor("shukla"), fSave(autosave)
{
    fGenerator = gen;
}

bool MuonInputTreeProcessor::BeginOfRunAction(const G4Run* /*run*/) {

    std::string tableindex = "shukla";
    std::cout << "FLX: Registering MuonInputTreeProcessor NTuples " << tableindex << std::endl;

    G4AnalysisManager* man = G4AnalysisManager::Instance();

    // Fill index energy
    fMuonTimeIndex   = man ->CreateNtupleDColumn(tableindex + "_t");
    fMuonEnergyIndex = man ->CreateNtupleDColumn(tableindex + "_E");
    fMuonDirXIndex   = man ->CreateNtupleDColumn(tableindex + "_px");
    fMuonDirYIndex   = man ->CreateNtupleDColumn(tableindex + "_py");
    fMuonDirZIndex   = man ->CreateNtupleDColumn(tableindex + "_pz");
    fMuonPosXIndex   = man ->CreateNtupleDColumn(tableindex + "_x");
    fMuonPosYIndex   = man ->CreateNtupleDColumn(tableindex + "_y");
    fMuonPosZIndex   = man ->CreateNtupleDColumn(tableindex + "_z");
    fMuonPDGIndex    = man ->CreateNtupleIColumn(tableindex + "_pdg");

    return true;
}

bool MuonInputTreeProcessor::ProcessEvent(const G4Event* /*event*/) {

    // Register Trigger State
    fHasInfo = true;
    fTime    = fGenerator->GetMuonTime();
    fEnergy  = fGenerator->GetMuonEnergy();

    // Set Ntuple to defaults

    if (fHasInfo) {
        G4AnalysisManager* man = G4AnalysisManager::Instance();
        man->FillNtupleDColumn(fMuonTimeIndex,   fGenerator->GetMuonTime() / s);
        man->FillNtupleDColumn(fMuonEnergyIndex, fGenerator->GetMuonEnergy()  / MeV);
        man->FillNtupleDColumn(fMuonDirXIndex,   fGenerator->GetMuonMom().x() / MeV);
        man->FillNtupleDColumn(fMuonDirYIndex,   fGenerator->GetMuonMom().y() / MeV);
        man->FillNtupleDColumn(fMuonDirZIndex,   fGenerator->GetMuonMom().z() / MeV);
        man->FillNtupleDColumn(fMuonPosXIndex,   fGenerator->GetMuonPos().x() / m);
        man->FillNtupleDColumn(fMuonPosYIndex,   fGenerator->GetMuonPos().y() / m);
        man->FillNtupleDColumn(fMuonPosZIndex,   fGenerator->GetMuonPos().z() / m);
        man->FillNtupleIColumn(fMuonPDGIndex ,   fGenerator->GetMuonPDG());
        return true;
    } else {
        G4AnalysisManager* man = G4AnalysisManager::Instance();
        man->FillNtupleDColumn(fMuonTimeIndex, -999.);
        man->FillNtupleDColumn(fMuonEnergyIndex, -999.);
        man->FillNtupleDColumn(fMuonDirXIndex, -999.);
        man->FillNtupleDColumn(fMuonDirYIndex, -999.);
        man->FillNtupleDColumn(fMuonDirZIndex, -999.);
        man->FillNtupleDColumn(fMuonPosXIndex, -999.);
        man->FillNtupleDColumn(fMuonPosYIndex, -999.);
        man->FillNtupleDColumn(fMuonPosZIndex, -999.);
        man->FillNtupleIColumn(fMuonPDGIndex,  -999 );
        return false;
    }
    return true;
}

G4double MuonInputTreeProcessor::GetExposureTime() {
    return fGenerator->GetExposureTime();
}

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
