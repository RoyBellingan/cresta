#ifdef __USE_PUMAS__
//******************************************************************************
// PumasBackwardsGenerator.cc
//
// 1.00 JMV, LLNL, Jan-2007:  First version.
//******************************************************************************
//
#include "PumasBackwardsGenerator.hh"
#include "pumas.h"

bool VerbosePUMAS = false;


namespace CRESTA {
namespace Flux {

static struct pumas_context * context = NULL;
static struct pumas_physics * physics = NULL;
  
  void PrintPUMASState(int steps, struct pumas_state * state){
    if (VerbosePUMAS){
     std::cout << steps << " " << state->position[0] << " " << state->position[1] << " "
     << state->position[2] << " " << state->kinetic << " " << state->weight << std::endl;
    }
    return;
  }


static double G4PUMASRAND(struct pumas_context * context)
{
  //return rand() / (double)RAND_MAX;
  return G4UniformRand();
}

static G4Navigator* G4PUMASNAV = NULL;
double G4PUMASDENSITY = 0.0;
double LASTG4PUMASDENSITY = 0.0;
int LASTPUMASLOCAL = 0;

static double G4PUMASLOCALS(struct pumas_medium * medium,
                            struct pumas_state * state,
                            struct pumas_locals * locals)
{

  // Get Position
  double x = state->position[0];
  double y = state->position[1];
  double z = state->position[2];

  // Get Geant4 Navigator
  if (!G4PUMASNAV) {
    G4PUMASNAV = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
  }

  // Get Geant4 Navigator
   G4PUMASDENSITY = -1.0;
   G4VPhysicalVolume* phys = G4PUMASNAV->LocateGlobalPointAndSetup(G4ThreeVector(x * m, y * m, z * m));
   if (phys){
     G4LogicalVolume* log = phys->GetLogicalVolume();
     if (log){
       G4Material* mat = log->GetMaterial();
       if (mat){
         G4PUMASDENSITY = mat->GetDensity() / (kg / m3);
       }
       if (VerbosePUMAS) std::cout << "FOUND MATERIAL " << mat << std::endl;
     }
   }

  if (VerbosePUMAS) std::cout << "GOT LOCALS DENSITY " << G4PUMASDENSITY << std::endl;
  if (G4PUMASDENSITY >= 0){
    locals->magnet[0] = 0.;
    locals->magnet[1] = 0.;
    locals->magnet[2] = 0.;
    locals->density = G4PUMASDENSITY;
    return 0.1; // Propose 10cm stepping length
  // Out of bounds
  } else {
    locals->magnet[0] = 0.;
    locals->magnet[1] = 0.;
    locals->magnet[2] = 0.;
    locals->density = G4PUMASDENSITY; // Set from Geant4 G4Navigator
    return 0.1; // Propose 10cm stepping length
  }

}

// Medium list. Should be one for each material, but we do this
// dynamically using G4, use two so PUMAS knows when there is a change.
static struct pumas_medium G4PUMASMEDIA[2] = { { 0, &G4PUMASLOCALS },
                                               { 1, &G4PUMASLOCALS }};

  
// Medium Function, determines material vs position.
static enum pumas_step G4PUMASMEDIUM(struct pumas_context * context,
                          struct pumas_state * state,
                          struct pumas_medium ** medium_ptr,
                          double * step_ptr)
{

  if (VerbosePUMAS) std::cout << "GETTING MEDIUM" << std::endl;
  // Initial Update
  if (medium_ptr != NULL) *medium_ptr = G4PUMASMEDIA;
  if (step_ptr != NULL) *step_ptr = 0.0;

  // Nav initialisation
  if (!G4PUMASNAV) {
    G4PUMASNAV = G4TransportationManager::GetTransportationManager()
                  ->GetNavigatorForTracking();
  }

  // Get the state position
  double x = state->position[0];
  double y = state->position[1];
  double z = state->position[2];

  if (VerbosePUMAS) std::cout << "GETTING DENSITY AT " << x << " " << y << " " << z << std::endl;
  // Get the navigator density.
  G4PUMASDENSITY = -1.0;
  G4VPhysicalVolume* phys = G4PUMASNAV->LocateGlobalPointAndSetup(G4ThreeVector(x * m, y * m, z * m));
  if (phys){
    G4LogicalVolume* log = phys->GetLogicalVolume();
    if (log){
      G4Material* mat = log->GetMaterial();
      if (mat){
        G4PUMASDENSITY = mat->GetDensity() / (kg / m3);
      }
    }
  }

  if (VerbosePUMAS) std::cout << "LOCAL PUMAS DENSITY " << G4PUMASDENSITY << std::endl;

  double step = 1E+03;

  
	  
  // Out of bounds or kinetic check < 500 GeV
  if (G4PUMASDENSITY <= 0.0 || state->kinetic > 10000){
    if (medium_ptr != NULL) *medium_ptr = NULL;
    if (step_ptr != NULL) step = -1;
    if (VerbosePUMAS) std::cout << "OUT OF BOUNDS OR KINETIC CHECK " << std::endl;
    return PUMAS_STEP_EXACT;
  }

  if (VerbosePUMAS) std::cout << "CHECKING CHANGE" << std::endl;
  // Hasn't Changed
  if (G4PUMASDENSITY == LASTG4PUMASDENSITY){

    if (LASTPUMASLOCAL == 0){
      if (medium_ptr != NULL) *medium_ptr = G4PUMASMEDIA + 0;
      if (step_ptr != NULL) step = 1.;
      LASTPUMASLOCAL = 0;
    } else {
      if (medium_ptr != NULL) *medium_ptr = G4PUMASMEDIA + 1;
      if (step_ptr != NULL) step = 1.;
      LASTPUMASLOCAL = 1;
    }

  // Has Changed
  } else {

    if (LASTPUMASLOCAL == 0){
      if (medium_ptr != NULL) *medium_ptr = G4PUMASMEDIA + 1;
      if (step_ptr != NULL) step = 1.;
      LASTPUMASLOCAL = 1;
    } else {
      if (medium_ptr != NULL) *medium_ptr = G4PUMASMEDIA + 0;
      if (step_ptr != NULL) step = 1.;
      LASTPUMASLOCAL = 0;
    }

  }
  if (VerbosePUMAS) std::cout << "SETTING STEP EPSILON" << std::endl;
  if (step_ptr != NULL) {
#define STEP_EPSILON 1E-07
    if (step > 0) step += STEP_EPSILON;
    *step_ptr = step;
  }

  //  sleep(1);
  if (step_ptr != NULL){
    if (VerbosePUMAS) std::cout << "STEP LENGTH " << *step_ptr << std::endl;
  }
  LASTG4PUMASDENSITY = G4PUMASDENSITY;

  return PUMAS_STEP_APPROXIMATE;	  
}

//----------------------------------------------------------------------------//
PumasBackwardsGenerator::PumasBackwardsGenerator()
{

  if (VerbosePUMAS) std::cout << "Building PUMAS Backwards Generator" << std::endl;
  
  // Create Particle Gun
  G4int nofParticles = 1;
  fParticleGun  = new G4ParticleGun(nofParticles);

  // Set exposure and nthrows
  fExposureTime = 0.0;
  fKineticThreshold = 1000;

  // Get a histogram of densities
  TH2D* histXY = NULL;
  TH2D* histXZ = NULL;
  TH2D* histYZ = NULL;

  // Load all materials
  std::cout << "Loading Materials" << std::endl;
  //  pumas_physics_create(&physics, PUMAS_PARTICLE_MUON, "materials/mdf/standard.xml",
  //                                        "materials/dedx/muon");

  const char * dump_file = "dump";
        FILE * fid = fopen(dump_file, "rb");
        if (fid == NULL) {
                perror(dump_file);
                //exit_gracefully(EXIT_FAILURE);
		throw;
        }
        pumas_physics_load(&physics, fid);
        fclose(fid);
	
  pumas_physics_material_index(physics,"StandardRock", &G4PUMASMEDIA[0].material);
  pumas_physics_material_index(physics,"Air", &G4PUMASMEDIA[1].material);

  //  const char * dump_file = "materials/dump";
  //        FILE * fid = fopen(dump_file, "rb");
  //    if (fid == NULL) {
  //              perror(dump_file);
  //            exit_gracefully(EXIT_FAILURE);
  //    }
  //    pumas_physics_load(&physics, fid);
  //    fclose(fid);
  
  // Build the PUMAS Context
  enum pumas_return rc = pumas_context_create(&context, physics, 0);
  context->mode.direction = PUMAS_MODE_BACKWARD;

  context->event = (pumas_event)(context->event |PUMAS_EVENT_LIMIT_DISTANCE);
  context->event = (pumas_event)(context->event |PUMAS_EVENT_LIMIT_KINETIC);


  context->mode.energy_loss = PUMAS_MODE_DETAILED;

  context->mode.scattering = PUMAS_MODE_FULL_SPACE; //LONGITUDINAL;
  context->limit.kinetic = 1E+02;
  context->limit.distance = 10000;
  context->random = &G4PUMASRAND;
  //context->event = PUMAS_EVENT_LIMIT_KINETIC;
  context->medium = &G4PUMASMEDIUM;

  std::cout << "Built pumas context : " << context << std::endl;


  DBTable table = DB::Get()->GetTable("PUMAS", "config");
  std::vector<G4double> vec = table.GetVecG4D("position");  
  fSpherePosition[0] = vec[0];
  fSpherePosition[1] = vec[1];
  fSpherePosition[2] = vec[2];

  
  // Make a new processor
  Analysis::Get()->SetFluxProcessor(new PumasBackwardsProcessor(this));
}

//----------------------------------------------------------------------------//
PumasBackwardsGenerator::~PumasBackwardsGenerator()
{
   pumas_context_destroy(&context);
   pumas_physics_destroy(&physics);
}


/* Gaisser's flux model, see e.g. the PDG */
static double flux_gaisser(double cos_theta, double Emu)
{
        const double ec = 1.1 * Emu * cos_theta;
        const double rpi = 1. + ec / 115.;
        const double rK = 1. + ec / 850.;
        return 1.4E+03 * pow(Emu, -2.7) * (1. / rpi + 0.054 / rK);
}

/* Volkova's parameterization of cos(theta*) */
static double cos_theta_star(double cos_theta)
{
        const double p[] = { 0.102573, -0.068287, 0.958633, 0.0407253,
                0.817285 };
        const double cs2 =
            (cos_theta * cos_theta + p[0] * p[0] + p[1] * pow(cos_theta, p[2]) +
                p[3] * pow(cos_theta, p[4])) /
            (1. + p[0] * p[0] + p[1] + p[3]);
        return cs2 > 0. ? sqrt(cs2) : 0.;
}

static double flux_gccly(double cos_theta, double kinetic_energy)
{
      // std::cout << "Getting Flux GCLLY" << std::endl;
        const double Emu = kinetic_energy + 0.10566;
        const double cs = cos_theta_star(cos_theta);
        return pow(1. + 3.64 / (Emu * pow(cs, 1.29)), -2.7) *
            flux_gaisser(cs, Emu);
}

void PumasBackwardsGenerator::GeneratePrimaries(G4Event* anEvent)
{
  if (VerbosePUMAS) std::cout << "GENERATING PRIMARY" << std::endl;
  double elevation = 90.0*G4PUMASRAND(context); //cos(theta);
  double cos_theta = cos((elevation) / 180. * M_PI);
  double sin_theta = sqrt(1.0 - cos_theta*cos_theta);
  double phi = CLHEP::twopi * G4PUMASRAND(context);
  double cos_phi   = cos(phi);
  double sin_phi   = sin(phi);

  // Throw random energy
  double kinetic_min = 0.1;
  double kinetic_max = 10.0;
  const double rk = log(kinetic_max / kinetic_min);
  double kf = kinetic_min * exp(rk * G4PUMASRAND(context));
  double wf = kf * rk; // Exp Energy weight
  
  // Throw a random position on the edge of a half sphere around the centre point.
  double sphereradius = 2.;
  double sphereelevation = 360.0*G4PUMASRAND(context);
  double spherecos_theta = cos((sphereelevation) / 180. * M_PI);
  double spheresin_theta = sqrt(1.0 - spherecos_theta*spherecos_theta);
  double spherephi = CLHEP::twopi * G4PUMASRAND(context);
  double spherecos_phi   = cos(spherephi);
  double spheresin_phi   = sin(spherephi);

  // Theta weight
  wf *= 90. * M_PI / 180.;

  // Phi weight
  wf *= 2.* M_PI;

  // Position weight
  wf *= 4. * M_PI * sphereradius * sphereradius;

  // Create starting PUMAS_STATE
  struct pumas_state state {
    state.charge=-1.,
    state.kinetic=kf,
    state.weight=wf
  };
  state.position[0] = sphereradius * spheresin_theta * spherecos_phi;
  state.position[1] = sphereradius * spheresin_theta * spheresin_phi;
  state.position[2] = sphereradius*spherecos_theta;

  state.direction[0] = -sin_theta*cos_phi;
  state.direction[1] = -sin_theta*sin_phi;
  state.direction[2] = -cos_theta;
  
  state.position[0] = state.position[0] + fSpherePosition[0]/m;
  state.position[1] = state.position[1] + fSpherePosition[1]/m;
  state.position[2] = state.position[2] + fSpherePosition[2]/m;
      
  // Update vectors
  fMuonPos    = G4ThreeVector(state.position[0]*m,state.position[1]*m,state.position[2]*m);
  fMuonDir    = G4ThreeVector(state.direction[0], state.direction[1], state.direction[2]);
  fMuonEnergy = kf * GeV;
  fMuonTime   = 1 * ns;

  fInitialWeight=wf;

  std::vector<double> posX;
  std::vector<double> posY;
  std::vector<double> posZ;

  int steps = 0;
  //  enum pumas_event event;
  //  struct pumas_medium * medium[2];
  
  // While the muon is less than some threshold, project backwards
  bool eventvalid = true;
  while (state.kinetic < fKineticThreshold - FLT_EPSILON) {

    if (VerbosePUMAS) std::cout << "RUNNING MUON TRANSPORT " << std::endl;
      context->mode.energy_loss =  PUMAS_MODE_DETAILED;
      context->mode.scattering =   PUMAS_MODE_FULL_SPACE;
      context->limit.kinetic = 1E+02;
      posX.push_back( state.position[0]*m );
      posY.push_back( state.position[1]*m );
      posZ.push_back( state.position[2]*m );
      
      PrintPUMASState(steps, &state);

      // Transport a step
      enum pumas_event event;
      struct pumas_medium * medium[2];
      pumas_context_transport(context, &state, &event, medium);
      
      if (event == PUMAS_EVENT_MEDIUM) {
	if (medium[1] == NULL) {

	  posX.push_back( state.position[0]*m );
	  posY.push_back( state.position[1]*m );
	  posZ.push_back( state.position[2]*m );
	  
	  break;
	}
      }
      if (steps > 10) {
	//eventvalid = false;
	break;
      }
      steps++;
  }

  //  std::cout << "DIF " << state.position[0]*m << " " << fFinalMuonPos[0] << std::endl;
  fFinalMuonPos    = G4ThreeVector(state.position[0]*m,state.position[1]*m,state.position[2]*m);
  fFinalMuonDir    = G4ThreeVector(state.direction[0], state.direction[1], state.direction[2]);
  fFinalMuonEnergy = state.kinetic * GeV;
  
  fEventWeight = state.weight;
  
  // Final position should be out of the material.
  // If Z is positive and within XY bounds, its an atmo event.
  G4double tempdensity = -1.0;
  G4VPhysicalVolume* phys = G4PUMASNAV->LocateGlobalPointAndSetup(G4ThreeVector(state.position[0]*m, state.position[1]*m, 0.0));
  if (phys){
    G4LogicalVolume* log = phys->GetLogicalVolume();
    if (log){
      G4Material* mat = log->GetMaterial();
      if (mat){
	tempdensity = mat->GetDensity() / (kg / m3);
      }
    }
  }
  
  if (tempdensity != -1.0 and eventvalid){
    fFluxWeight  = flux_gccly(-state.direction[2],
                              state.kinetic);
    G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
    if (pVVisManager) {
      G4Polyline polyline;
      G4Colour colour(1., 1., 0.);
      G4VisAttributes attribs(colour);
      
      polyline.SetVisAttributes(attribs);
      for (int i = 0; i < posX.size(); i++) {
        polyline.push_back( G4ThreeVector(posX[i], posY[i], posZ[i]));
      }
      pVVisManager->Draw(polyline);
    }
    
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    fParticleGun->SetParticleDefinition(particleTable->FindParticle("mu-"));
    fParticleGun->SetParticleEnergy(fMuonEnergy);
    fParticleGun->SetParticleTime(fMuonTime);
    fParticleGun->SetParticleMomentumDirection(fMuonDir);
    fParticleGun->SetParticlePosition(fMuonPos);
    fParticleGun->GeneratePrimaryVertex(anEvent);
    
  } else {
    fFluxWeight = 0.0;
  }

  // Use total accumulated weights to calculate average flux.
  // Units are in:
  // GeV^{-1}m^{-2} s^{-2} sr^{-1}
  // GeV removed by just scaling the energy kmax-kmin.
  // sr removed by assumption flux is over all angles.
  // This is the flux at this specific single point. What area should be used?
  // s^-2? Unsure.


  // Add to generator
  /*G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  fParticleGun->SetParticleDefinition(particleTable->FindParticle("mu-"));
  fParticleGun->SetParticleEnergy(fMuonEnergy);
  fParticleGun->SetParticleTime(fMuonTime);
  fParticleGun->SetParticleMomentumDirection(fMuonDir);
  fParticleGun->SetParticlePosition(fMuonPos);
  fParticleGun->GeneratePrimaryVertex(anEvent);
  */

}
//------------------------------------------------------------------



//------------------------------------------------------------------
PumasBackwardsProcessor::PumasBackwardsProcessor(PumasBackwardsGenerator * gen, bool autosave) :
  VFluxProcessor("pumas")
{
  fGenerator = gen;
}

bool PumasBackwardsProcessor::BeginOfRunAction(const G4Run* /*run*/) {

  std::string tableindex = "pumas";
  std::cout << "FLX: Registering PumasPrimaryFluxProcessor NTuples " << tableindex << std::endl;

  G4AnalysisManager* man = G4AnalysisManager::Instance();

  // Fill index energy
  fEventWeightIndex = man ->CreateNtupleDColumn(tableindex + "_w");
  fFluxWeightIndex = man ->CreateNtupleDColumn(tableindex + "_fw");
  fInitialWeightIndex = man ->CreateNtupleDColumn(tableindex + "_iw");

  fMuonTimeIndex   = man ->CreateNtupleDColumn(tableindex + "_t");
  fMuonEnergyIndex = man ->CreateNtupleDColumn(tableindex + "_E");
  fMuonDirXIndex   = man ->CreateNtupleDColumn(tableindex + "_dx");
  fMuonDirYIndex   = man ->CreateNtupleDColumn(tableindex + "_dy");
  fMuonDirZIndex   = man ->CreateNtupleDColumn(tableindex + "_dz");
  fMuonPosXIndex   = man ->CreateNtupleDColumn(tableindex + "_x");
  fMuonPosYIndex   = man ->CreateNtupleDColumn(tableindex + "_y");
  fMuonPosZIndex   = man ->CreateNtupleDColumn(tableindex + "_z");

  fFinalMuonEnergyIndex = man ->CreateNtupleDColumn(tableindex + "_FE");
  fFinalMuonDirXIndex   = man ->CreateNtupleDColumn(tableindex + "_Fdx");
  fFinalMuonDirYIndex   = man ->CreateNtupleDColumn(tableindex + "_Fdy");
  fFinalMuonDirZIndex   = man ->CreateNtupleDColumn(tableindex + "_Fdz");
  fFinalMuonPosXIndex   = man ->CreateNtupleDColumn(tableindex + "_Fx");
  fFinalMuonPosYIndex   = man ->CreateNtupleDColumn(tableindex + "_Fy");
  fFinalMuonPosZIndex   = man ->CreateNtupleDColumn(tableindex + "_Fz");
  
  fMuonPDGIndex    = man ->CreateNtupleIColumn(tableindex + "_pdg");
  return true;
}

bool PumasBackwardsProcessor::ProcessEvent(const G4Event* /*event*/) {

  // Register Trigger State
  fHasInfo = true;
  fTime    = 1.0;

  G4AnalysisManager* man = G4AnalysisManager::Instance();

  if (fHasInfo) {
      G4AnalysisManager* man = G4AnalysisManager::Instance();
      man->FillNtupleDColumn(fEventWeightIndex,   fGenerator->GetEventWeight());
      man->FillNtupleDColumn(fFluxWeightIndex,   fGenerator->GetFluxWeight());
      man->FillNtupleDColumn(fInitialWeightIndex, fGenerator->GetInitialWeight());

      man->FillNtupleDColumn(fMuonTimeIndex,   fGenerator->GetMuonTime());
      man->FillNtupleDColumn(fMuonEnergyIndex, fGenerator->GetMuonEnergy());
      man->FillNtupleDColumn(fMuonDirXIndex,   fGenerator->GetMuonDir().x());
      man->FillNtupleDColumn(fMuonDirYIndex,   fGenerator->GetMuonDir().y());
      man->FillNtupleDColumn(fMuonDirZIndex,   fGenerator->GetMuonDir().z());
      man->FillNtupleDColumn(fMuonPosXIndex,   fGenerator->GetMuonPos().x() / m);
      man->FillNtupleDColumn(fMuonPosYIndex,   fGenerator->GetMuonPos().y() / m);
      man->FillNtupleDColumn(fMuonPosZIndex,   fGenerator->GetMuonPos().z() / m);

      man->FillNtupleDColumn(fFinalMuonEnergyIndex, fGenerator->GetFinalMuonEnergy());
      man->FillNtupleDColumn(fFinalMuonDirXIndex,   fGenerator->GetFinalMuonDir().x());
      man->FillNtupleDColumn(fFinalMuonDirYIndex,   fGenerator->GetFinalMuonDir().y());
      man->FillNtupleDColumn(fFinalMuonDirZIndex,   fGenerator->GetFinalMuonDir().z());
      man->FillNtupleDColumn(fFinalMuonPosXIndex,   fGenerator->GetFinalMuonPos().x() / m);
      man->FillNtupleDColumn(fFinalMuonPosYIndex,   fGenerator->GetFinalMuonPos().y() / m);
      man->FillNtupleDColumn(fFinalMuonPosZIndex,   fGenerator->GetFinalMuonPos().z() / m);

      
      man->FillNtupleIColumn(fMuonPDGIndex ,   fGenerator->GetMuonPDG());
      return true;
  } else {
      G4AnalysisManager* man = G4AnalysisManager::Instance();
      man->FillNtupleDColumn(fEventWeightIndex,   -999.);
      man->FillNtupleDColumn(fFluxWeightIndex,   -999.);
      man->FillNtupleDColumn(fInitialWeightIndex,   -999.);

      man->FillNtupleDColumn(fMuonTimeIndex, -999.);
      man->FillNtupleDColumn(fMuonEnergyIndex, -999.);
      man->FillNtupleDColumn(fMuonDirXIndex, -999.);
      man->FillNtupleDColumn(fMuonDirYIndex, -999.);
      man->FillNtupleDColumn(fMuonDirZIndex, -999.);
      man->FillNtupleDColumn(fMuonPosXIndex, -999.);
      man->FillNtupleDColumn(fMuonPosYIndex, -999.);
      man->FillNtupleDColumn(fMuonPosZIndex, -999.);

      man->FillNtupleDColumn(fFinalMuonEnergyIndex, -999.);
      man->FillNtupleDColumn(fFinalMuonDirXIndex, -999.);
      man->FillNtupleDColumn(fFinalMuonDirYIndex, -999.);
      man->FillNtupleDColumn(fFinalMuonDirZIndex, -999.);
      man->FillNtupleDColumn(fFinalMuonPosXIndex, -999.);
      man->FillNtupleDColumn(fFinalMuonPosYIndex, -999.);
      man->FillNtupleDColumn(fFinalMuonPosZIndex, -999.);
      
      man->FillNtupleIColumn(fMuonPDGIndex,  -999 );
      return false;
  }

  return true;
}

} // - namespace Flux
} // - namespace CRESTA

#endif
