#include "RNGWrapper.hh"
#include "Randomize.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
namespace CRY { // CRESTA CRY External Sub Namespace
// ---------------------------------------------------------------------------

template CLHEP::HepRandomEngine* RNGWrapper<CLHEP::HepRandomEngine>::m_obj;

template double (CLHEP::HepRandomEngine::*RNGWrapper<CLHEP::HepRandomEngine>::m_func)(void);

// ---------------------------------------------------------------------------
} // - namespace CRY
} // - namespace Flux
} // - namespace CRESTA
