#ifndef __CRESTA_ParrallelBeamGenerator_HH__
#define __CRESTA_ParrallelBeamGenerator_HH__
#include "SystemHeaders.hh"
#include "ROOTHeaders.hh"
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "GeometryPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
//---------------------------------------------------------------------------------

class ParrallelBeamGenerator : public G4VUserPrimaryGeneratorAction
{
public:

  /// Constructor
  ParrallelBeamGenerator();
  /// Destructor
  ~ParrallelBeamGenerator();

  /// Pick a muon/antimuon based on random number
  void SampleParticleType();

  /// Randomly sample position from inside a G4Box
  void SamplePositionAndDirection(G4ThreeVector& pos, G4ThreeVector& dir);

  /// Generate the primary particle
  void GeneratePrimaries(G4Event* anEvent);

  // Get Functions
  inline G4double GetParticleTime()     { return fParticleTime;     };
  inline G4double GetParticleEnergy()   { return fParticleEnergy;   };
  inline G4ThreeVector GetParticleDir() { return fParticleDir;      };
  inline G4ThreeVector GetParticlePos() { return fParticlePos;      };
  inline G4double GetExposureTime() { return fExposureTime; };
  inline void SetExposureTime(G4double e) { fExposureTime = e; };
  G4Box* GetSourceBox();

private:

  G4double fMinEnergy; ///< Min Energy Range to integrate/throw
  G4double fMaxEnergy; ///< Max Energy Range to integrate/throw

  /// Definitions for particle gun to avoid string comparisons
  G4ParticleDefinition* fParticleDef;
  G4ParticleGun* fParticleGun; ///< Main particle gun

  // Source box setup originally used geometries from the main GEO
  // volume list, but no longer does. That is why it is setup this
  // way.
  G4Box* fSourceBox; ///< FLAG : Whether source box been created/checked
  G4ThreeVector fSourceBoxWidth; ///< Length/Width of box.
  G4ThreeVector fSourceBoxPosition; ///< Position of box in world volume

  // Throws are tracked regardless of acceptance, so integrated time always correct.
  /// Current Integrated Exposure time. Derivide from NThrows and Integrated flux.
  G4double fExposureTime;
  int fNThrows; ///< Number of throws ran so far.

  G4double fParticleTime;     ///< ParticleTime   Info for auto flux processor
  G4double fParticleEnergy;   ///< ParticleEnergy Info for auto flux processor
  G4ThreeVector fParticleDir; ///< ParticleDir    Info for auto flux processor
  G4ThreeVector fParticlePos; ///< ParticlePos    Info for auto flux processor
};
//---------------------------------------------------------------------------------




//---------------------------------------------------------------------------------
/// ShuklaFluxProcessor class : Automatically saves the true muon information
/// for each event into the TTree
class ParrallelBeamProcessor : public VFluxProcessor {
public:
  /// Processor can only be created with an associated
  /// shukla generator object.
  ParrallelBeamProcessor(ParrallelBeamGenerator* gen, bool autosave = true);
  /// Destructor
  ~ParrallelBeamProcessor() {};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Save the information from the generator for this event
  bool ProcessEvent(const G4Event* event);

  /// Return an integrated exposure time in s. Used for
  /// ending the run after so many seconds.
  G4double GetExposureTime();
  void ResetExposureTime() { fGenerator->SetExposureTime(0.0);};

protected:

  ParrallelBeamGenerator* fGenerator; ///< Pointer to associated generator

  bool fSave; ///< Flag to save event info automatically

  int fParticleTimeIndex; ///< Time Ntuple Index
  int fParticleEnergyIndex; ///< Energy Ntuple Index
  int fParticleDirXIndex; ///< DirX Ntuple Index
  int fParticleDirYIndex; ///< DirY Ntuple Index
  int fParticleDirZIndex; ///< DirZ Ntuple Index
  int fParticlePosXIndex; ///< PosX Ntuple Index
  int fParticlePosYIndex; ///< PosY Ntuple Index
  int fParticlePosZIndex; ///< PosZ Ntuple Index

};

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
#endif
