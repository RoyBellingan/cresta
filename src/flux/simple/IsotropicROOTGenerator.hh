//******************************************************************************
// PrimaryGeneratorAction.hh
//
// This class is a class derived from G4VUserPrimaryGeneratorAction for
// constructing the process used to generate incident particles.
//
// 1.00 JMV, LLNL, JAN-2007:  First version.
//******************************************************************************
//
#ifndef __CRESTA_IsotropicROOTGenerator_HH__
#define __CRESTA_IsotropicROOTGenerator_HH__
#include "SystemHeaders.hh"
#include "ROOTHeaders.hh"
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "GeometryPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
//---------------------------------------------------------------------------------

class IsotropicROOTGenerator : public G4VUserPrimaryGeneratorAction
{
public:

  /// Constructor
  IsotropicROOTGenerator();
  /// Destructor
  ~IsotropicROOTGenerator();

  /// Generate the primary muon, and apply acceptance filter.
  void GeneratePrimaries(G4Event* anEvent);

  inline const G4double& GetParticleEnergy(){ return fParticleEnergy; };
  inline const G4ThreeVector& GetParticleDir(){ return fParticleDir; };
  inline const G4ThreeVector& GetParticlePos(){ return fParticlePos; };
  inline const G4double& GetParticleTime(){ return fParticleTime;};
  inline const int GetParticlePDG()
  { return fParticleDefinitions[fParticleIndex]->GetPDGEncoding(); };

  // Get Functions
  inline G4double GetExposureTime() { return fParticleTime; };
  inline void SetExposureTime(G4double e) { fParticleTime = e; };
  void Draw();


void SampleParticle();
void SampleKinematics();

private:

  G4ParticleGun* fParticleGun; ///< Main particle gun

  TH1D* fParticleHist;
  std::vector<TH1D*> fParticleHistograms;
  G4double fSphereRadius;
  G4int fParticleIndex;
  std::vector<G4double> fParticleRates;
  std::vector<G4ParticleDefinition*> fParticleDefinitions;

  G4double fFluxUnits;
  G4double fParticleTime;
  G4ThreeVector fSpherePos;
  G4ThreeVector fParticleDir;
  G4double fParticleEnergy;
  // G4double fParticleTime;
  G4ThreeVector fParticlePos;
  bool fThrowDirection;

};
//---------------------------------------------------------------------------------




//---------------------------------------------------------------------------------
/// ShuklaFluxProcessor class : Automatically saves the true muon information
/// for each event into the TTree
class IsotropicROOTProcessor : public VFluxProcessor {
public:
  /// Processor can only be created with an associated
  /// shukla generator object.
  IsotropicROOTProcessor(IsotropicROOTGenerator* gen, bool autosave = true);
  /// Destructor
  ~IsotropicROOTProcessor() {};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Save the information from the generator for this event
  bool ProcessEvent(const G4Event* event);

  /// Return an integrated exposure time in s. Used for
  /// ending the run after so many seconds.
  G4double GetExposureTime();
  void ResetExposureTime() { fGenerator->SetExposureTime(0.0);};

protected:

  IsotropicROOTGenerator* fGenerator; ///< Pointer to associated generator

  int fParticleEnergyIndex;
  int fParticleTimeIndex;
  int fParticlePosXIndex;
  int fParticlePosYIndex;
  int fParticlePosZIndex;
  int fParticleDirXIndex;
  int fParticleDirYIndex;
  int fParticleDirZIndex;
  int fParticlePDGIndex;



};

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
#endif
