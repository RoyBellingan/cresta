#include "IsotropicROOTGenerator.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
// ---------------------------------------------------------------------------

IsotropicROOTGenerator::IsotropicROOTGenerator()
    : G4VUserPrimaryGeneratorAction(),
      fParticleGun(0),
      fParticleTime(0)
{
    std::cout << "FLX: Building Isotropic ROOT Generator" << std::endl;

    // Setup Table
    DBTable table = DB::Get()->GetTable("isotropicroot", "config");

    // Setup Particle Gun
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

    // Setup Particle Gun
    std::cout << "FLX: --> Creating Particle Gun." << std::endl;
    std::vector<std::string> particlelist  = table.GetVecS("particles");
    std::vector<std::string> histogramlist = table.GetVecS("histograms");
    TFile* inputfile = new TFile(table.GetS("inputfile").c_str());
    G4double integratedrate = 0.0;

    // Make New Particle Gun
    G4int nofParticles = particlelist.size();
    fParticleGun  = new G4ParticleGun(nofParticles);
    fParticleRates.push_back(0.0);
    bool rescale = table.GetB("rescale");

    // Loop over all and add definitions.
    for (int i = 0; i < particlelist.size(); i++){
      fParticleDefinitions.push_back( particleTable->FindParticle(particlelist[i]) );

      // Grab Histograms
      TH1D* histogram = (TH1D*) inputfile->Get( histogramlist[i].c_str() );
      histogram->SetDirectory(0);
      if (rescale){
        std::cout << "Scaling Histograms" << std::endl;
        histogram->Scale(1.0 / histogram->Integral());
      };

      fParticleHistograms .push_back( histogram );

      integratedrate += histogram->Integral();
      fParticleRates.push_back(integratedrate);
    }

    // Remove input histogram file
    inputfile->Close();

    // Now find histogram integrals
    for (int i = 0; i < fParticleHistograms.size(); i++){
      fParticleRates[i] = fParticleRates[i] / integratedrate;
    }

    // Read in Neutron flux table : KE vs theta
    fFluxUnits = table.GetG4D("rate_units");

    // Get the Particle Energy, Rate, Dir, and Position
    fSphereRadius = table.GetG4D("sphere_radius");

    // Get Position
    std::vector<G4double> pos = table.GetVecG4D("position");
    fSpherePos = G4ThreeVector(pos[0],pos[1],pos[2]);

    // Choose to throw dir or not
    fThrowDirection = table.GetB("throw_direction");

    // Reset Time Counter
    fParticleTime = 0.0;

    // Now setup the particle integrals and source/target boxes
    std::cout << "FLX: --> Complete." << std::endl;

    // Make a new processor
    Analysis::Get()->SetFluxProcessor(new IsotropicROOTProcessor(this));
}

IsotropicROOTGenerator::~IsotropicROOTGenerator()
{
    delete fParticleGun;
}

void IsotropicROOTGenerator::SampleParticle(){

  // Generate random number between 0 and 1
  G4double rand = G4UniformRand();

  // Find index that the number lies #include
  fParticleIndex = 0;
  for (int i = 0; i < fParticleRates.size()-1; i++){
    // std::cout << "Random : " << rand << " Range : " << fParticleRates[i] << " -> " << fParticleRates[i+1] << std::endl;
    if (rand > fParticleRates[i] and rand < fParticleRates[i+1]){
      fParticleIndex = i;
      break;
    }
  }
  // std::cout << "Reading Particle Index : " << fParticleIndex << std::endl;
  if (fParticleIndex == -1) fParticleIndex = 0;

  // Update Particle Index and Kinematic Histogram
  fParticleGun->SetParticleDefinition(fParticleDefinitions[fParticleIndex]);
  fParticleHist = fParticleHistograms[fParticleIndex];

}

void IsotropicROOTGenerator::SampleKinematics(){

  // Throw energy
  G4double ethrow = fParticleHist->GetRandom();

  // Set Particle Energy
  fParticleEnergy = pow( 10.0, ethrow ) * eV;
  // std::cout << "Particle Energy = " << fParticleEnergy / MeV << " -> " << fParticleDefinitions[fParticleIndex]->GetPDGEncoding() << std::endl;

  // Set Position as point on a sphere
  G4double thetapos = CLHEP::twopi * G4UniformRand();
  G4double phipos = CLHEP::twopi * G4UniformRand();
  fParticlePos[0] = fSpherePos[0] + fSphereRadius * sin(thetapos) * cos(phipos);
  fParticlePos[1] = fSpherePos[1] + fSphereRadius * sin(thetapos) * sin(phipos);
  fParticlePos[2] = fSpherePos[2] + fSphereRadius * cos(thetapos);

  // Set Direction as random
  G4double thetadir = CLHEP::twopi * G4UniformRand();
  G4double phidir = CLHEP::twopi * G4UniformRand();
  if (fThrowDirection){
    fParticleDir[0] = sin(thetadir) * cos(phidir);
    fParticleDir[1] = sin(thetadir) * sin(phidir);
    fParticleDir[2] = cos(thetadir);
  } else {
    fParticleDir[0] = -sin(thetapos) * cos(phipos);
    fParticleDir[1] = -sin(thetapos) * sin(phipos);
    fParticleDir[2] = -cos(thetapos);
  }

}

void IsotropicROOTGenerator::GeneratePrimaries(G4Event* anEvent) {

  SampleParticle();

  SampleKinematics();

  fParticleTime += 0.0;

  fParticleGun->SetParticleEnergy(fParticleEnergy);
  fParticleGun->SetParticleTime(fParticleTime);
  fParticleGun->SetParticleMomentumDirection(fParticleDir);
  fParticleGun->SetParticlePosition(fParticlePos);
  fParticleGun->GeneratePrimaryVertex(anEvent);

  return;
}
//---------------------------------------------------------------------------------


//------------------------------------------------------------------
IsotropicROOTProcessor::IsotropicROOTProcessor(IsotropicROOTGenerator* gen, bool autosave) :
    VFluxProcessor("isotropicroot")
{
    fGenerator = gen;
}

bool IsotropicROOTProcessor::BeginOfRunAction(const G4Run* /*run*/) {

    std::string tableindex = "isotropicroot";
    std::cout << "FLX: Registering IsotropicROOTProcessor NTuples " << tableindex << std::endl;

    G4AnalysisManager* man = G4AnalysisManager::Instance();

    fParticleEnergyIndex = man ->CreateNtupleDColumn(tableindex + "_e");
    fParticleTimeIndex = man ->CreateNtupleDColumn(tableindex + "_t");
    fParticlePosXIndex = man ->CreateNtupleDColumn(tableindex + "_x");
    fParticlePosYIndex = man ->CreateNtupleDColumn(tableindex + "_y");
    fParticlePosZIndex = man ->CreateNtupleDColumn(tableindex + "_z");
    fParticleDirXIndex = man ->CreateNtupleDColumn(tableindex + "_dx");
    fParticleDirYIndex = man ->CreateNtupleDColumn(tableindex + "_dy");
    fParticleDirZIndex = man ->CreateNtupleDColumn(tableindex + "_dz");
    fParticlePDGIndex  = man ->CreateNtupleIColumn(tableindex + "_pdg");

    return true;
}

bool IsotropicROOTProcessor::ProcessEvent(const G4Event* /*event*/) {

    // Register Trigger State
    fHasInfo = true;
    fTime    = fGenerator->GetParticleTime();
    fEnergy  = fGenerator->GetParticleEnergy();
    G4AnalysisManager* man = G4AnalysisManager::Instance();

    if (fHasInfo){
    man->FillNtupleDColumn(fParticleEnergyIndex, fGenerator->GetParticleEnergy() / eV);
    man->FillNtupleDColumn(fParticleTimeIndex, fGenerator->GetParticleTime());
    man->FillNtupleDColumn(fParticlePosXIndex, fGenerator->GetParticlePos().x() / mm);
    man->FillNtupleDColumn(fParticlePosYIndex, fGenerator->GetParticlePos().y() / mm);
    man->FillNtupleDColumn(fParticlePosZIndex, fGenerator->GetParticlePos().z() / mm);
    man->FillNtupleDColumn(fParticleDirXIndex, fGenerator->GetParticleDir().x() / mm);
    man->FillNtupleDColumn(fParticleDirYIndex, fGenerator->GetParticleDir().y() / mm);
    man->FillNtupleDColumn(fParticleDirZIndex, fGenerator->GetParticleDir().z() / mm);
    man->FillNtupleIColumn(fParticlePDGIndex, fGenerator->GetParticlePDG());

  } else {
    man->FillNtupleDColumn(fParticleEnergyIndex, -999.9);
    man->FillNtupleDColumn(fParticleTimeIndex, -999.9);
    man->FillNtupleDColumn(fParticlePosXIndex, -999.9);
    man->FillNtupleDColumn(fParticlePosYIndex, -999.9);
    man->FillNtupleDColumn(fParticlePosZIndex, -999.9);
    man->FillNtupleDColumn(fParticleDirXIndex, -999.9);
    man->FillNtupleDColumn(fParticleDirYIndex, -999.9);
    man->FillNtupleDColumn(fParticleDirZIndex, -999.9);
    man->FillNtupleIColumn(fParticlePDGIndex, -999);
  }
    return true;
}

G4double IsotropicROOTProcessor::GetExposureTime() {
    return fGenerator->GetParticleTime();
}

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
