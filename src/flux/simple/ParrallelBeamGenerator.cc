#include "ParrallelBeamGenerator.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
// ---------------------------------------------------------------------------

ParrallelBeamGenerator::ParrallelBeamGenerator()
    : G4VUserPrimaryGeneratorAction(),
      fParticleGun(0),
      fNThrows(0),
      fParticleTime(0),
      fSourceBox(NULL)
{
    //    G4AutoLock lock(&myMutex);
    std::cout << "FLX: Building Isotropic Sphere Generator" << std::endl;

    // Setup Table
    DBTable table = DB::Get()->GetTable("ParrallelBeam", "config");

    // Get requested particle
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    std::string id;
    if (table.Has("particle_id")) id = table.GetS("particle_id");
    fParticleDef = particleTable->FindParticle(id);

    // Create gun
    std::cout << "FLX: --> Creating Particle Gun." << std::endl;
    G4int nofParticles = 1;
    fParticleGun  = new G4ParticleGun(nofParticles);
    fParticleGun->SetParticleDefinition(fParticleDef);

    // Load user defined source box
    GetSourceBox();

    // Get other info frmo user
    if (table.Has("energy_min")) fMinEnergy = table.GetG4D("energy_min");
    if (table.Has("energy_max")) fMaxEnergy = table.GetG4D("energy_max");

    if (table.Has("direction")){
      std::vector<G4double> vecval = table.GetVecG4D("direction");
      fParticleDir[0] = vecval[0];
      fParticleDir[1] = vecval[1];
      fParticleDir[2] = vecval[2];
    }

    // Now setup the particle integrals and source/target boxes
    std::cout << "FLX: --> Complete." << std::endl;

    // Make a new processor
    Analysis::Get()->SetFluxProcessor(new ParrallelBeamProcessor(this));
}


G4Box* ParrallelBeamGenerator::GetSourceBox() {

  // Already has good source_box
  if (fSourceBox) return fSourceBox;
  std::cout << "FLX: Creating Source box" << std::endl;

  std::vector<DBTable> targetlinks = DB::Get()->GetTableGroup("FLUX");
  for (uint i = 0; i < targetlinks.size(); i++) {
    DBTable tbl = (targetlinks[i]);

    // Select tables with target box names
    std::string index = tbl.GetIndexName();
    if (index.compare("source_box") != 0) continue;

    // Get size and provide some checks
    std::vector<G4double> size = tbl.GetVecG4D("size");

    // Create the box
    fSourceBox = new G4Box(index, 0.5 * size[0], 0.5 * size[1], 0.5 * size[2]);

    // Find box placement
    std::vector<G4double> pos = tbl.GetVecG4D("position");
    fSourceBoxPosition = G4ThreeVector(pos[0], pos[1], pos[2]);

    break;
  }
  if (fSourceBox) return fSourceBox;

  // Cant find
  std::cout << "Cannot find source box table!" << std::endl;
  throw;
}

ParrallelBeamGenerator::~ParrallelBeamGenerator()
{
    delete fParticleGun;
}

void ParrallelBeamGenerator::SamplePositionAndDirection(G4ThreeVector& pos, G4ThreeVector& dir)
{

    dir[0] = fParticleDir[0];
    dir[1] = fParticleDir[1];
    dir[2] = fParticleDir[2];

    pos[0] = fSourceBoxPosition[0] + fSourceBox->GetXHalfLength() * (-1.0 + 2.0 * G4UniformRand()) ; // in m
    pos[1] = fSourceBoxPosition[1] + fSourceBox->GetYHalfLength() * (-1.0 + 2.0 * G4UniformRand()) ; // in m
    pos[2] = fSourceBoxPosition[2] + fSourceBox->GetZHalfLength() * (-1.0 + 2.0 * G4UniformRand()) ; // in m

    return;
}


void ParrallelBeamGenerator::GeneratePrimaries(G4Event* anEvent) {

    // Sample the energy and particle type
    G4double E = fMinEnergy + (fMaxEnergy-fMinEnergy) * G4UniformRand();
    G4ThreeVector direction = G4ThreeVector();
    G4ThreeVector position = G4ThreeVector();
    SamplePositionAndDirection(position, direction);

    fParticleDir = direction;
    fParticlePos = position;
    fParticleEnergy = E;
    fParticleTime += 1.0;

    fParticleGun->SetParticleDefinition(fParticleDef);
    fParticleGun->SetParticleEnergy(fParticleEnergy);
    fParticleGun->SetParticleTime(fParticleTime);
    fParticleGun->SetParticleMomentumDirection(fParticleDir);
    fParticleGun->SetParticlePosition(fParticlePos);
    fParticleGun->GeneratePrimaryVertex(anEvent);

    return;
}
//---------------------------------------------------------------------------------


//------------------------------------------------------------------
ParrallelBeamProcessor::ParrallelBeamProcessor(ParrallelBeamGenerator* gen, bool autosave) :
    VFluxProcessor("iso"), fSave(autosave)
{
    fGenerator = gen;
}

bool ParrallelBeamProcessor::BeginOfRunAction(const G4Run* /*run*/) {

    std::string tableindex = "iso";
    std::cout << "FLX: Registering ParrallelBeamProcessor NTuples " << tableindex << std::endl;

    G4AnalysisManager* man = G4AnalysisManager::Instance();

    // Fill index energy
    fParticleTimeIndex   = man ->CreateNtupleDColumn(tableindex + "_t");
    fParticleEnergyIndex = man ->CreateNtupleDColumn(tableindex + "_E");
    fParticleDirXIndex   = man ->CreateNtupleDColumn(tableindex + "_dx");
    fParticleDirYIndex   = man ->CreateNtupleDColumn(tableindex + "_dy");
    fParticleDirZIndex   = man ->CreateNtupleDColumn(tableindex + "_dz");
    fParticlePosXIndex   = man ->CreateNtupleDColumn(tableindex + "_x");
    fParticlePosYIndex   = man ->CreateNtupleDColumn(tableindex + "_y");
    fParticlePosZIndex   = man ->CreateNtupleDColumn(tableindex + "_z");

    return true;
}

bool ParrallelBeamProcessor::ProcessEvent(const G4Event* /*event*/) {

    // Register Trigger State
    fHasInfo = true;
    fTime    = fGenerator->GetParticleTime();
    fEnergy  = fGenerator->GetParticleEnergy();

    // Set Ntuple to defaults

    if (fHasInfo) {
        G4AnalysisManager* man = G4AnalysisManager::Instance();
        man->FillNtupleDColumn(fParticleTimeIndex,   fGenerator->GetParticleTime());
        man->FillNtupleDColumn(fParticleEnergyIndex, fGenerator->GetParticleEnergy());
        man->FillNtupleDColumn(fParticleDirXIndex,   fGenerator->GetParticleDir().x() / MeV);
        man->FillNtupleDColumn(fParticleDirYIndex,   fGenerator->GetParticleDir().y() / MeV);
        man->FillNtupleDColumn(fParticleDirZIndex,   fGenerator->GetParticleDir().z() / MeV);
        man->FillNtupleDColumn(fParticlePosXIndex,   fGenerator->GetParticlePos().x() / m);
        man->FillNtupleDColumn(fParticlePosYIndex,   fGenerator->GetParticlePos().y() / m);
        man->FillNtupleDColumn(fParticlePosZIndex,   fGenerator->GetParticlePos().z() / m);
        return true;
    } else {
        G4AnalysisManager* man = G4AnalysisManager::Instance();
        man->FillNtupleDColumn(fParticleTimeIndex, -999.);
        man->FillNtupleDColumn(fParticleEnergyIndex, -999.);
        man->FillNtupleDColumn(fParticleDirXIndex, -999.);
        man->FillNtupleDColumn(fParticleDirYIndex, -999.);
        man->FillNtupleDColumn(fParticleDirZIndex, -999.);
        man->FillNtupleDColumn(fParticlePosXIndex, -999.);
        man->FillNtupleDColumn(fParticlePosYIndex, -999.);
        man->FillNtupleDColumn(fParticlePosZIndex, -999.);
        return false;
    }
    return true;
}

G4double ParrallelBeamProcessor::GetExposureTime() {
    return fGenerator->GetParticleTime();
}

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
