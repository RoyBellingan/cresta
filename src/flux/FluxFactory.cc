#include "FluxFactory.hh"

#include "cry/CRYPrimaryGenerator.hh"
#include "simple/PrimaryGeneratorROOT.hh"
#include "simple/IsotropicSphereSource.hh"
#include "simple/ParrallelBeamGenerator.hh"
#include "radiography/MuonInputTree.hh"
#include "simple/IsotropicBoxSource.hh"
#include "simple/GaussianBeamSource.hh"
#include "simple/IsotropicCylinderSurface.hh"
#ifdef __USE_PUMAS__
#include "pumas/PumasBackwardsGenerator.hh"
#endif
namespace CRESTA { // Main CRESTA Namespace
namespace Flux { // CRESTA Flux Sub Namespace
// ---------------------------------------------------------------------------

G4VUserPrimaryGeneratorAction* FluxFactory::LoadFluxGenerator() {
  // Get default config if requested
  DBTable tbl = DB::Get()->GetTable("GLOBAL", "config");
  return LoadFluxGenerator(tbl);
}

G4VUserPrimaryGeneratorAction* FluxFactory::LoadFluxGenerator(DBTable table) {

  // Get the flux specification from the global config
  std::string type = table.GetS("flux");
  std::cout << "FLX: Loading Primary Flux Generator : " << type << std::endl;

  // Read Flux Table using ugly string comparisons!
  if (type.compare("cry") == 0)    return new CRYPrimaryGenerator();
  if (type.compare("isotropicsphere") == 0) return new IsotropicSphereGenerator();
  if (type.compare("muoninputtree") == 0) return new MuonInputTree();
  if (type.compare("parrallel") == 0) return new ParrallelBeamGenerator();
  if (type.compare("isotropicboxsource") == 0) return new IsotropicBoxSource();
  if (type.compare("isotropiccylindersurface") == 0) return new IsotropicCylinderSurface();
  if (type.compare("gaussianbeamsource") == 0) return new GaussianBeamSource();
  #ifdef __USE_PUMAS__
  if (type.compare("pumasbackwards") == 0) return new PumasBackwardsGenerator();
  #endif
  // If none of the previous results worked, lets check the dynamic loader
  G4VUserPrimaryGeneratorAction* gen = NULL;
  gen = DynamicObjectLoader::Get()->LoadDynamicFluxGenerator(type);
  if (gen) return gen;

  // Check if only made
  std::cout << "Generator Creation Failed!" << std::endl;
  throw;

  // Return NULL generator
  return 0;
}

// ---------------------------------------------------------------------------
} // - namespace Flux
} // - namespace CRESTA
