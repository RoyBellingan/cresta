// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __CRESTA_ActionFactory_HH__
#define __CRESTA_ActionFactory_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"

/// Top level CRESTA package namespace, to seperate from extensions.
namespace CRESTA { // Main CRESTA Namespace
/// Namespace for all G4 top level construction calls (World,Flux,Physics,etc)
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

/// Globally accessible Construction Factory for producing main run actions.
namespace ActionConstruction {
/// Function to load global configured actions
G4VUserActionInitialization* Build();
/// Function to load an action initialization from table
G4VUserActionInitialization* Build(DBTable table);
} // - namespace ActionConstruction

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
#endif
