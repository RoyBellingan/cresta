// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "ActionConstruction.hh"
#include "FluxConstruction.hh"

#include "cresta/CRESTAActionInitialization.hh"
#include "optical/OpticalActionInitialization.hh"
//#include "attikus/AttikusActionInitialization.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

G4VUserActionInitialization* ActionConstruction::Build() {
	// Get global definition table
	DBTable table = DB::Get()->GetTable("GLOBAL", "config");
	G4VUserActionInitialization* actions = Build(table);
  return actions;
}

G4VUserActionInitialization* ActionConstruction::Build(DBTable table) {
	// Get the action type from the config table
	std::string type = table.GetS("action");
	if (type.compare("cresta") == 0) return new CRESTAActionInitialization();
	else if (type.compare("optical") == 0) return new OpticalActionInitialization();
	//	else if (type.compare("attikus") == 0) return new AttikusActionInitialization();
	
	// Check we didn't get to here and fail string comparison
	std::cout << "Failed to create actions : " << type << std::endl;
	throw;
	return 0;
}

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
