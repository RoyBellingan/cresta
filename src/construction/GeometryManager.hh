// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __GEOMETRYMANAGER_HH__
#define __GEOMETRYMANAGER_HH__
#include "G4Headers.hh"
#include "CorePackage.hh"
#include "ProcessorPackage.hh"
#include "DetectorPackage.hh"
#include "DBPackage.hh"
#include "TriggerPackage.hh"
#include "GeometryPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

/// \class GeometryManager (singleton)
/// \brief Globally accessible geometry manager.
///
/// Keeps track of all loaded VGeometry objects and makes sure the
/// Geant4 mother/child hierarchy is correct when building.
class GeometryManager {
public:

  /// Main constructor, doesn't build world automatically.
  GeometryManager();
  /// Empty destructor.
  ~GeometryManager() {};

  /// Main construction function, builds the world based on all "GEO"
  /// DB tables provided.
  G4VPhysicalVolume* ConstructAll();

  /// For a given table, get the logical volume given its "mother" tag.
  G4LogicalVolume* GetMother(DBTable geo_tab);

  /// Get top level World Volume.
  G4VPhysicalVolume* GetWorldVolume() { return fWorldVolume; };

  /// Check if a table has a mother loaded for it.
  int MotherStatus(DBTable geo_tab);

  /// Check Geometry manager has a geometry of given id.
  bool HasVGeometry(std::string name);

  /// Singleton Access function.
  static inline GeometryManager *Get()
  { return fPrimary == 0 ? fPrimary = new GeometryManager : fPrimary; };

  /// List of all Geometry objects and their names.
  std::map<std::string, VGeometry*> fVGeometrys;

  /// List of all names loaded.
  std::vector<std::string> fGeoIDs;

  /// Singleton Instance.
  static GeometryManager *fPrimary;

  /// Top Level World Volume
  G4VPhysicalVolume* fWorldVolume;
};

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
#endif
