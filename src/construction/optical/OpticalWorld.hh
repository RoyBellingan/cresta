// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __CRESTA_OpticalWorld_HH__
#define __CRESTA_OpticalWorld_HH__
#include "G4Headers.hh"
#include "CorePackage.hh"
#include "ProcessorPackage.hh"
#include "DetectorPackage.hh"
#include "DBPackage.hh"
#include "TriggerPackage.hh"
#include "GeometryPackage.hh"

#include "construction/GeometryManager.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

/// \class CRESTAActionInitialization
/// \brief Action initialization class for CRESTA run routines.
///
/// Handles Run/RunAction/Stacking/Stepping and the Flux.
/// Associated OpticalWorld is called automatically in WorldConstruction.
class OpticalWorld : public G4VUserDetectorConstruction
{
public:
  /// constructor
  OpticalWorld();
  /// destructor
  virtual ~OpticalWorld();

public:
  /// Defines the detector geometry and returns a pointer to the physical World Volume
  virtual G4VPhysicalVolume* Construct();
  /// Register some of the detector's volumes as "sensitive"
  virtual void ConstructSDandField();
};

// ---------------------------------------------------------------------------
} // - namespace CRESTA
} // - namespace CRESTA
#endif
