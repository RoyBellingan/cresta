// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "OpticalRun.hh"
#include "G4Headers.hh"
#include "CorePackage.hh"
#include "DBPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

OpticalRun::OpticalRun()
  : G4Run()
{
  // Start a counter for this run
  Analysis::Get()->StartTheClock();
}

OpticalRun::~OpticalRun()
{
}

void OpticalRun::RecordEvent(const G4Event* event)
{

  // Print progress
  int eventid = event->GetEventID();
  Analysis::Get()->PrintProgress(eventid, numberOfEventToBeProcessed);

  // Do all processing loops
  Analysis::Get()->ProcessEvent(event);

  // Reset analysis state for next event.
  Analysis::Get()->BeginOfEventAction();

  // Check Abort State
  Analysis::Get()->CheckAbortState();

}

void OpticalRun::Merge(const G4Run* aRun)
{
  // Uncomment this if local run processing needed at a later date
  // const OpticalRun* localRun = static_cast<const OpticalRun*>(aRun);

  // Merge the normal runs
  G4Run::Merge(aRun);
}

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
