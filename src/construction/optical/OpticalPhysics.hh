#ifndef OpticalPhysics_h
#define OpticalPhysics_h 1
#include "G4Headers.hh"
#include "CorePackage.hh"
#include "ProcessorPackage.hh"
#include "DetectorPackage.hh"
#include "DBPackage.hh"
#include "TriggerPackage.hh"
#include "GeometryPackage.hh"

#include "G4VModularPhysicsList.hh"
#include "construction/GeometryManager.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

class OpticalPhysics: public G4VModularPhysicsList
{
public:
  /// constructor
  OpticalPhysics();
  /// destructor
  virtual ~OpticalPhysics();

  /// Set user cuts
  virtual void SetCuts();
};

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
#endif
