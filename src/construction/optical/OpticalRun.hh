// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __CRESTA_OpticalRun_HH__
#define __CRESTA_OpticalRun_HH__
#include "G4Headers.hh"
#include "CorePackage.hh"
#include "FluxPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

/// \class OpticalRun
/// \brief Main OpticalRun class used for event recording and generation
class OpticalRun : public G4Run
{
public:
    /// Constructor
    OpticalRun();
    /// Destructor
    virtual ~OpticalRun();

    /// Process to register the event
    virtual void RecordEvent(const G4Event*);

    /// Merges run outputs between slave jobs
    virtual void Merge(const G4Run*);

protected:
    int fPrintSize; ///< Counter to check processing size is sufficient
    long int fStartTime; ///< Start Time of this Run
    long int fCurTime; ///< End Time of this Run
};

// ---------------------------------------------------------------------------
} // - namespace CRESTA
} // - namespace CRESTA
#endif
