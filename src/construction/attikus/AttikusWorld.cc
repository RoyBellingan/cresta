// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "AttikusWorld.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

AttikusWorld::AttikusWorld()
  : G4VUserDetectorConstruction()
{}

AttikusWorld::~AttikusWorld()
{}

G4VPhysicalVolume* AttikusWorld::Construct()
{
  // if GDML is defined then load from global config
  DBTable tbl = DB::Get()->GetTable("GLOBAL", "config");

  // Build entire geometry
  G4VPhysicalVolume* world = GeometryManager::Get()->ConstructAll();

  // Create the triggers/processors
  ProcessorFactory::ConstructProcessors();
  TriggerFactory::ConstructTriggers();
  G4RunManager::GetRunManager()->GeometryHasBeenModified();

  return world;
}

void AttikusWorld::ConstructSDandField()
{
  return;
}

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
