#include "BinaryReactionPhysics.hh"

#include "BinaryReactionProcess.hh"
#include "G4GenericIon.hh"
#include "G4Neutron.hh"
#include "G4Proton.hh"
#include "G4Alpha.hh"
#include "globals.hh"
#include "G4PhysicsListHelper.hh"
#include "G4ProcessManager.hh"

// factory
#include "G4PhysicsConstructorFactory.hh"
//
G4_DECLARE_PHYSCONSTR_FACTORY(BinaryReactionPhysics);

BinaryReactionPhysics::BinaryReactionPhysics(G4int)
:  G4VPhysicsConstructor("BinaryReactionPhysics") {
  std::cout << "BINARY Creating Binary Reaction Physics in class" << std::endl;
}

BinaryReactionPhysics::BinaryReactionPhysics(const G4String& name)
:  G4VPhysicsConstructor(name) {
}

BinaryReactionPhysics::~BinaryReactionPhysics()
{
}

void BinaryReactionPhysics::ConstructParticle()
{
  G4GenericIon::GenericIon();
  G4Neutron::Neutron();
  G4Proton::Proton();
  G4Alpha::Alpha();
}

void BinaryReactionPhysics::ConstructProcess()
{
  std::cout << "BINARY Registering binary processes" << std::endl;
  BinaryReactionProcess* reactionProcess = new BinaryReactionProcess();
  reactionProcess->ParseParams(fReactionParams);

  G4ProcessManager* pManager = NULL;

  AddParticleProcess("neutronBinary", G4Neutron::Neutron(), reactionProcess);
  AddParticleProcess("protonBinary", G4Proton::Proton(), reactionProcess);
  AddParticleProcess("alphaBinary", G4Alpha::Alpha(), reactionProcess);
  AddParticleProcess("genericionBinary", G4GenericIon::GenericIon(), reactionProcess);

}

void BinaryReactionPhysics::AddParticleProcess(const G4String& name,
					       G4ParticleDefinition* part,
					       G4VDiscreteProcess* proc) {

  G4ProcessManager* pManager = part->GetProcessManager();
  pManager->AddDiscreteProcess(proc);

}
