// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __CRESTA_MaterialsFactory_hh__
#define __CRESTA_MaterialsFactory_hh__
#include "G4Headers.hh"
#include "DBPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

/// Detector Factory used to create SD from tables
namespace MaterialsFactory {
/// Get Element from string
G4Element* GetElement(std::string name);
/// Function to create detector objects from tables
G4Material* GetMaterial(std::string name);
/// Get some logical visualisation attributes depending on
/// material defaults
G4VisAttributes* GetVisForMaterial(DBTable table);
/// Get vis just based on material database name
G4VisAttributes* GetVisForMaterial(std::string name);
/// Get the material properties from its name
G4MaterialPropertiesTable* GetMaterialPropertiesTable(std::string name);
/// Build Material Properties from JSON table
G4MaterialPropertiesTable* GetMaterialPropertiesTable(DBTable table);
} // - namespace MaterialsFactory

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
#endif
