// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "PhysicsConstruction.hh"

#include "Shielding.hh"
#include "QGSP_BERT_HP.hh"
#include "optical/OpticalPhysics.hh"
#include "optical/WLSPhysicsList.hh"
//#include "attikus/AttikusPhysicsList.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// -----------------------------------------------------------------

G4VUserPhysicsList* PhysicsConstruction::Build() {
  return LoadPhysicsList();
}

G4VUserPhysicsList* PhysicsConstruction::LoadPhysicsList() {
  DBTable table = DB::Get()->GetTable("GLOBAL", "config");
  return LoadPhysicsList(table);
}

G4VUserPhysicsList* PhysicsConstruction::LoadPhysicsList(DBTable table) {

  // Get physList factory but also have support for user defined physics....
  G4PhysListFactory *physListFactory = new G4PhysListFactory();
  physListFactory->SetVerbose(0);
  G4VUserPhysicsList* physicsList;

  // Get physics list by horrible string comparison
  std::string physics = table.GetS("physics");
  if (physics.compare("shielding") == 0){
    physicsList =  new Shielding();
    std::cout << "PHY: Setting Shielding Physics List" << std::endl;
  } else if (physics.compare("QGSP_BERT_HP") == 0) {
    physicsList =  new QGSP_BERT_HP();
    std::cout << "PHY: Setting QGSP_BERT_HP Physics List" << std::endl;
  } else if (physics.compare("optical") == 0) {
    physicsList =  new OpticalPhysics();
    std::cout << "PHY: Setting Custom Optical Physics List" << std::endl;
  } else if (physics.compare("wlsoptical") == 0) {
    physicsList =  new WLSPhysicsList("QGSP_BERT_HP");
    std::cout << "PHY: Setting Custom Optical Physics List" << std::endl;
    //  } else if (physics.compare("attikus") == 0) {
    //    physicsList =  new AttikusPhysicsList();
    //    std::cout << "PHY: Setting Attikus Physics List" << std::endl;
  } else {
    physicsList = physListFactory->GetReferencePhysList(physics);
  }

  /// Add option to override cuts.
  if (table.Has("cuts")){
    SetPhysicsListCuts(table.GetS("cuts"), physicsList);
  }

  // If found just return good list
  if (physicsList) {
    physicsList->SetVerboseLevel(0);
    G4HadronicProcessStore::Instance()->SetVerbose(0);
    return physicsList;
  }

  // if here then failed
  std::cout << "Could not find physics list : " << physics << std::endl;
  throw;
}

void PhysicsConstruction::SetPhysicsListCuts(std::string cutname, G4VUserPhysicsList* list){

  std::cout << "PHY: --> Setting Custom Default Cuts : " << cutname << std::endl;
  DBTable regtbl = DB::Get()->GetTable("CUTS", cutname);

  // Get a default
  G4double defaultcut = 0.1 * nm;
  if (regtbl.Has("default_cut")) defaultcut = regtbl.GetG4D("default_cut");
  list->SetCutValue( defaultcut, "gamma"   );
  list->SetCutValue( defaultcut, "e-"      );
  list->SetCutValue( defaultcut, "e+"      );
  list->SetCutValue( defaultcut, "mu-"     );
  list->SetCutValue( defaultcut, "mu+"     );
  list->SetCutValue( defaultcut, "tau-"    );
  list->SetCutValue( defaultcut, "tau+"    );
  list->SetCutValue( defaultcut, "pi+"     );
  list->SetCutValue( defaultcut, "pi-"     );
  list->SetCutValue( defaultcut, "pi0"     );
  list->SetCutValue( defaultcut, "proton"  );
  list->SetCutValue( defaultcut, "neutron" );

  // Get list of cuts/info for this region
  if (regtbl.Has("gamma_cut"))
    list->SetCutValue( regtbl.GetG4D("gamma_cut"), "gamma" );
  if (regtbl.Has("electron_cut"))
    list->SetCutValue( regtbl.GetG4D("electron_cut"), "e-" );
  if (regtbl.Has("positron_cut"))
    list->SetCutValue( regtbl.GetG4D("positron_cut"), "e+" );
  if (regtbl.Has("muon_cut"))
    list->SetCutValue( regtbl.GetG4D("muon_cut"), "mu-" );
  if (regtbl.Has("antimuon_cut"))
    list->SetCutValue( regtbl.GetG4D("antimuon_cut"), "mu+" );
  if (regtbl.Has("tau_cut"))
    list->SetCutValue( regtbl.GetG4D("tau_cut"), "tau-" );
  if (regtbl.Has("antitau_cut"))
    list->SetCutValue( regtbl.GetG4D("antitau_cut"), "tau+" );
  if (regtbl.Has("piplus_cut"))
    list->SetCutValue( regtbl.GetG4D("piplus_cut"), "pi+" );
  if (regtbl.Has("piminus_cut"))
    list->SetCutValue( regtbl.GetG4D("piminus_cut"), "pi-" );
  if (regtbl.Has("pizero_cut"))
    list->SetCutValue( regtbl.GetG4D("pizero_cut"), "pi0" );
  if (regtbl.Has("proton_cut"))
    list->SetCutValue( regtbl.GetG4D("proton_cut"), "proton" );
  if (regtbl.Has("neutron_cut"))
    list->SetCutValue( regtbl.GetG4D("neutron_cut"), "neutron" );

  return;
}

G4ProductionCuts* PhysicsConstruction::LoadProductionCuts(std::string cutname) {

  std::cout << "PHY: --> Loading Custom Cuts : " << cutname << std::endl;
  DBTable regtbl = DB::Get()->GetTable("CUTS", cutname);

  G4ProductionCuts* cuts = new G4ProductionCuts();

  // Get a default
  G4double defaultcut = 0.1 * nm;
  if (regtbl.Has("default_cut")) defaultcut = regtbl.GetG4D("default_cut");

  cuts->SetProductionCut( defaultcut, "gamma"   );
  cuts->SetProductionCut( defaultcut, "e-"      );
  cuts->SetProductionCut( defaultcut, "e+"      );
  cuts->SetProductionCut( defaultcut, "mu-"     );
  cuts->SetProductionCut( defaultcut, "mu+"     );
  cuts->SetProductionCut( defaultcut, "tau-"    );
  cuts->SetProductionCut( defaultcut, "tau+"    );
  cuts->SetProductionCut( defaultcut, "pi+"     );
  cuts->SetProductionCut( defaultcut, "pi-"     );
  cuts->SetProductionCut( defaultcut, "pi0"     );
  cuts->SetProductionCut( defaultcut, "proton"  );
  cuts->SetProductionCut( defaultcut, "neutron" );

  // Get list of cuts/info for this region
  if (regtbl.Has("gamma_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("gamma_cut"), "gamma" );
  if (regtbl.Has("electron_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("electron_cut"), "e-" );
  if (regtbl.Has("positron_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("positron_cut"), "e+" );
  if (regtbl.Has("muon_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("muon_cut"), "mu-" );
  if (regtbl.Has("antimuon_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("antimuon_cut"), "mu+" );
  if (regtbl.Has("tau_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("tau_cut"), "tau-" );
  if (regtbl.Has("antitau_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("antitau_cut"), "tau+" );
  if (regtbl.Has("piplus_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("piplus_cut"), "pi+" );
  if (regtbl.Has("piminus_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("piminus_cut"), "pi-" );
  if (regtbl.Has("pizero_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("pizero_cut"), "pi0" );
  if (regtbl.Has("proton_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("proton_cut"), "proton" );
  if (regtbl.Has("neutron_cut"))
    cuts->SetProductionCut( regtbl.GetG4D("neutron_cut"), "neutron" );

  return cuts;
}

// -----------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
