// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "GeometryManager.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

GeometryManager *GeometryManager::fPrimary(0);

GeometryManager::GeometryManager(){
}

G4VPhysicalVolume* GeometryManager::ConstructAll(){

  std::cout << "**************************************************************" << std::endl;
  std::cout << "GEO: Building Geometry " << std::endl;
  std::vector<DBTable> tables_clone = DB::Get()->GetTableGroup("GEO");
  std::vector<DBTable>::iterator geo_iter = tables_clone.begin();

  int count = 0;
  while (tables_clone.size() > 0){

    for (geo_iter = tables_clone.begin(); geo_iter != tables_clone.end();){

      DBTable geo_tab = (*geo_iter);
      std::string geo_id = geo_tab.GetIndexName();

      // Check doesn't already exist
      if (HasVGeometry(geo_id)){
        std::cout << "Trying to add duplicate GEOM! "
                  << "INDEX : " << geo_id << std::endl;
        geo_tab.Print();
        throw;
      }

      // Check if mother already created
      int mother_status = MotherStatus(geo_tab);
      if (mother_status == 2){
        count++;
        ++geo_iter;
        continue; // If it hasn't skip for now
      }

      // If not enabled, skip.
      if (geo_tab.Has("enable") && !geo_tab.GetB("enable")){
        std::cout << "Disabling GEO : " << geo_id << std::endl;
        geo_iter = tables_clone.erase(geo_iter);
        continue;
      }

      // Build the geometry.
      VGeometry* geo_obj = GeoFactory::ConstructGeometry(geo_tab);
      fVGeometrys[geo_id] = geo_obj;
      fGeoIDs.push_back(geo_id);

      // Remove tabs
      geo_iter = tables_clone.erase(geo_iter);

    }
    if (count > 10000){
      std::cout << "Stuck in infinite detector loop" << std::endl;
      throw;
    }
  }

  std::cout << "**************************************************************" << std::endl;
  std::cout << "GEO: Building Geometry " << std::endl;
  tables_clone = DB::Get()->GetTableGroup("OPTICAL");
  geo_iter = tables_clone.begin();

  count = 0;
  while (tables_clone.size() > 0){

    for (geo_iter = tables_clone.begin(); geo_iter != tables_clone.end();){

      DBTable geo_tab = (*geo_iter);
      std::string geo_id = geo_tab.GetIndexName();

      // Check doesn't already exist
      if (HasVGeometry(geo_id)){
        std::cout << "Trying to add duplicate OPTICAL! "
                  << "INDEX : " << geo_id << std::endl;
        geo_tab.Print();
        throw;
      }

      // If not enabled, skip.
      if (geo_tab.Has("enable") && !geo_tab.GetB("enable")){
        std::cout << "Disabling OPTICAL : " << geo_id << std::endl;
        geo_iter = tables_clone.erase(geo_iter);
        continue;
      }

      // Build the geometry.
      VGeometry* geo_obj = GeoFactory::ConstructGeometry(geo_tab);
      fVGeometrys[geo_id] = geo_obj;
      fGeoIDs.push_back(geo_id);

      // Remove tabs
      geo_iter = tables_clone.erase(geo_iter);

    }
    if (count > 10000){
      std::cout << "Stuck in infinite detector loop" << std::endl;
      throw;
    }
  }

  fWorldVolume = fVGeometrys["world"]->GetPhysical();
  return fWorldVolume;
}


int GeometryManager::MotherStatus(DBTable geo_tab){
  if (geo_tab.Has("mother")){
    std::string mother = (geo_tab).GetS("mother");
    if (this->HasVGeometry(mother)){
      return 1;
    } else{
      return 2;
    }
  }
  return 0; // World has no mother
}

bool GeometryManager::HasVGeometry(std::string name){
  return (fVGeometrys.find(name) != fVGeometrys.end());
}

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
