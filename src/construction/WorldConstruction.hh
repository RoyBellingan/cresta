// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __CRESTA_WorldConstruction_HH__
#define __CRESTA_WorldConstruction_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// ---------------------------------------------------------------------------

/// Globally accessible Construction Factory for producing main run actions.
namespace WorldConstruction {
/// Function to load global world configuration
G4VUserDetectorConstruction* Build();
/// Function to build a world config from a table.
G4VUserDetectorConstruction* Build(DBTable table);
} // - namespace WorldConstruction

// ---------------------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
#endif
