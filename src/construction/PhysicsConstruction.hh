// Copyright 2018 P. Stowell, C. Steer, L. Thompson

/*******************************************************************************
*    This file is part of CRESTA.
*
*    CRESTA is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    CRESTA is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with CRESTA.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifndef __PhysicsFactory_HH__
#define __PhysicsFactory_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Construction { // CRESTA Construction Sub Namespace
// -----------------------------------------------------------------

/// Physics List Creator Name Space
namespace PhysicsConstruction{
  /// Main Global Physics Loader
  G4VUserPhysicsList* Build();
  /// Function to return a given physics list by table definition
  /// If no table provided, the global table definition is used.
  G4VUserPhysicsList* LoadPhysicsList(DBTable tbl);
  /// Load physics list from global table
  G4VUserPhysicsList* LoadPhysicsList();
  /// Set cuts for a physics list
  void SetPhysicsListCuts(std::string cutname, G4VUserPhysicsList* list);
  /// Get production cuts to apply to region.
  G4ProductionCuts* LoadProductionCuts(std::string cutname);
} // - namespace PhysicsConstruction

// -----------------------------------------------------------------
} // - namespace Construction
} // - namespace CRESTA
#endif
