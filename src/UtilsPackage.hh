#ifndef __UTIL_PACKAGE_HH__
#define __UTIL_PACKAGE_HH__
// HACK way to hide CORE includes in later packages and have the user not have to
// worry about it....
#include "utils/PoCAUtils.hh"

using namespace CRESTA::Utility;

#endif
