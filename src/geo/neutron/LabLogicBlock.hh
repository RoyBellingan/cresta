#ifndef __LabLogicBlock__HH__
#define __LabLogicBlock__HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

#include "geo/simple/GeoSolid.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

/// Simple solid box geometry object
class LabLogicBlock : public GeoSolid {
public:
  /// Create a LabLogicBlock from a table
  LabLogicBlock(DBTable table);

  /// Construct the solid volume for this geobox
  G4VSolid *ConstructSolidVolume(DBTable table);
};

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
