#include "LabLogicBlock.hh"

#include "G4Box.hh"
#include "geo/GeoUtils.hh"
#include "construction/MaterialsFactory.hh"
using namespace CRESTA::Construction;

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

LabLogicBlock::LabLogicBlock(DBTable table) {
  Construct(table);
}

G4VSolid* LabLogicBlock::ConstructSolidVolume(DBTable tbl) {

  std::string geo_id = tbl.GetIndexName();

  // Now setup object
  G4double worldX = 5.0*cm;
  G4double worldY = 5.0*cm;
  G4double worldZ = 2.5*cm;
  G4Box* geo_solid = new G4Box(geo_id, 0.5 * worldX, 0.5 * worldY, 0.5 * worldZ);

  return geo_solid;
}

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
