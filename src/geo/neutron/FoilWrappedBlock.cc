#include "FoilWrappedBlock.hh"

#include "G4Box.hh"
#include "geo/GeoUtils.hh"
#include "construction/MaterialsFactory.hh"
using namespace CRESTA::Construction;

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

FoilWrappedBlock::FoilWrappedBlock(DBTable table) {
  Construct(table);
}

void FoilWrappedBlock::Construct(DBTable table)
{
  SetID(table.GetIndexName());
  SetType("foilwrappedblock");
  std::string fName = table.GetIndexName();
  std::string fMotherName = table.GetS("mother");
  fMotherLogical = GEOUtils::GetMotherLogicalFromStore(table);

  //  --------------------------------------------------------------------------
  // Read Configs
  G4double shell_width = table.Has("shell_width") ?
    table.GetG4D("shell_width") : 0.2*mm;

  G4double coating_width = table.Has("coating_width") ?
    table.GetG4D("coating_width") : 0.2*mm;

  std::string shell_mat_name = table.Has("shell_material") ?
    table.GetS("shell_material") : "G4_B";

  std::string coating_mat_name = table.Has("coating_material") ?
    table.GetS("coating_material") : "G4_AIR";

  std::string block_mat_name = table.Has("block_material") ?
    table.GetS("block_material") : "LABLOGIC_SCINTILLATOR";

  G4String coating_sensitive = table.Has("coating_sensitive") ?
    table.GetS("coating_sensitive") : "";

  G4String block_sensitive = table.Has("block_sensitive") ?
    table.GetS("block_sensitive") : "";


  //  --------------------------------------------------------------------------
  // Make the wrapping material outer as the mother
  std::string shell_name = fName + "_shell";

  G4Box* shell_sol = new G4Box( shell_name, 0.5*5*cm + coating_width + shell_width,
				0.5*5*cm + coating_width + shell_width,
				0.5*1.5*cm + coating_width + shell_width);

  G4Material* shell_mat = MaterialsFactory::GetMaterial(shell_mat_name);

  G4LogicalVolume* shell_log = new G4LogicalVolume( shell_sol, shell_mat, shell_name );

  G4VisAttributes *shell_vis = MaterialsFactory::GetVisForMaterial(shell_mat_name);
  shell_log->SetVisAttributes(shell_vis);

  //  --------------------------------------------------------------------------
  // Make the coating material
  std::string coating_name = fName + "_coating";

  G4Box* coating_sol = new G4Box( coating_name,
				  0.5*5*cm + coating_width,
				  0.5*5*cm + coating_width,
				  0.5*1.5*cm + coating_width );

  G4Material* coating_mat = MaterialsFactory::GetMaterial(coating_mat_name);
  G4LogicalVolume* coating_log = new G4LogicalVolume( coating_sol, coating_mat, coating_name );

  G4VisAttributes *coating_vis = MaterialsFactory::GetVisForMaterial(coating_mat_name);
  coating_log->SetVisAttributes(coating_vis);

  G4VPhysicalVolume* coating_physical = new G4PVPlacement( new G4RotationMatrix(), G4ThreeVector(), coating_log, coating_name, shell_log, true, 0);

  if (!coating_sensitive.empty()){
    table.SetIndexName(coating_name);
    table.Set("sensitive", coating_sensitive);
    fCoatingSensitive = GeoSolid::ConstructSensitiveDetector(table, coating_log, coating_physical);
    fSensitive = fCoatingSensitive;
  }

  //  --------------------------------------------------------------------------
  // Make the main block
  std::string block_name = fName + "_block";

  G4Box* block_sol = new G4Box( block_name,
				0.5*5*cm,
				0.5*5*cm,
				0.5*1.5*cm );

  G4Material* block_mat = MaterialsFactory::GetMaterial(block_mat_name);

  G4LogicalVolume* block_log = new G4LogicalVolume( block_sol, block_mat, block_name );

  G4VisAttributes *block_vis = MaterialsFactory::GetVisForMaterial(block_mat_name);
  block_log->SetVisAttributes(block_vis);

  G4VPhysicalVolume* block_physical = new G4PVPlacement( new G4RotationMatrix(), G4ThreeVector(), block_log, block_name, coating_log, true, 0);

  if (!block_sensitive.empty()){
    table.SetIndexName(block_name);
    table.Set("sensitive",block_sensitive);
    fBlockSensitive = GeoSolid::ConstructSensitiveDetector(table, block_log, block_physical);
    fSensitive = fBlockSensitive;
  }

  // Do final assignment
  fLogical = shell_log;
  fPhysical = GeoSolid::ConstructPhysicalPlacement(table, fMotherLogical, fLogical);
}

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
