#ifndef __GEO_TUBS__HH__
#define __GEO_TUBS__HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "GeoSolid.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

/// Simple solid tubs geometry object
class GeoTubs : public GeoSolid {
public:
  /// Create a GeoTubs from a table
  GeoTubs(DBTable table);

  /// Construct the solid volume for this geobox
  G4VSolid *ConstructSolidVolume(DBTable table);
};

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
