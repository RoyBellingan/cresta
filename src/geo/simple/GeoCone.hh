#ifndef __GEO_CONS__HH__
#define __GEO_CONS__HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

#include "GeoSolid.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

/// Simple solid cone geometry object
class GeoCone : public GeoSolid {
public:
  /// Create a GeoCons from a table
  GeoCone(DBTable table);

  /// Construct the solid volume for this geobox
  G4VSolid *ConstructSolidVolume(DBTable table);
};

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
