#ifndef __GEO_PARA_HH__
#define __GEO_PARA_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "GeoSolid.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

/// Simple solid box geometry object
class GeoPara : public GeoSolid {
public:
  /// Create a GeoPara from a table
  GeoPara(DBTable table);

  /// Construct the solid volume for this geobox
  G4VSolid *ConstructSolidVolume(DBTable table);
};

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
