#ifndef __GEOSOLID_HH__
#define __GEOSOLID_HH__
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

/// Solid geometry object, building on the VGeometry template.
/// Contains all functions to produce logical and physical volumes
/// from a DBTable.
class GeoSolid : public VGeometry {
public:

  /// Empty Constructor
  GeoSolid();

  /// Function to actually create each volume from a table.
  void Construct(DBTable table);

  /// Create the solid volume for this objet. Children must implement.
  virtual G4VSolid *ConstructSolidVolume(DBTable /*table*/){ return 0; };

  /// Construct logical volume given materials etc in a table
  virtual G4LogicalVolume *ConstructLogicalVolume(DBTable table, G4VSolid* solid);

  /// Construct a physical volume placement given the settings in a table and the provided mother+logic
  virtual G4VPhysicalVolume* ConstructPhysicalVolume(DBTable table, G4LogicalVolume* mother, G4LogicalVolume* logic);

  /// Construct a sensitive detector from the table and attach to the logic
  virtual G4VSensitiveDetector* ConstructSensitiveDetector(DBTable table, G4LogicalVolume* logic, G4VPhysicalVolume* vol);

  /// Construct a physical volume placement given the settings in a table and the provided mother+logic
  virtual G4VPhysicalVolume* ConstructPhysicalReplica(DBTable table, G4LogicalVolume* mother, G4LogicalVolume* logic);

  /// Construct a physical volume placement given the settings in a table and the provided mother+logic
  virtual G4VPhysicalVolume* ConstructPhysicalPlacement(DBTable table, G4LogicalVolume* mother, G4LogicalVolume* logic);

  /// Construct a physical volume placement given the settings in a table and the provided mother+logic
  virtual G4VPhysicalVolume* ConstructPhysicalParametrisation(DBTable table, G4LogicalVolume* mother, G4LogicalVolume* logic);

protected:

  G4VSolid* fSolid; ///< Geant4 solid object for this geometry

};
// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
