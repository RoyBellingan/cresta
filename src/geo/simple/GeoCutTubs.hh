#ifndef __GEO_CUTTUBS_HH__
#define __GEO_CUTTUBS_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

#include "GeoSolid.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

/// Simple solid cut tubs geometry object
class GeoCutTubs : public GeoSolid {
public:
  /// Create a GeoCutTubs from a table
  GeoCutTubs(DBTable table);

  /// Construct the solid volume for this geobox
  G4VSolid *ConstructSolidVolume(DBTable table);
};

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
