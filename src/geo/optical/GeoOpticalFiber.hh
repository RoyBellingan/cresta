#ifndef __GEO_OPTICALFIBER_HH__
#define __GEO_OPTICALFIBER_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "geo/simple/GeoSolid.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

class GeoOpticalFiber : public GeoSolid {
public:
  /// Create a GeoOpticalFibre from a table
  GeoOpticalFiber(DBTable table);

  /// Construct for this geobox
  void Construct(DBTable table);
};

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
