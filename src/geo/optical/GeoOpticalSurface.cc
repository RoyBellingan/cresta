#include "GeoOpticalSurface.hh"

#include "G4Box.hh"
#include "geo/GeoUtils.hh"
#include "construction/MaterialsFactory.hh"
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"
using namespace CRESTA::Construction;

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

GeoOpticalSurface::GeoOpticalSurface(DBTable table) {
  Construct(table);
}

void GeoOpticalSurface::Construct(DBTable tbl) {

  std::string name           = tbl.GetIndexName();
  std::string surface_type   = tbl.GetS("surface");
  std::string surface_finish = tbl.GetS("finish");
  std::string surface_model  = tbl.GetS("model");
  G4OpticalSurface* wrapper  = new G4OpticalSurface(name);

  std::string outer_name = tbl.GetS("outer");
  std::string inner_name = tbl.GetS("inner");

  G4VPhysicalVolume* outer_log = GEOUtils::GetPhysicalFromStore(outer_name);
  G4VPhysicalVolume* inner_log = GEOUtils::GetPhysicalFromStore(inner_name);



  if (surface_type != "dielectric_LUT"){
    if (surface_type == "dielectric_metal") wrapper->SetType(dielectric_metal);
    else if (surface_type == "dielectric_dielectric") wrapper->SetType(dielectric_dielectric);
    else{
      std::cout << "Unknown 'surface' type : " << surface_type << std::endl;
      throw;
    }
    
    if (surface_finish == "polished") wrapper->SetFinish(polished);
    else if (surface_finish == "ground") wrapper->SetFinish(ground);
    else{
      std::cout << "Unknown 'finish' : " << surface_finish << std::endl;
      throw;
    }
    
    if (surface_model == "glisur") wrapper->SetModel(glisur);
    else{
      std::cout << "Unknown 'model' : " << surface_model << std::endl;
      throw;
    }

    G4MaterialPropertiesTable* mat = new G4MaterialPropertiesTable();
    std::vector<std::string> allowed_properties;
    allowed_properties.push_back("REFLECTIVITY");
    allowed_properties.push_back("EFFICIENCY");

    for (int i = 0; i < allowed_properties.size(); i++) {
      std::string name = allowed_properties[i];
      if (tbl.Has(name + "_Y")) {

	std::vector<G4double> yvals = tbl.GetVecG4D(name + "_Y");

	std::vector<G4double> xvals;
	if (tbl.Has(name + "_X")) xvals = tbl.GetVecG4D(name + "_X");
	else if (tbl.Has("PROPERTIES_X")) xvals = tbl.GetVecG4D("PROPERTIES_X");
	else {
	  std::cout << "NO X" << std::endl;
	  throw;
	}

	if (xvals.size() != yvals.size()) {
	  std::cout << "Material X Y must be equal in length" << std::endl;
	  throw;
	}
	mat->AddProperty(name.c_str(), &xvals[0], &yvals[0], xvals.size());

      } else if (tbl.Has(name)) {

	mat->AddConstProperty(name.c_str(), tbl.GetG4D(name));

      }
    }

    wrapper->SetMaterialPropertiesTable(mat);


  } else {
    wrapper->SetType(dielectric_LUT);
    wrapper->SetModel(LUT);

    if (surface_model == "polishedlumirrorair") wrapper->SetFinish(polishedlumirrorair);
    else if (surface_model == "polishedlumirrorglue") wrapper->SetFinish(polishedlumirrorglue);
    else if (surface_model == "polishedair") wrapper->SetFinish(polishedair);
    else if (surface_model == "polishedteflonair") wrapper->SetFinish(polishedteflonair);
    else if (surface_model == "polishedtioair") wrapper->SetFinish(polishedtioair);
    else if (surface_model == "polishedtyvekair") wrapper->SetFinish(polishedtyvekair);
    /*    else if (surface_model == "") wrapper->SetFinish();
    else if (surface_model == "") wrapper->SetFinish();
    else if (surface_model == "") wrapper->SetFinish();
    else if (surface_model == "") wrapper->SetFinish();
    else if (surface_model == "") wrapper->SetFinish();
    else if (surface_model == "") wrapper->SetFinish();
    else if (surface_model == "") wrapper->SetFinish();
    */




  }

  new G4LogicalBorderSurface(name, inner_log, outer_log, wrapper);

}

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
