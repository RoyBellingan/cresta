#include "GeoOpticalFiber.hh"

#include "G4Box.hh"
#include "geo/GeoUtils.hh"
#include "construction/MaterialsFactory.hh"
#include "detector/DetectorFactory.hh"
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4IntersectionSolid.hh"
#include <math.h>
using namespace CRESTA::Construction;

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

GeoOpticalFiber::GeoOpticalFiber(DBTable table) {
  Construct(table);
}

//Construct the whole thing
void GeoOpticalFiber::Construct(DBTable tbl) {

  //Reading out the table/setting parameters<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  //pysical fiber
  std::string name   = tbl.GetIndexName();
  G4double r_max     = tbl.GetG4D("r_max");
  G4double r_core    = r_max - (r_max*6)/100;//thickness is 3% of fiber diameter

  G4double r_min = 0.0;
  //G4double length    = tbl.GetG4D("length")*0.5;
  G4double r_tor     = tbl.GetG4D("r_tor");
  G4double phi_start = tbl.Has("phi_start") ? tbl.GetG4D("phi_start") : 0.0;
  G4double phi_delta = tbl.Has("phi_delta") ? tbl.GetG4D("phi_delta") : CLHEP::twopi;
  //G4double phi_start_tor     = -(length/2)/(r_max + r_tor);
  //G4double phi_delta_tor     = (length)/(r_max + r_tor);
  std::string cladding_mat   = tbl.GetS("cladding_material");
  std::string fiber_mat      = tbl.GetS("material");
  G4LogicalVolume* mother    = GEOUtils::GetMotherLogicalFromStore(tbl);

  //retrieveing rotation
  G4RotationMatrix* rotation = new G4RotationMatrix();
  const std::vector<double> &rotation_tbl = tbl.GetVecG4D("rotation");
  rotation->rotateX(rotation_tbl[0] * CLHEP::deg);
  rotation->rotateY(rotation_tbl[1] * CLHEP::deg);
  rotation->rotateZ(rotation_tbl[2] * CLHEP::deg);
  //retrieving position
  G4ThreeVector position(0.0,0.0,0.0);
  G4ThreeVector zeropos(0.0,0.0,0.0); //position that is always zero (for the fiber core)
  const std::vector<double> &position_tbl = tbl.GetVecG4D("position");
  position.setX(position_tbl[0]);
  position.setY(position_tbl[1]);
  position.setZ(position_tbl[2]);


  //coupling material<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  std::string coupling_mat = tbl.GetS("coupling_material");
  G4double coupling_t = tbl.Has("coupling_t") ? tbl.GetG4D("coupling_t")*0.5 : 1*mm;
  G4RotationMatrix* coup_rotation1 = new G4RotationMatrix();
  G4RotationMatrix* coup_rotation2 = new G4RotationMatrix();
  G4ThreeVector coup_position1(0.0,0.0,0.0);
  G4ThreeVector coup_position2(0.0,0.0,0.0);
  G4double phi_delta_coupling     = (coupling_t)/(r_max + r_tor);
  
  //pmt<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  std::string pmt1_enable = tbl.GetS("pmt1_enable");
  std::string pmt2_enable = tbl.GetS("pmt2_enable");
  G4double pmt_t = tbl.Has("pmt_t") ? tbl.GetG4D("pmt_t")*0.5 : 1*mm; //thickness of the pmt
  std::string pmt_mat = tbl.GetS("pmt_material");
  G4RotationMatrix* pmt_rotation1 = new G4RotationMatrix();
  G4RotationMatrix* pmt_rotation2 = new G4RotationMatrix();
  G4ThreeVector pmt_position1(0.0,0.0,0.0);
  G4ThreeVector pmt_position2(0.0,0.0,0.0);
  G4double phi_delta_pmt     = (pmt_t)/(r_max + r_tor);


  //adjusting lenght of fiber so that it can encompass pmt and coupling (children must be inside their mother volumes)
  G4double length    = tbl.GetG4D("length")*0.5 + pmt_t*2 + coupling_t*2;
  G4double phi_start_tor     = -(length)/(r_max + r_tor);
  G4double phi_delta_tor     = ((length)*2)/(r_max + r_tor);

  //optical surfaces<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  std::string surface_type   = tbl.Has("surface") ? tbl.GetS("surface") : "dielectric_dielectric";
  std::string surface_finish = tbl.Has("finish") ? tbl.GetS("finish") : "polished";
  std::string surface_model  = tbl.Has("model") ? tbl.GetS("model") : "glisur";
  G4OpticalSurface* wrapper  = new G4OpticalSurface(name);
  G4OpticalSurface* w_fiber_clad = new G4OpticalSurface(name);
  G4OpticalSurface* w_fiber_coup = new G4OpticalSurface(name);
  G4OpticalSurface* w_coup_pmt  = new G4OpticalSurface(name);

  std::string outer_name = tbl.GetS("mother");

  G4VPhysicalVolume* outer_physical = GEOUtils::GetPhysicalFromStore(outer_name);



  //construction fiber---------------------------------------------------------
  
  //depending on the r_tor radius, either a torus or a tub will be made
  //since only a part of the full torus is required, it is cut so that the length of the center curve is "length"
  //to achieve that, an intersection with a clyinder slice of the according angles is made

  //placeholder cladding
  G4VSolid* cladding_solid = new G4Tubs(name, 0.0, r_max, length, phi_start_tor, phi_delta_tor);
  //placehoder solid fiber (core)
  G4VSolid* fiber_solid = new G4Tubs(name, 0.0, r_core, length, phi_start_tor, phi_delta_tor);
  //placeholder coupling
  G4VSolid* coupling_solid1 = new G4Tubs(name, r_min, r_max, coupling_t, phi_start, phi_delta);
  G4VSolid* coupling_solid2 = new G4Tubs(name, r_min, r_max, coupling_t, phi_start, phi_delta);

  //placeholder pmt
  G4VSolid* pmt_solid1 = new G4Tubs(name, r_min, r_max, pmt_t, phi_start, phi_delta);
  G4VSolid* pmt_solid2 = new G4Tubs(name, r_min, r_max, pmt_t, phi_start, phi_delta);

  
  if (r_tor != 0){
    //cladding
    G4VSolid* claddingA = new G4Torus(name, r_min, r_max, r_tor, phi_start, phi_delta);
    G4VSolid* claddingB = new G4Tubs(name, 0.0, r_tor+r_max, r_max, phi_start_tor, phi_delta_tor);
    cladding_solid = new G4IntersectionSolid(name, claddingA, claddingB);

    //fiber is the core of the fiber, it starts and ends inside the cladding, before the coupling an pmt so that they don't overlap
    G4VSolid* solidA = new G4Torus(name, r_min, r_core, r_tor, phi_start, phi_delta);
    G4VSolid* solidB = new G4Tubs(name, 0.0, r_tor+r_core, r_max, phi_start_tor+ phi_delta_pmt + phi_delta_coupling, phi_delta_tor -phi_delta_pmt -phi_delta_coupling);
    fiber_solid = new G4IntersectionSolid(name, solidA, solidB);

    coupling_solid1 = new G4Torus(name, r_min, r_max, r_tor, phi_start_tor +phi_delta_pmt, phi_delta_coupling);
    coupling_solid2 = new G4Torus(name, r_min, r_max, r_tor, phi_start_tor + phi_delta_tor -phi_delta_pmt -phi_delta_coupling, phi_delta_coupling);

    pmt_solid1 = new G4Torus(name, r_min, r_max, r_tor, phi_start_tor, phi_delta_pmt);
    pmt_solid2 = new G4Torus(name, r_min, r_max, r_tor, phi_start_tor + phi_delta_tor -phi_delta_pmt, phi_delta_pmt);

    
  }
  
  else if (r_tor == 0){
    //pmt and coupling positions for tubs fiber
    pmt_position1.setZ(-length + pmt_t);
    coup_position1.setZ(-length +pmt_t*2 + coupling_t);
    pmt_position2.setZ(length -pmt_t);
    coup_position2.setZ(+length -pmt_t*2 - coupling_t);
  
    fiber_solid = new G4Tubs(name, r_min, r_core, length-2*pmt_t-2*coupling_t,
                    phi_start, phi_delta);
    cladding_solid  = new G4Tubs(name, r_min, r_max, length,
                    phi_start, phi_delta);

  }

  //cladding is mother volume of all the other parts
  G4Material* cladding_material = MaterialsFactory::GetMaterial(cladding_mat);
  G4VisAttributes *cladding_vis = MaterialsFactory::GetVisForMaterial(cladding_mat);
  G4LogicalVolume* cladding_logic = new G4LogicalVolume(cladding_solid, cladding_material, name);
  cladding_logic->SetVisAttributes(cladding_vis);
  
  G4VPhysicalVolume* cladding_physical = new G4PVPlacement(rotation, position, cladding_logic, name, mother, true, 0);


  //fiber also known as core
  G4Material* fiber_material = MaterialsFactory::GetMaterial(fiber_mat);
  G4VisAttributes *fiber_vis = MaterialsFactory::GetVisForMaterial(fiber_mat);
  G4LogicalVolume* fiber_logic = new G4LogicalVolume(fiber_solid, fiber_material, name);
  fiber_logic->SetVisAttributes(fiber_vis);
  //fiber is in mother
  G4VPhysicalVolume* fiber_physical = new G4PVPlacement(new G4RotationMatrix(), zeropos, fiber_logic, "core", cladding_logic, true, 0);




  
  //construction coupling agent------------------------------------------------
  G4Material* coupling_material = MaterialsFactory::GetMaterial(coupling_mat);
  G4VisAttributes *coupling_vis = MaterialsFactory::GetVisForMaterial(coupling_mat);

  G4LogicalVolume* coupling_logic1 = new G4LogicalVolume(coupling_solid1, coupling_material, name);
  coupling_logic1->SetVisAttributes(coupling_vis);
  G4LogicalVolume* coupling_logic2 = new G4LogicalVolume(coupling_solid2, coupling_material, name);
  coupling_logic2->SetVisAttributes(coupling_vis);

 
  //initially the coupling physicals were here



 
  //construction PMT----------------------------------------------------------
  G4Material* pmt_material = MaterialsFactory::GetMaterial(pmt_mat);
  G4VisAttributes *pmt_vis = MaterialsFactory::GetVisForMaterial(pmt_mat);
  
  G4LogicalVolume* pmt_logic1 = new G4LogicalVolume(pmt_solid1, pmt_material, name);
  pmt_logic1->SetVisAttributes(pmt_vis);
  G4LogicalVolume* pmt_logic2 = new G4LogicalVolume(pmt_solid2, pmt_material, name);
  pmt_logic2->SetVisAttributes(pmt_vis);



  //initially the pmt physicals were here, but I moved them down so that the condition for the wrappers and the sensitivity can exist in the same scope





  
  
  //Wrapper setup--------------------------------------------------------------
  //setting some things for the wrapper (G4OpticalSurface)
  wrapper->SetType(dielectric_dielectric);
  wrapper->SetFinish(polished);
  wrapper->SetModel(glisur);

  //material properties for the wrapper (G4OpticalSurface)
  G4MaterialPropertiesTable* mat = new G4MaterialPropertiesTable();
  std::vector<std::string> allowed_properties;
  allowed_properties.push_back("WLSTIMECONSTANT");
  allowed_properties.push_back("BIRKSCONSTANT");
  allowed_properties.push_back("FASTCOMPONENT");
  allowed_properties.push_back("SLOWCOMPONENT");
  allowed_properties.push_back("RINDEX");
  allowed_properties.push_back("ABSLENGTH");
  allowed_properties.push_back("SCINTILLATIONYIELD");
  allowed_properties.push_back("RESOLUTIONSCALE");
  allowed_properties.push_back("SLOWTIMECONSTANT");
  allowed_properties.push_back("FASTTIMECONSTANT");
  allowed_properties.push_back("YIELDRATIO");
  allowed_properties.push_back("BIRKSCONSTANT");
  allowed_properties.push_back("REFLECTIVITY");
  allowed_properties.push_back("EFFICIENCY");

  for (int i = 0; i < allowed_properties.size(); i++) {
    std::string name = allowed_properties[i];
    if (tbl.Has(name + "_Y")) {

      std::vector<G4double> yvals = tbl.GetVecG4D(name + "_Y");

      std::vector<G4double> xvals;
      if (tbl.Has(name + "_X")) xvals = tbl.GetVecG4D(name + "_X");
      else if (tbl.Has("PROPERTIES_X")) xvals = tbl.GetVecG4D("PROPERTIES_X");
      else {
	std::cout << "NO X in Fiber" << std::endl;
        throw;
      }

      if (xvals.size() != yvals.size()) {
	std::cout << "Material X Y must be equal in length" << std::endl;
        throw;
      }
      mat->AddProperty(name.c_str(), &xvals[0], &yvals[0], xvals.size());

    } else if (tbl.Has(name)) {

      mat->AddConstProperty(name.c_str(), tbl.GetG4D(name));

    }
  }

  wrapper->SetMaterialPropertiesTable(mat);

  //W_Fiber_Clad setup-------------------------------------------------------------
  //setting some things for the w_fiber_clad (G4OpticalSurface)
  w_fiber_clad->SetType(dielectric_dielectric);
  w_fiber_clad->SetFinish(polished);
  w_fiber_clad->SetModel(glisur);

  //material properties for the w_fiber_clad (G4OpticalSurface)
  G4MaterialPropertiesTable* matclad = new G4MaterialPropertiesTable();

  //the same list of allowed optical surface properties is used but in the table we're looking for thngs with a prefix of "FtoCl" to make sure only Fiber to cladding properties are called for this table. But they are added to the materialpropertiestable with the name from the allowed-property. This is in case GEANT4 needs these properties to have the exact name

  for (int i = 0; i < allowed_properties.size(); i++) {
    std::string name = allowed_properties[i];
    if (tbl.Has("FtoCl_" + name + "_Y")) {

      std::vector<G4double> yvals = tbl.GetVecG4D("FtoCl_" + name + "_Y");

      std::vector<G4double> xvals;
      if (tbl.Has("FtoCl_" + name + "_X")) xvals = tbl.GetVecG4D("FtoCl_" +name + "_X");
      else if (tbl.Has("FtoCl_PROPERTIES_X")) xvals = tbl.GetVecG4D("FtoCl_PROPERTIES_X");
      else {
	std::cout << "NO X in Fiber to Cladding" << std::endl;
        throw;
      }

      if (xvals.size() != yvals.size()) {
	std::cout << "Material X Y must be equal in length" << std::endl;
        throw;
      }
      matclad->AddProperty(name.c_str(), &xvals[0], &yvals[0], xvals.size());

    } else if (tbl.Has("FtoCl_" + name)) {

      matclad->AddConstProperty(name.c_str(), tbl.GetG4D("FtoCl_"+name));

    }
  }

  w_fiber_clad->SetMaterialPropertiesTable(matclad);
  
  //W_Fiber_Coup setup-------------------------------------------------------------
  //setting some things for the w_fiber_coup (G4OpticalSurface)
  w_fiber_coup->SetType(dielectric_dielectric);
  w_fiber_coup->SetFinish(polished);
  w_fiber_coup->SetModel(glisur);

  //material properties for the w_fiber_coup (G4OpticalSurface)
  G4MaterialPropertiesTable* matcoup = new G4MaterialPropertiesTable();

  //the same list of allowed optical surface properties is used but in the table we're looking for thngs with a prefix of "C_" to make sure only coupling properties are called for this table. But they are added to the materialpropertiestable with the name from the allowed-property. This is in case GEANT4 needs these properties to have the exact name

  for (int i = 0; i < allowed_properties.size(); i++) {
    std::string name = allowed_properties[i];
    if (tbl.Has("FtoC_" + name + "_Y")) {

      std::vector<G4double> yvals = tbl.GetVecG4D("FtoC_" + name + "_Y");

      std::vector<G4double> xvals;
      if (tbl.Has("FtoC_" + name + "_X")) xvals = tbl.GetVecG4D("FtoC_" +name + "_X");
      else if (tbl.Has("FtoC_PROPERTIES_X")) xvals = tbl.GetVecG4D("FtoC_PROPERTIES_X");
      else {
	std::cout << "NO X in Fiber to Coupling" << std::endl;
        throw;
      }

      if (xvals.size() != yvals.size()) {
	std::cout << "Material X Y must be equal in length" << std::endl;
        throw;
      }
      matcoup->AddProperty(name.c_str(), &xvals[0], &yvals[0], xvals.size());

    } else if (tbl.Has("FtoC_" + name)) {

      matcoup->AddConstProperty(name.c_str(), tbl.GetG4D("FtoC_"+name));

    }
  }

  w_fiber_coup->SetMaterialPropertiesTable(matcoup);

   //W_Coup_Pmt setup-------------------------------------------------------------
  //setting some things for the w_coup_pmt (G4OpticalSurface)
  w_coup_pmt->SetType(dielectric_dielectric);
  w_coup_pmt->SetFinish(polished);
  w_coup_pmt->SetModel(glisur);

  //material properties for the w_coup_pmt (G4OpticalSurface)
  G4MaterialPropertiesTable* matpmt = new G4MaterialPropertiesTable();

  //the same list of allowed optical surface properties is used but in the table we're looking for thngs with a prefix of "CtoP_" to make sure only coupling properties are called for this table. But they are added to the materialpropertiestable with the name from the allowed-property. This is in case GEANT4 needs these properties to have the exact name

  for (int i = 0; i < allowed_properties.size(); i++) {
    std::string name = allowed_properties[i];
    if (tbl.Has("CtoP_" + name + "_Y")) {

      std::vector<G4double> yvals = tbl.GetVecG4D("CtoP_" + name + "_Y");

      std::vector<G4double> xvals;
      if (tbl.Has("CtoP_" + name + "_X")) xvals = tbl.GetVecG4D("CtoP_" +name + "_X");
      else if (tbl.Has("CtoP_PROPERTIES_X")) xvals = tbl.GetVecG4D("CtoP_PROPERTIES_X");
      else {
	std::cout << "NO X in Coupling to PMT" << std::endl;
        throw;
      }

      if (xvals.size() != yvals.size()) {
	std::cout << "Material X Y must be equal in length" << std::endl;
        throw;
      }
      matpmt->AddProperty(name.c_str(), &xvals[0], &yvals[0], xvals.size());

    } else if (tbl.Has("CtoP_" + name)) {

      matpmt->AddConstProperty(name.c_str(), tbl.GetG4D("CtoP_"+name));

    }
  }

  w_coup_pmt->SetMaterialPropertiesTable(matpmt);

  //Making the optical surfaces------------------------------------------------
  //cladding to outside
  new G4LogicalBorderSurface(name, cladding_physical, outer_physical, wrapper);
  new G4LogicalBorderSurface(name, outer_physical, cladding_physical, wrapper);

  //fiber to cladding
  new G4LogicalBorderSurface(name, fiber_physical, cladding_physical, w_fiber_clad);
  new G4LogicalBorderSurface(name, cladding_physical, fiber_physical, w_fiber_clad);


  if (pmt1_enable == "1"){
    G4VPhysicalVolume* pmt_physical1 = new G4PVPlacement(pmt_rotation1, pmt_position1, pmt_logic1, "pmt1", cladding_logic, true, 0);
    G4VPhysicalVolume* coupling_physical1 = new G4PVPlacement(coup_rotation1,  coup_position1, coupling_logic1, "coupling1", cladding_logic, true, 0);
    //copied across from GeoSolid::ConstructSensitiveDetector and adapted to pmt1 and pmt2
    // If it does then create a new SD object
    std::string sensitive1 = tbl.GetS("sensitive");

    // First check detector not already loaded
    std::string sd1name = name +"_pmt1";
    VDetector* sd1 = Analysis::Get()->GetDetector(sd1name, true);
    if (!sd1){
      DBTable sd1tbl = DB::Get()->GetTable("DETECTOR", sensitive1);
      sd1tbl.SetIndexName(sd1name);
      sd1 = Detector::DetectorFactory::CreateSD(sd1tbl);
      Analysis::Get()->RegisterDetector(sd1);
    }

    // Assign the logical volume
    sd1->SetLogicalVolume(pmt_logic1, pmt_physical1);
    
    G4VSensitiveDetector* Sensitive1 = sd1;
    
    //END1
    //fiber to coupling
    new G4LogicalBorderSurface(name, fiber_physical, coupling_physical1, w_fiber_coup);
    new G4LogicalBorderSurface(name, coupling_physical1, fiber_physical, w_fiber_coup);

    //coupling to pmt
    new G4LogicalBorderSurface(name, coupling_physical1, pmt_physical1, w_coup_pmt);
    new G4LogicalBorderSurface(name, pmt_physical1, coupling_physical1, w_coup_pmt);
  }

  if (pmt2_enable == "1"){
    G4VPhysicalVolume* pmt_physical2 = new G4PVPlacement(pmt_rotation2, pmt_position2, pmt_logic2, "pmt2", cladding_logic, true, 0);
    G4VPhysicalVolume* coupling_physical2 = new G4PVPlacement(coup_rotation2, coup_position2, coupling_logic2, "coupling2", cladding_logic, true, 0);
  
    // If it does then create a new SD object
    std::string sensitive2 = tbl.GetS("sensitive");

    // First check detector not already loaded
    std::string sd2name = name +"_pmt2";
    VDetector* sd2 = Analysis::Get()->GetDetector(sd2name, true);
    if (!sd2){
      DBTable sd2tbl = DB::Get()->GetTable("DETECTOR", sensitive2);
      sd2tbl.SetIndexName(sd2name);
      sd2 = Detector::DetectorFactory::CreateSD(sd2tbl);
      Analysis::Get()->RegisterDetector(sd2);
    }

    // Assign the logical volume
    sd2->SetLogicalVolume(pmt_logic2, pmt_physical2);
    
    G4VSensitiveDetector* Sensitive2 = sd2;
    
    //END2
    //fiber to coupling
    new G4LogicalBorderSurface(name, fiber_physical, coupling_physical2, w_fiber_coup);
    new G4LogicalBorderSurface(name, coupling_physical2, fiber_physical, w_fiber_coup);
    
    //coupling to pmt
    new G4LogicalBorderSurface(name, coupling_physical2, pmt_physical2, w_coup_pmt);
    new G4LogicalBorderSurface(name, pmt_physical2, coupling_physical2, w_coup_pmt);
  }
  
}

  


// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
