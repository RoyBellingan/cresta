#ifndef __CRESTA_GeoFactory_HH__
#define __CRESTA_GeoFactory_HH__
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

// namespace GeoFactory
namespace GeoFactory {
VGeometry* ConstructGeometry(DBTable table);
} // - namespace GeoFactory

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
#endif
