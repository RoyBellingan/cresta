#include "geo/GeoFactory.hh"

#include "simple/GeoBox.hh"
#include "simple/GeoTubs.hh"
#include "simple/GeoCone.hh"
#include "simple/GeoEllipticalTube.hh"
#include "neutron/FoilWrappedBlock.hh"
#include "neutron/LabLogicBlock.hh"
#include "optical/GeoOpticalSurface.hh"
#include "optical/GeoOpticalFiber.hh"

#include "cadmesh/GeoCADMesh.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Geometry { // CRESTA Geomtry Sub Namespace
// ---------------------------------------------------------------------------

VGeometry* GeoFactory::ConstructGeometry(DBTable table) {

  std::string type = table.GetS("type");
  std::cout << "GEO: Constructing " << type << " : " << table.GetIndexName() <<  std::endl;

  // Core Constructors
  if (type.compare("box") == 0) return new GeoBox(table);
  if (type.compare("tubs") == 0) return new GeoTubs(table);
  if (type.compare("cons") == 0) return new GeoCone(table);
  if (type.compare("eliptube") == 0) return new GeoEllipticalTube(table);
  if (type.compare("foilwrappedblock") == 0) return new FoilWrappedBlock(table);
  if (type.compare("lablogicblock") == 0) return new LabLogicBlock(table);
  if (type.compare("opticalsurface") == 0) return new GeoOpticalSurface(table);
  if (type.compare("opticalfiber") == 0) return new GeoOpticalFiber(table);

  // Cadmesh Constructors
#ifdef __USE_CADMESH__
  if (type.compare("cadmesh") == 0) return new GeoCADMesh(table);
#endif

  // Dynamic Constructors
  VGeometry* geo = DynamicObjectLoader::Get()->ConstructDynamicGeometry(table);
  if (geo) return geo;

  // Fail if none loaded!
  std::cout << "Failed to Construct Geometry" << std::endl;
  throw;
  return 0;
}

// ---------------------------------------------------------------------------
} // - namespace Geometry
} // - namespace CRESTA
