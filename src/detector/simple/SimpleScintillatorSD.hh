#ifndef __SimpleScintillatorSD_HH__
#define __SimpleScintillatorSD_HH__

#include "ScintillatorHit.hh"

#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "UtilsPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// \brief Simple Scintillator Sensitive Detector
///
/// Measures all energy deposited inside a volume.
/// Assumption is that all hits above a certain threshold contribute to
/// the "scintillation" measurement. No actual light is propogated.
class SimpleScintillatorSD : public VDetector {
public:

  /// Main constructor from a database table
  SimpleScintillatorSD(DBTable table);
  /// Simple C++ constructor
  SimpleScintillatorSD(std::string name, std::string id, bool autoprocess = true, bool autosave = true);
  /// Destructor
  ~SimpleScintillatorSD() {};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Initialize the Hit Collection
  void Initialize(G4HCofThisEvent*HCE);


  /// Getter functions
  inline ScintillatorHitsCollection* GetHC() { return fHitsCollection; };
  inline G4int GetHCID() { return fHCID; };


protected:

  ScintillatorHitsCollection* fHitsCollection;
  G4int fHCID;
  G4double fEfficiency;

};

// ---------------------------------------------------------------------------

/// \brief True Muon Processor Object
///
/// By default this is created alongside the true muon
/// tracker object, so the info is automatically added
/// to the TTree.
class SimpleScintillatorProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  SimpleScintillatorProcessor(SimpleScintillatorSD* trkr, bool autosave = true);
  /// Destructor
  ~SimpleScintillatorProcessor() {};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Reset all information
  void Reset();

  // Getter Functions
  inline G4double       GetTime() { return fTime; };
  inline G4double       GetPosX() { return fPosX;  };
  inline G4double       GetPosY() { return fPosY;  };
  inline G4double       GetPosZ() { return fPosZ;  };
  inline G4double       GetVolPosX() { return fVolPosX;  };
  inline G4double       GetVolPosY() { return fVolPosY;  };
  inline G4double       GetVolPosZ() { return fVolPosZ;  };
  inline G4double       GetEDep() { return fEdep;  };
  inline G4double       GetThetaXZ() { return fThetaXZ;  };
  inline G4double       GetThetaYZ() { return fThetaYZ;  };


protected:

  SimpleScintillatorSD* fDetector; ///< Pointer to associated to SD
  G4int fHCID;

  bool fSave; ///< Flag to save event info automatically

  G4double fTime;
  G4double fEdep;
  G4double fPosX;
  G4double fPosY;
  G4double fPosZ;
  G4double fVolPosX;
  G4double fVolPosY;
  G4double fVolPosZ;
  int fPDG;
  G4double fThetaXZ;
  G4double fThetaYZ;
  G4int fId;
  int fTrg;
  int fIdIndex;

  int fTimeIndex;
  int fPDGIndex;
  int fPosXIndex;
  int fPosYIndex;
  int fPosZIndex;
  int fVolPosXIndex;
  int fVolPosYIndex;
  int fVolPosZIndex;
  int fThXZIndex;
  int fThYZIndex;
  int fEdepIndex;

};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
