#include "ScintillatorHit.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

G4ThreadLocal G4Allocator<ScintillatorHit>* ScintillatorHitAllocator = 0;

ScintillatorHit::ScintillatorHit() : G4VHit()
{}

ScintillatorHit::~ScintillatorHit()
{}

ScintillatorHit::ScintillatorHit(const ScintillatorHit &right)
    : G4VHit() {
    fId = right.fId;
    fTime = right.fTime;
    fEdep = right.fEdep;
    fType = right.fType;
    fPos = right.fPos;
    fRot = right.fRot;
    // fPLogV = right.fPLogV;
}

const ScintillatorHit& ScintillatorHit::operator=(const ScintillatorHit &right)
{
    fId = right.fId;
    fTime = right.fTime;
    fEdep = right.fEdep;
    fType = right.fType;
    fPos = right.fPos;
    fRot = right.fRot;
    //fPLogV = right.fLogV;
    return *this;
}

void ScintillatorHit::SetAngles(G4ThreeVector dir) {
    fThetaXZ = std::atan2(dir.getZ(), dir.getX());
    fThetaYZ = std::atan2(dir.getZ(), dir.getY());
}

int ScintillatorHit::operator==(const ScintillatorHit &/*right*/) const
{
    return 0;
}

void ScintillatorHit::Draw()
{
    G4VVisManager* pVVisManager = Analysis::Get()->GetVisManager();
    if (pVVisManager)
    {
        G4Square square(fPos);
        square.SetScreenSize(6);
        square.SetFillStyle(G4Square::filled);
        G4Colour colour(1., 0., 0.);
        G4VisAttributes attribs(colour);
        square.SetVisAttributes(attribs);
        pVVisManager->Draw(square);
    }
}

void ScintillatorHit::Print()
{
    G4cout << "  Hodoscope[" << fId << "] " << fTime / ns << " (nsec)" << G4endl;
    G4cout << G4endl;
    G4cout << G4endl;
    G4cout << G4endl;
    G4cout << "  Hit : " << particle_type << " Time : " << fTime / ns << " (nsec)" << " Pos. : " << fPos / m << G4endl;
    G4cout << G4endl;
    G4cout << G4endl;
    G4cout << G4endl;
}

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
