#ifndef __GridScintillatorSD_HH__
#define __GridScintillatorSD_HH__
#include "ScintillatorHit.hh"
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// True Muon Tracker Detector Object :
/// Saves the highest momentrum muon track
/// that entered a detector logical voluem
class GridScintillatorSD : public VDetector {
public:

  /// Main constructor from a database table
  GridScintillatorSD(DBTable table);
  /// Grid C++ constructor
  GridScintillatorSD(std::string name, std::string id, bool autoprocess = true, bool autosave = true);
  /// Destructor
  ~GridScintillatorSD() {};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Initialize the Hit Collection
  void Initialize(G4HCofThisEvent*HCE);


  /// Getter functions
  inline ScintillatorHitsCollection* GetHC() { return fHitsCollection; };
  inline G4int GetHCID() { return fHCID; };


protected:

  ScintillatorHitsCollection* fHitsCollection;
  G4int fHCID;
  G4double fEfficiency;

};

/// GridScintillator Processor Object :
/// By default this is created alongside the true muon
/// tracker object, so the info is automatically added
/// to the TTree.
class GridScintillatorProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  GridScintillatorProcessor(GridScintillatorSD* trkr, bool autosave = true);
  /// Destructor
  ~GridScintillatorProcessor() {};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Reset all information
  void Reset();

protected:

  GridScintillatorSD* fDetector; ///< Pointer to associated to SD
  G4int fHCID;

  bool fSave; ///< Flag to save event info automatically

  std::vector<float> fVoxelE;
  std::vector<float> fVoxelX;
  std::vector<float> fVoxelY;
  std::vector<float> fVoxelZ;
  std::vector<float> fVoxelT;
  std::vector<int> fVoxelP;
};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
