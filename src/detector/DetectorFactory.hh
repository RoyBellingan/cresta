#ifndef __DetectorFactory_HH__
#define __DetectorFactory_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

// namespace
namespace CRESTA {
namespace Detector {

/// Detector Factory used to create SD from tables
namespace DetectorFactory {
/// Function to create detector objects from tables
VDetector* CreateSD(DBTable tbl);
} // - namespace DetectorFactory

} // - namespace Detector
} // - namespace CRESTA
#endif
