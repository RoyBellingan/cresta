#include "DetectorFactory.hh"

#include "true/TrueMuonTracker.hh"
#include "true/TrueInTracker.hh"
#include "true/TrueParticleKiller.hh"
#include "true/TrueMuonSaveAndKill.hh"
#include "simple/SimpleScintillatorSD.hh"
#include "simple/GridScintillatorSD.hh"
#include "drift/LongDriftSD.hh"
#include "tomography/TargetMuonInOutTracker.hh"
#include "neutron/NeutronSD.hh"
#include "neutron/AlphaDetector.hh"
#include "neutron/ProtonDetector.hh"
#include "neutron/TrueNeutronTracker.hh"
#include "true/TrueParticleVector.hh"

#include "optical/PhotonSD.hh"

namespace CRESTA {
namespace Detector {

VDetector* DetectorFactory::CreateSD(DBTable tbl) {

  // Retrieve the table that matches this sensitive
  std::string type = tbl.GetS("type");

  // Now Search for different types
  if (type.compare("truemuon") == 0) return new TrueMuonTracker(tbl);
  if (type.compare("scintillator") == 0) return new SimpleScintillatorSD(tbl);
  if (type.compare("gridscintillator") == 0) return new GridScintillatorSD(tbl);
  if (type.compare("neutron") == 0) return new NeutronSD(tbl);
  if (type.compare("longdrift") == 0) return new LongDriftSD(tbl);
  if (type.compare("muoninouttracker") == 0) return new TargetMuonInOutTracker(tbl);
  if (type.compare("trueinabsorber") == 0) return new TrueInAbsorber(tbl);
  if (type.compare("photonsd") == 0) return new PhotonSD(tbl);
  if (type.compare("trueparticlekiller") == 0) return new TrueParticleKiller(tbl);
  if (type.compare("truemuonsaveandkill") == 0) return new TrueMuonSaveAndKill(tbl);
  if (type.compare("alpha") == 0) return new AlphaDetector(tbl);
  if (type.compare("proton") == 0) return new ProtonDetector(tbl);
  if (type.compare("trueneutron") == 0) return new TrueNeutronTracker(tbl);
  if (type.compare("trueparticlevector") == 0) return new TrueParticleVector(tbl);

  VDetector* proc = DynamicObjectLoader::Get()->ConstructDynamicDetector(tbl);
  if (proc) return proc;

  // Check we didn't get to here
  std::cout << "Failed to Create New SD : '" << type << "'" << std::endl;
  throw;
  return 0;
}

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
