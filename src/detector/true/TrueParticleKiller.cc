#include "TrueParticleKiller.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

TrueParticleKiller::TrueParticleKiller(DBTable tbl):
  VDetector(tbl.GetIndexName(), "truemuon")
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

}

TrueParticleKiller::TrueParticleKiller(std::string name, std::string id, bool autoprocess, bool autosave):
  VDetector(name, id)
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

}

void TrueParticleKiller::ResetState() {
  VDetector::ResetState();
}

G4bool TrueParticleKiller::ProcessHits(G4Step* step, G4TouchableHistory* /*touch*/) {

  // Get only muons
  G4Track* track = step->GetTrack();
  track->SetTrackStatus(fKillTrackAndSecondaries);

  return true;
}

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
