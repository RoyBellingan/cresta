#ifndef __TrueParticleKiller_HH__
#define __TrueParticleKiller_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// \brief TrueParticle Killer
///
/// Destroys any particles that enter this volume. Use to make kill boxes.
class TrueParticleKiller : public VDetector {
public:

  /// Main constructor from a database table
  TrueParticleKiller(DBTable table);
  /// Simple C++ constructor
  TrueParticleKiller(std::string name, std::string id, bool autoprocess=true, bool autosave=true);
  /// Destructor
  ~TrueParticleKiller(){};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
