#ifndef __TrueInAbsorber_HH__
#define __TrueInAbsorber_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// True In Tracker SD. Keeps Track of all incoming particles into a SD
class TrueInAbsorber : public VDetector {
public:

  /// Main constructor from a database table
  TrueInAbsorber(DBTable table);
  /// Simple C++ constructor
  TrueInAbsorber(std::string name, std::string id, bool autoprocess=true, bool autosave=true);
  /// Destructor
  ~TrueInAbsorber(){};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

  // Get Functions
  inline int GetNParticles()  { return particle_pdg.size(); };

  inline std::vector<int>&     GetParticlePDG() { return particle_pdg; };
  inline std::vector<double>&  GetParticleT()   { return particle_t;   };
  inline std::vector<double>&  GetParticleE()   { return particle_E;   };
  inline std::vector<double>&  GetParticlePX()  { return particle_px;  };
  inline std::vector<double>&  GetParticlePY()  { return particle_py;  };
  inline std::vector<double>&  GetParticlePZ()  { return particle_pz;  };
  inline std::vector<double>&  GetParticleX()   { return particle_x;   };
  inline std::vector<double>&  GetParticleY()   { return particle_y;   };
  inline std::vector<double>&  GetParticleZ()   { return particle_z;   };

protected:

  std::vector<int>     particle_pdg;

  std::vector<double>  particle_t;
  std::vector<double>  particle_E;

  std::vector<double>  particle_px;
  std::vector<double>  particle_py;
  std::vector<double>  particle_pz;

  std::vector<double>  particle_x;
  std::vector<double>  particle_y;
  std::vector<double>  particle_z;


};
// ---------------------------------------------------------------------------

/// \brief TrueInAbsorber Processor
///
/// Saves the measured data from a TrueInAbsorber Detector to disk.
class TrueInProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  TrueInProcessor(TrueInAbsorber* trkr, bool autosave=true);

  /// Destructor
  ~TrueInProcessor(){};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

protected:

  TrueInAbsorber* fTracker; ///< Pointer to associated tracker SD
  bool fSave;

};
// ---------------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
