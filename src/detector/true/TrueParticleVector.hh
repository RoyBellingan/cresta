#ifndef __TrueParticleVector_HH__
#define __TrueParticleVector_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// \brief TrueParticleVector Detector
///
/// Measures the average true muon trajectory of all muons entering a volume.
/// Assumption is that only muon passes through, with minimal scattering.
class TrueParticleVector : public VDetector {
public:

  /// Main constructor from a database table
  TrueParticleVector(DBTable table);
  /// Simple C++ constructor
  TrueParticleVector(std::string name, std::string id, bool autoprocess=true, bool autosave=true);
  /// Destructor
  ~TrueParticleVector(){};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

  // Getter Functions
  inline std::vector<float>& GetParticleXPos(){ return fParticleXPos; }
  inline std::vector<float>& GetParticleYPos(){ return fParticleYPos; }
  inline std::vector<float>& GetParticleZPos(){ return fParticleZPos; }
  inline std::vector<float>& GetParticleEdep(){ return fParticleEdep; }
  inline std::vector<float>& GetParticleEpar(){ return fParticleEpar; }

  inline std::vector<int>& GetParticlePDG(){ return fParticlePDG; }
  inline int GetNHits(){ return fParticleHits; }

protected:

  std::vector<float> fParticleXPos;
  std::vector<float> fParticleYPos;
  std::vector<float> fParticleZPos;
  std::vector<float> fParticleEpar;
  std::vector<float> fParticleEdep;
  
  std::vector<int> fParticlePDG;
  int fParticleHits;
};

/// \brief TrueParticleVector Processor
///
/// Saves the measured data from a TrueParticleVector Detector to disk.
class TrueParticleVectorProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  TrueParticleVectorProcessor(TrueParticleVector* trkr, bool autosave=true);
  /// Destructor
  ~TrueParticleVectorProcessor(){};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Draw function
  void DrawEvent();

  void Reset();

protected:

  TrueParticleVector* fTracker; ///< Pointer to associated tracker SD

  bool fSave; ///< Flag to save event info automatically

  std::vector<float> fParticleXPos;
  std::vector<float> fParticleYPos;
  std::vector<float> fParticleZPos;
  std::vector<float> fParticleEpar;
  std::vector<float> fParticleEdep;
  std::vector<int> fParticlePDG;

  int fParticleXPosIndex; 
  int fParticleYPosIndex;
  int fParticleZPosIndex;
  int fParticlePDGIndex;
  
  


};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
