#include "TrueParticleVector.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

TrueParticleVector::TrueParticleVector(DBTable tbl):
  VDetector(tbl.GetIndexName(), "truemuon")
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

  // By default also include the auto processor
  if (!tbl.Has("processor") or tbl.GetI("processor") > 0) {
    Analysis::Get()->RegisterProcessor(new TrueParticleVectorProcessor(this));
  }
}

TrueParticleVector::TrueParticleVector(std::string name, std::string id, bool autoprocess, bool autosave):
  VDetector(name, id)
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

  // By default also include the auto processor
  if (autoprocess) {
    Analysis::Get()->RegisterProcessor(new TrueParticleVectorProcessor(this, autosave));
  }
}

void TrueParticleVector::ResetState() {
  VDetector::ResetState();
  fParticleXPos.clear();
  fParticleYPos.clear();
  fParticleZPos.clear();
  fParticlePDG.clear();
  fParticleHits = 0;
}

G4bool TrueParticleVector::ProcessHits(G4Step* step, G4TouchableHistory* /*touch*/) {

  // // Don't save tracks if no energy left in the detector
  // G4double edep = step->GetTotalEnergyDeposit();
  // fTotEDep += edep;
  // if (edep <= 0.) return false;

  // Get only muons
  G4Track* track = step->GetTrack();
  int steppdg = track->GetParticleDefinition()->GetPDGEncoding();

  // Get the step+1 inside the detector
  G4StepPoint* steppoint = step->GetPostStepPoint();
  G4double steptime = steppoint->GetGlobalTime();
  G4ThreeVector steppos = steppoint->GetPosition();
  G4ThreeVector stepmom = track->GetMomentum();

  G4double edep = step->GetTotalEnergyDeposit();    
  G4double epar = track->GetKineticEnergy();

  fParticleXPos.push_back(steppos[0]);
  fParticleYPos.push_back(steppos[1]);
  fParticleZPos.push_back(steppos[2]);
  fParticlePDG.push_back(steppdg);
  fParticleEdep.push_back(edep);
  fParticleEpar.push_back(epar);

  fParticleHits += 1;

  return true;
}


//------------------------------------------------------------------
TrueParticleVectorProcessor::TrueParticleVectorProcessor(TrueParticleVector* trkr, bool autosave) :
  VProcessor(trkr->GetID()), fSave(autosave)
{
  fTracker = trkr;
}

bool TrueParticleVectorProcessor::BeginOfRunAction(const G4Run* /*run*/) {

  std::string tableindex = GetID();
  std::cout << "DET: Registering TrueParticleVectorProcessor NTuples " << tableindex << std::endl;

  G4AnalysisManager* man = G4AnalysisManager::Instance();

  // Fill index energy
  fParticleXPosIndex = man ->CreateNtupleFColumn(tableindex + "_x",fParticleXPos);
  fParticleYPosIndex = man ->CreateNtupleFColumn(tableindex + "_y",fParticleYPos);
  fParticleZPosIndex = man ->CreateNtupleFColumn(tableindex + "_z",fParticleZPos);
  fParticlePDGIndex = man ->CreateNtupleIColumn(tableindex + "_pdg",fParticlePDG);
  man ->CreateNtupleFColumn(tableindex + "_edep", fParticleEdep);
  man ->CreateNtupleFColumn(tableindex + "_epar", fParticleEpar);


  return true;
}

void TrueParticleVectorProcessor::Reset(){
  fParticleXPos.clear();
  fParticleYPos.clear();
  fParticleZPos.clear();
  fParticlePDG.clear();
  fParticleEdep.clear();
  fParticleEpar.clear();
}

bool TrueParticleVectorProcessor::ProcessEvent(const G4Event* /*event*/) {

  // Register Trigger State
  fHasInfo = true;
  fTime    = 0;
  fEnergy  = 0;

  std::vector<float> tempx = fTracker->GetParticleXPos();
  std::vector<float> tempy = fTracker->GetParticleYPos();
  std::vector<float> tempz = fTracker->GetParticleZPos();
  std::vector<int> temppdg = fTracker->GetParticlePDG();
  std::vector<float> tempedep = fTracker->GetParticleEdep();
  std::vector<float> tempepar = fTracker->GetParticleEpar();


  for (int i = 0; i  < fTracker->GetNHits(); i++){
    fParticleXPos.push_back( tempx[i] );
    fParticleYPos.push_back( tempy[i] );
    fParticleZPos.push_back( tempz[i] );
    fParticlePDG.push_back( temppdg[i] );
    fParticleEdep.push_back( tempedep[i] );
    fParticleEpar.push_back( tempepar[i] );
  }

  G4AnalysisManager* man = G4AnalysisManager::Instance();
  // Auto filled vectors
  //  man->FillNtupleDColumn(fParticleXPosIndex);
//  man->FillNtupleDColumn(fParticleYPosIndex);
//  man->FillNtupleDColumn(fParticleZPosIndex);

  
  return true;
}

void TrueParticleVectorProcessor::DrawEvent(){
}

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
