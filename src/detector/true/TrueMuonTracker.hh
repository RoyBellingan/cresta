#ifndef __TrueMuonTracker_HH__
#define __TrueMuonTracker_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// \brief TrueMuonTracker Detector
///
/// Measures the average true muon trajectory of all muons entering a volume.
/// Assumption is that only muon passes through, with minimal scattering.
class TrueMuonTracker : public VDetector {
public:

  /// Main constructor from a database table
  TrueMuonTracker(DBTable table);
  /// Simple C++ constructor
  TrueMuonTracker(std::string name, std::string id, bool autoprocess=true, bool autosave=true);
  /// Destructor
  ~TrueMuonTracker(){};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

  // Getter Functions
  inline G4double      GetMuonTime(){ return fMuonTime; };
  inline G4ThreeVector  GetMuonMom(){ return fMuonMom;  };
  inline G4ThreeVector  GetMuonPos(){ return fMuonPos;  };
  inline G4double       GetTotEDep(){ return fTotEDep;  };
  inline int            GetMuonPDG(){ return fMuonPDG;  };

protected:

  G4double      fMuonTime; ///< HM Muon Step Time
  G4ThreeVector fMuonMom;  ///< HM Muon Mom Vector
  G4ThreeVector fMuonPos;  ///< HM Muon Pos Vector
  G4double      fTotEDep;  ///< Total Energy Deposited this event
  int           fMuonPDG;  ///< Muon PDG Code
  int           fNMuonHits; //< Number of deposits

};

/// \brief TrueMuonTracker Processor
///
/// Saves the measured data from a TrueMuonTracker Detector to disk.
class TrueMuonProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  TrueMuonProcessor(TrueMuonTracker* trkr, bool autosave=true);
  /// Destructor
  ~TrueMuonProcessor(){};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Draw function
  void DrawEvent();

protected:

  TrueMuonTracker* fTracker; ///< Pointer to associated tracker SD

  bool fSave; ///< Flag to save event info automatically

  int fMuonTimeIndex; ///< Time Ntuple Index
  int fMuonMomXIndex; ///< MomX Ntuple Index
  int fMuonMomYIndex; ///< MomY Ntuple Index
  int fMuonMomZIndex; ///< MomZ Ntuple Index
  int fMuonPosXIndex; ///< PosX Ntuple Index
  int fMuonPosYIndex; ///< PosY Ntuple Index
  int fMuonPosZIndex; ///< PosZ Ntuple Index
  int fMuonPDGIndex;  ///< MPDG Ntuple Index

};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
