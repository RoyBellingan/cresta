#ifndef __PhotonSD_HH__
#define __PhotonSD_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

#include "PhotonHit.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// \brief Photon SD.
///
/// Manually saves and kills all
/// photons entering a sensitive detector.
class PhotonSD : public VDetector {
public:

  /// Main constructor from a database table
  PhotonSD(DBTable table);
  /// Destructor
  ~PhotonSD() {};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

  /// Save the events
  G4TrackStatus ManuallyProcessHits(const G4Step* aStep, G4TouchableHistory* touch);

  /// Initialize the Hit Collection
  void Initialize(G4HCofThisEvent*HCE);

  /// Get the Local Hit Collection
  inline PhotonHitsCollection* GetHC() { return fHitsCollection; };
  /// Get detector hit collection index.
  inline G4int GetHCID() { return fHCID; };

protected:

  PhotonHitsCollection* fHitsCollection;   ///< Local photon hit collection
  G4int fHCID; ///< Hit collection index

};

/// Photon Processor Object
/// Saves 2D vector of photon energies and times
class PhotonProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  PhotonProcessor(PhotonSD* trkr, bool autosave = true);
  /// Destructor
  ~PhotonProcessor() {};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Draw function
  void DrawEvent();

  void Reset();

protected:
  std::vector<double> fPhotonE; ///< Local vector of saved photon energies.
  std::vector<double> fPhotonT; ///< Local vector of saved photon arrival times.

  std::vector<double> fPhotonX;
  std::vector<double> fPhotonY;
  std::vector<double> fPhotonZ;

  std::vector<double> fPhotonVX;
  std::vector<double> fPhotonVY;
  std::vector<double> fPhotonVZ;

  PhotonSD* fDetector; ///< Pointer to associated tracker SD
  int fHCID; ///< Hit collection ID
};

// ---------------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
