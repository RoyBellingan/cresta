#include "AlphaDetector.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

AlphaDetector::AlphaDetector(DBTable tbl):
  VDetector(tbl.GetIndexName(), "alphadetector")
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

  // By default also include the auto processor
  if (!tbl.Has("processor") or tbl.GetI("processor") > 0) {
    Analysis::Get()->RegisterProcessor(new AlphaProcessor(this));
  }
}

void AlphaDetector::ResetState() {
  VDetector::ResetState();
  fAlphaTime = 0.0;
  fAlphaMom = G4ThreeVector();
  fAlphaPos = G4ThreeVector();
  fAlphaPDG = -999;
  fTotEDep = 0.0;
  fNAlphaHits = 0;
}

G4bool AlphaDetector::ProcessHits(G4Step* step, G4TouchableHistory* /*touch*/) {

  // // Don't save tracks if no energy left in the detector
  G4double edep = step->GetTotalEnergyDeposit();
  fTotEDep += edep;
  if (edep <= 0.) return false;

  // Get only alphas
  G4Track* track = step->GetTrack();
  int steppdg = track->GetParticleDefinition()->GetPDGEncoding();
  bool is_alpha = (steppdg == 1000020040);
  if (!is_alpha) return true;

  // Get the step+1 inside the detector
  G4StepPoint* steppoint = step->GetPostStepPoint();
  G4double steptime = steppoint->GetGlobalTime();
  G4ThreeVector steppos = steppoint->GetPosition();
  G4ThreeVector stepmom = track->GetMomentum();

  // Keep tallying the number of hits the alpha causes
  fNAlphaHits++;

  // Only save the first alpha we see
  if (fNAlphaHits > 1) return true;
  fAlphaPos  = steppos;
  fAlphaMom  = stepmom;
  fAlphaTime = steptime;
  fAlphaPDG  = steppdg;

  return true;
}


//------------------------------------------------------------------
AlphaProcessor::AlphaProcessor(AlphaDetector* trkr, bool autosave) :
  VProcessor(trkr->GetID()), fSave(autosave)
{
  fTracker = trkr;
}

bool AlphaProcessor::BeginOfRunAction(const G4Run* /*run*/) {

  std::string tableindex = GetID();
  std::cout << "DET: Registering AlphaProcessor NTuples " << tableindex << std::endl;

  G4AnalysisManager* man = G4AnalysisManager::Instance();

  // Fill index energy
  fAlphaEnergyIndex = man ->CreateNtupleDColumn(tableindex + "_E");
  fAlphaTimeIndex = man ->CreateNtupleDColumn(tableindex + "_t");
  fAlphaMomXIndex = man ->CreateNtupleDColumn(tableindex + "_px");
  fAlphaMomYIndex = man ->CreateNtupleDColumn(tableindex + "_py");
  fAlphaMomZIndex = man ->CreateNtupleDColumn(tableindex + "_pz");
  fAlphaPosXIndex = man ->CreateNtupleDColumn(tableindex + "_x");
  fAlphaPosYIndex = man ->CreateNtupleDColumn(tableindex + "_y");
  fAlphaPosZIndex = man ->CreateNtupleDColumn(tableindex + "_z");
  fAlphaPDGIndex  = man ->CreateNtupleIColumn(tableindex + "_pdg");

  return true;
}

bool AlphaProcessor::ProcessEvent(const G4Event* /*event*/) {

  // Register Trigger State
  fHasInfo = fTracker->GetAlphaPDG() != -999;
  fTime    = fTracker->GetAlphaTime();
  fEnergy  = fTracker->GetTotEDep();

  if (fHasInfo) {
    // Fill alpha vectors
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    man->FillNtupleDColumn(fAlphaTimeIndex, fTracker->GetAlphaTime());
    man->FillNtupleDColumn(fAlphaEnergyIndex, fTracker->GetTotEDep());
    man->FillNtupleDColumn(fAlphaMomXIndex, fTracker->GetAlphaMom().x() / MeV);
    man->FillNtupleDColumn(fAlphaMomYIndex, fTracker->GetAlphaMom().y() / MeV);
    man->FillNtupleDColumn(fAlphaMomZIndex, fTracker->GetAlphaMom().z() / MeV);
    man->FillNtupleDColumn(fAlphaPosXIndex, fTracker->GetAlphaPos().x() / m);
    man->FillNtupleDColumn(fAlphaPosYIndex, fTracker->GetAlphaPos().y() / m);
    man->FillNtupleDColumn(fAlphaPosZIndex, fTracker->GetAlphaPos().z() / m);
    man->FillNtupleIColumn(fAlphaPDGIndex , fTracker->GetAlphaPDG());
    return true;
  } else {
    // Set Ntuple to defaults
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    man->FillNtupleDColumn(fAlphaTimeIndex, -999.);
    man->FillNtupleDColumn(fAlphaEnergyIndex, -999.);
    man->FillNtupleDColumn(fAlphaMomXIndex, -999.);
    man->FillNtupleDColumn(fAlphaMomYIndex, -999.);
    man->FillNtupleDColumn(fAlphaMomZIndex, -999.);
    man->FillNtupleDColumn(fAlphaPosXIndex, -999.);
    man->FillNtupleDColumn(fAlphaPosYIndex, -999.);
    man->FillNtupleDColumn(fAlphaPosZIndex, -999.);
    man->FillNtupleIColumn(fAlphaPDGIndex,  fTracker->GetAlphaPDG() );
    return false;
  }
}

void AlphaProcessor::DrawEvent(){
}

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
