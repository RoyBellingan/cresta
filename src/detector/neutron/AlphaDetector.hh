#ifndef __AlphaDetector_HH__
#define __AlphaDetector_HH__

#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace

/// True Alpha Tracker Detector Object :
/// Saves the highest momentrum muon track
/// that entered a detector logical voluem
class AlphaDetector : public VDetector {
public:

  /// Main constructor from a database table
  AlphaDetector(DBTable table);
  /// Destructor
  ~AlphaDetector(){};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

  // Getter Functions
  inline G4double       GetAlphaTime() { return fAlphaTime; };
  inline G4ThreeVector  GetAlphaMom()  { return fAlphaMom;  };
  inline G4ThreeVector  GetAlphaPos()  { return fAlphaPos;  };
  inline G4double       GetTotEDep()   { return fTotEDep;   };
  inline int            GetAlphaPDG()  { return fAlphaPDG;  };

protected:

  G4double      fAlphaTime; ///< HM Alpha Step Time
  G4ThreeVector fAlphaMom;  ///< HM Alpha Mom Vector
  G4ThreeVector fAlphaPos;  ///< HM Alpha Pos Vector
  G4double      fTotEDep;  ///< Total Energy Deposited this event
  int           fAlphaPDG;  ///< Alpha PDG Code as a cross-check
  int           fNAlphaHits; //< Number of energy deposits in the detector

};

/// True Alpha Processor Object :
/// By default this is created alongside the true muon
/// tracker object, so the info is automatically added
/// to the TTree.
class AlphaProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  AlphaProcessor(AlphaDetector* trkr, bool autosave=true);
  /// Destructor
  ~AlphaProcessor(){};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Draw function
  void DrawEvent();

  // inline double GetEDep(){ return fEDep; };

protected:

  AlphaDetector* fTracker; ///< Pointer to associated tracker SD

  bool fSave; ///< Flag to save event info automatically

  int fAlphaTimeIndex; ///< Time Ntuple Index
  int fAlphaEnergyIndex; ///< Time Ntuple Index
  int fAlphaMomXIndex; ///< MomX Ntuple Index
  int fAlphaMomYIndex; ///< MomY Ntuple Index
  int fAlphaMomZIndex; ///< MomZ Ntuple Index
  int fAlphaPosXIndex; ///< PosX Ntuple Index
  int fAlphaPosYIndex; ///< PosY Ntuple Index
  int fAlphaPosZIndex; ///< PosZ Ntuple Index
  int fAlphaPDGIndex;  ///< MPDG Ntuple Index

};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
