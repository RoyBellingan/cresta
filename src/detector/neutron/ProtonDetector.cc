#include "ProtonDetector.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

ProtonDetector::ProtonDetector(DBTable tbl):
  VDetector(tbl.GetIndexName(), "alphadetector")
{
  std::cout << "DET: Creating new " << GetType()
            << " detector : " << GetID() << std::endl;

  // Set initial state
  ResetState();

  // By default also include the auto processor
  if (!tbl.Has("processor") or tbl.GetI("processor") > 0) {
    Analysis::Get()->RegisterProcessor(new ProtonProcessor(this));
  }
}

void ProtonDetector::ResetState() {
  VDetector::ResetState();
  fProtonTime = 0.0;
  fProtonMom = G4ThreeVector();
  fProtonPos = G4ThreeVector();
  fProtonPDG = -999;
  fTotEDep = 0.0;
  fNProtonHits = 0;
}

G4bool ProtonDetector::ProcessHits(G4Step* step, G4TouchableHistory* /*touch*/) {

  // // Don't save tracks if no energy left in the detector
  G4double edep = step->GetTotalEnergyDeposit();
  fTotEDep += edep;
  if (edep <= 0.) return false;

  // Get only protons
  G4Track* track = step->GetTrack();
  int steppdg = track->GetParticleDefinition()->GetPDGEncoding();
  bool is_proton = (steppdg == 2212);
  if (!is_proton) return true;

  // Get the step+1 inside the detector
  G4StepPoint* steppoint = step->GetPostStepPoint();
  G4double steptime = steppoint->GetGlobalTime();
  G4ThreeVector steppos = steppoint->GetPosition();
  G4ThreeVector stepmom = track->GetMomentum();

  // Keep tallying the number of hits the proton causes
  fNProtonHits++;

  // Only save the first proton we see
  if (fNProtonHits > 1) return true;
  fProtonPos  = steppos;
  fProtonMom  = stepmom;
  fProtonTime = steptime;
  fProtonPDG  = steppdg;

  return true;
}


//------------------------------------------------------------------
ProtonProcessor::ProtonProcessor(ProtonDetector* trkr, bool autosave) :
  VProcessor(trkr->GetID()), fSave(autosave)
{
  fTracker = trkr;
}

bool ProtonProcessor::BeginOfRunAction(const G4Run* /*run*/) {

  std::string tableindex = GetID();
  std::cout << "DET: Registering ProtonProcessor NTuples " << tableindex << std::endl;

  G4AnalysisManager* man = G4AnalysisManager::Instance();

  // Fill index energy
  fProtonEnergyIndex = man ->CreateNtupleDColumn(tableindex + "_E");
  fProtonTimeIndex = man ->CreateNtupleDColumn(tableindex + "_t");
  fProtonMomXIndex = man ->CreateNtupleDColumn(tableindex + "_px");
  fProtonMomYIndex = man ->CreateNtupleDColumn(tableindex + "_py");
  fProtonMomZIndex = man ->CreateNtupleDColumn(tableindex + "_pz");
  fProtonPosXIndex = man ->CreateNtupleDColumn(tableindex + "_x");
  fProtonPosYIndex = man ->CreateNtupleDColumn(tableindex + "_y");
  fProtonPosZIndex = man ->CreateNtupleDColumn(tableindex + "_z");
  fProtonPDGIndex  = man ->CreateNtupleIColumn(tableindex + "_pdg");

  return true;
}

bool ProtonProcessor::ProcessEvent(const G4Event* /*event*/) {

  // Register Trigger State
  fHasInfo = fTracker->GetProtonPDG() != -999;
  fTime    = fTracker->GetProtonTime();
  fEnergy  = fTracker->GetTotEDep();

  if (fHasInfo) {
    // Fill proton vectors
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    man->FillNtupleDColumn(fProtonTimeIndex, fTracker->GetProtonTime());
    man->FillNtupleDColumn(fProtonEnergyIndex, fTracker->GetTotEDep());
    man->FillNtupleDColumn(fProtonMomXIndex, fTracker->GetProtonMom().x() / MeV);
    man->FillNtupleDColumn(fProtonMomYIndex, fTracker->GetProtonMom().y() / MeV);
    man->FillNtupleDColumn(fProtonMomZIndex, fTracker->GetProtonMom().z() / MeV);
    man->FillNtupleDColumn(fProtonPosXIndex, fTracker->GetProtonPos().x() / m);
    man->FillNtupleDColumn(fProtonPosYIndex, fTracker->GetProtonPos().y() / m);
    man->FillNtupleDColumn(fProtonPosZIndex, fTracker->GetProtonPos().z() / m);
    man->FillNtupleIColumn(fProtonPDGIndex , fTracker->GetProtonPDG());
    return true;
  } else {
    // Set Ntuple to defaults
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    man->FillNtupleDColumn(fProtonTimeIndex, -999.);
    man->FillNtupleDColumn(fProtonEnergyIndex, -999.);
    man->FillNtupleDColumn(fProtonMomXIndex, -999.);
    man->FillNtupleDColumn(fProtonMomYIndex, -999.);
    man->FillNtupleDColumn(fProtonMomZIndex, -999.);
    man->FillNtupleDColumn(fProtonPosXIndex, -999.);
    man->FillNtupleDColumn(fProtonPosYIndex, -999.);
    man->FillNtupleDColumn(fProtonPosZIndex, -999.);
    man->FillNtupleIColumn(fProtonPDGIndex,  fTracker->GetProtonPDG() );
    return false;
  }
}

void ProtonProcessor::DrawEvent(){
}

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
