#ifndef __ProtonDetector_HH__
#define __ProtonDetector_HH__

#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace

/// True Proton Tracker Detector Object :
/// Saves the highest momentrum muon track
/// that entered a detector logical voluem
class ProtonDetector : public VDetector {
public:

  /// Main constructor from a database table
  ProtonDetector(DBTable table);
  /// Destructor
  ~ProtonDetector(){};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

  // Getter Functions
  inline G4double       GetProtonTime() { return fProtonTime; };
  inline G4ThreeVector  GetProtonMom()  { return fProtonMom;  };
  inline G4ThreeVector  GetProtonPos()  { return fProtonPos;  };
  inline G4double       GetTotEDep()   { return fTotEDep;   };
  inline int            GetProtonPDG()  { return fProtonPDG;  };

protected:

  G4double      fProtonTime; ///< HM Proton Step Time
  G4ThreeVector fProtonMom;  ///< HM Proton Mom Vector
  G4ThreeVector fProtonPos;  ///< HM Proton Pos Vector
  G4double      fTotEDep;  ///< Total Energy Deposited this event
  int           fProtonPDG;  ///< Proton PDG Code as a cross-check
  int           fNProtonHits; //< Number of energy deposits in the detector

};

/// True Proton Processor Object :
/// By default this is created alongside the true muon
/// tracker object, so the info is automatically added
/// to the TTree.
class ProtonProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  ProtonProcessor(ProtonDetector* trkr, bool autosave=true);
  /// Destructor
  ~ProtonProcessor(){};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Draw function
  void DrawEvent();

  // inline double GetEDep(){ return fEDep; };

protected:

  ProtonDetector* fTracker; ///< Pointer to associated tracker SD

  bool fSave; ///< Flag to save event info automatically

  int fProtonTimeIndex; ///< Time Ntuple Index
  int fProtonEnergyIndex; ///< Time Ntuple Index
  int fProtonMomXIndex; ///< MomX Ntuple Index
  int fProtonMomYIndex; ///< MomY Ntuple Index
  int fProtonMomZIndex; ///< MomZ Ntuple Index
  int fProtonPosXIndex; ///< PosX Ntuple Index
  int fProtonPosYIndex; ///< PosY Ntuple Index
  int fProtonPosZIndex; ///< PosZ Ntuple Index
  int fProtonPDGIndex;  ///< MPDG Ntuple Index

};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
