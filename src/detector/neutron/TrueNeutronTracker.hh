#ifndef __TrueNeutronTracker_HH__
#define __TrueNeutronTracker_HH__

#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "construction/MaterialsFactory.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Detector { // CRESTA Detector Sub Namespace

/// True Neutron Tracker Detector Object :
/// Saves the highest momentrum muon track
/// that entered a detector logical voluem
class TrueNeutronTracker : public VDetector {
public:

  /// Main constructor from a database table
  TrueNeutronTracker(DBTable table);
  /// Simple C++ constructor
  TrueNeutronTracker(std::string name, std::string id, bool autoprocess=true, bool autosave=true);
  /// Destructor
  ~TrueNeutronTracker(){};

  /// Main processing. Looks for highest momentum track.
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);

  /// Reset all information
  void ResetState();

  // Getter Functions
  inline G4double       GetNeutronTime(){ return fNeutronTime; };
  inline G4ThreeVector  GetNeutronMom() { return fNeutronMom;  };
  inline G4ThreeVector  GetNeutronPos() { return fNeutronPos;  };
  inline G4double       GetTotEDep()    { return fTotEDep;     };
  inline int            GetNeutronPDG() { return fNeutronPDG;  };
  inline G4double       GetNeutronKE()  { return fNeutronKE;   };
protected:

  G4double      fNeutronTime;  ///< HM Neutron Step Time
  G4ThreeVector fNeutronMom;   ///< HM Neutron Mom Vector
  G4ThreeVector fNeutronPos;   ///< HM Neutron Pos Vector
  G4double      fTotEDep;      ///< Total Energy Deposited this event
  int           fNeutronPDG;   ///< Neutron PDG Code
  int           fNNeutronHits; ///< Number of deposits
  G4double      fNeutronKE;    ///< KE Deposit
  bool fUpward;
};

/// True Neutron Processor Object :
/// By default this is created alongside the true muon
/// tracker object, so the info is automatically added
/// to the TTree.
class TrueNeutronProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  TrueNeutronProcessor(TrueNeutronTracker* trkr, bool autosave=true);
  /// Destructor
  ~TrueNeutronProcessor(){};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Draw function
  void DrawEvent();

protected:

  TrueNeutronTracker* fTracker; ///< Pointer to associated tracker SD

  bool fSave; ///< Flag to save event info automatically

  int fNeutronTimeIndex; ///< Time Ntuple Index
  int fNeutronMomXIndex; ///< MomX Ntuple Index
  int fNeutronMomYIndex; ///< MomY Ntuple Index
  int fNeutronMomZIndex; ///< MomZ Ntuple Index
  int fNeutronPosXIndex; ///< PosX Ntuple Index
  int fNeutronPosYIndex; ///< PosY Ntuple Index
  int fNeutronPosZIndex; ///< PosZ Ntuple Index
  int fNeutronPDGIndex;  ///< MPDG Ntuple Index
  int fNeutronKinEIndex; ///< KinE Ntuple Index

};

//------------------------------------------------------------------
} // - namespace Detector
} // - namespace CRESTA
#endif
