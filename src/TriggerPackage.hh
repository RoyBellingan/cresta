#ifndef __TRIGGER_PACKAGE_HH__
#define __TRIGGER_PACKAGE_HH__
// HACK way to hide DB includes in later packages and have the user not have to
// worry about....
#include "trigger/TriggerFactory.hh"
#include "trigger/simple/CoincidenceTrigger.hh"
#include "trigger/simple/SimpleTrigger.hh"
using namespace CRESTA::Trigger;
#endif
