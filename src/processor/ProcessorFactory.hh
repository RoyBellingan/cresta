#ifndef __PROCESSOR_FACTORY_HH__
#define __PROCESSOR_FACTORY_HH__
#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "DetectorPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Processor { // CRESTA Detector Sub Namespace
// ---------------------------------------------------------------------------

/// Detector Factory used to create SD from tables
namespace ProcessorFactory {
/// Function to create detector objects from tables
VProcessor* Construct(DBTable tbl);
/// Function to generate all possible triggers loaded by the user tables
void ConstructProcessors();
} // - namespace PROCESSOR FACTORY

} // - namespace Processor
} // - namespace CRESTA
#endif
