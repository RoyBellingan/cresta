#ifndef __TrueMuonPoCA_HH__
#define __TrueMuonPoCA_HH__

#include "G4Headers.hh"
#include "DBPackage.hh"
#include "CorePackage.hh"
#include "DetectorPackage.hh"

namespace CRESTA { // Main CRESTA Namespace
namespace Processor { // CRESTA Detector Sub Namespace

/// True Muon Processor Object :
/// By default this is created alongside the true muon
/// tracker object, so the info is automatically added
/// to the TTree.
class TrueMuonPoCAProcessor : public VProcessor {
public:

  /// Processor can only be created with an associated
  /// tracker object.
  TrueMuonPoCAProcessor(DBTable tbl);

  /// Destructor
  ~TrueMuonPoCAProcessor(){};

  /// Setup Ntuple entries
  bool BeginOfRunAction(const G4Run* run);

  /// Process the information the tracker recieved for this event
  bool ProcessEvent(const G4Event* event);

  /// Draw in geometry
  void DrawEvent();


protected:

  TrueMuonTracker* fTrackerA; ///< Pointer to associated tracker A SD
  TrueMuonTracker* fTrackerB; ///< Pointer to associated tracker B SD
  G4double fDistance;
  G4double fAngle;
  G4ThreeVector fPoCA;
  G4double fAngleThreshold; ///< Limit to avoid straight tracks.

  bool fSave; ///< Flag to save event info automatically

  int fPoCATimeIndex; ///< Time Ntuple Index
  int fPoCAPosXIndex; ///< PosX Ntuple Index
  int fPoCAPosYIndex; ///< PosY Ntuple Index
  int fPoCAPosZIndex; ///< PosZ Ntuple Index
  int fPoCADistIndex; ///< Distance at PoCA
  int fPoCAAnglIndex; ///< Angle at PoCA

};

// ---------------------------------------------------------------------------
} // - namespace Processor
} // - namespace CRESTA
#endif
