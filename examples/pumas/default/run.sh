
# Dump binary
ln -sf ../../external/pumas/lib/ ./lib
ln -sf ../../../external/pumas-materials/ materials
pumas_load materials/mdf/examples/standard.xml materials/dedx/muon/ dump

# Run straight
pumas_straight 100 10 1000
