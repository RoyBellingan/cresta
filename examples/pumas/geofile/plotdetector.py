from ROOT import *
import sys

infile = TFile(sys.argv[1],"READ")
infile.ls()
detectorevents = infile.Get("detectorevents")
detectorevents.Show(0)
hist = TH2D("hist","hist",30,-1.0,1.0,30,-1.0,1.0)
for event in detectorevents:
    scale = sqrt(event.tracker_vx*event.tracker_vx + event.tracker_vy*event.tracker_vy + event.tracker_vz*event.tracker_vz)
    if scale == 0: continue
    hist.Fill( event.tracker_vx/scale, event.tracker_vy/scale, event.pumas_fw)


gStyle.SetPalette(kDarkBodyRadiator)
hist.Draw("COLZ")
hist.GetZaxis().SetRangeUser(0.0,1.0)
hist.SetTitle("Ratio;dx;dy;Total Weights")
gPad.Update()

gPad.Update()
raw_input("WAIT")
