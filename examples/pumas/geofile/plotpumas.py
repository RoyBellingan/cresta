from ROOT import *
import sys

infile = TFile(sys.argv[1],"READ")
infile.ls()
detectorevents = infile.Get("detectorevents")
detectorevents.Show(0)
hist = TH2D("hist","hist",30,-1.0,1.0,30,-1.0,1.0)
for event in detectorevents:
    scale = sqrt(event.pumas_dx*event.pumas_dx + event.pumas_dy*event.pumas_dy + event.pumas_dz*event.pumas_dz)
    if scale == 0: continue
    hist.Fill( event.pumas_dx/scale, event.pumas_dy/scale, event.pumas_fw)


gStyle.SetPalette(kDarkBodyRadiator)
hist.Draw("COLZ")
hist.GetZaxis().SetRangeUser(0.0,1.0)
hist.SetTitle("Ratio;dx;dy;Total Weights")
gPad.Update()

gPad.Update()
raw_input("WAIT")
