
# Dump binary
ln -sf ../../external/pumas/lib/ ./lib
ln -sf ../../../external/pumas-materials/ materials
pumas_load materials/mdf/examples/standard.xml materials/dedx/muon/ dump

# Run straight
cosmicraysim -g detector.geo -n 10000 -o withtargets
