// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  flux: "parrallel",
  world: "cresta",
  action: "cresta",
  physics: "QGSP_BERT"
}

// ---------------------------------------------------
// World Geometry : 3 x 3 x 3 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["3.*m", "3.0*m", "2.*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

// ---------------------------------------------------
// Flux Generator Source Location : Default is Parrallel
{
  name: "ParrallelBeam",
  index: "config",
  energy_min: "100000*eV"
  energy_max: "1000000*eV"
  direction: ["0.0","0.0","-1.0"]
  particle_id: "gamma"
}

{
  name: "FLUX",
  index: "source_box",
  size: ["0.1*m", "0.1*m", "0.05*m"],
  position: ["0.0","0.0", "+40.0*cm"],
}

//--------------------------------------------
// Main Box Geometry, placed inside mother WORLD

{
  name: "DETECTOR"
  index: "scintillator"
  type: "scintillator"
  efficiency: 0.85
}

{
  name: "GEO",
  index: "tracker",
  type: "box",

  mother: "world",

  material: "G4_POLYSTYRENE"

  size: ["40*cm","80*cm","40*cm"],

  position: ["0.0","0.0","0.0"],
  
  color: [0.0,0.0,1.0]

  sensitive: "scintillator"
}

