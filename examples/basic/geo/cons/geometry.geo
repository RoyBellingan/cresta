// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  flux: "cry",
  world: "cresta",
  action: "cresta",
  physics: "QGSP_BERT"
}

// ---------------------------------------------------
// World Geometry : 3 x 3 x 3 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["3.*m", "3.0*m", "2.*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

// ---------------------------------------------------
// Flux Generator Source Location : Default is Shukla
{
  name: "FLUX",
  index: "source_box",
  size: ["3.0*m", "3.0*m", "0.05*m"],
  position: ["0.0","0.0", "0.975*m"],
}

//--------------------------------------------
// Main Box Geometry, placed inside mother WORLD
{
  name: "GEO",
  index: "tracker",
  type: "cons",

  mother: "world",

  material: "G4_POLYSTYRENE",

  r_max1: "30*cm",
  size_z: "20*cm",

  // r_min1 : "0.0"
  // r_min2 : "0.0"
  // r_max2 : "0.0"
  // phi_start : "0.0"
  // phi_end : "2.0*3.1414"

  position: ["0.0","0.0","0.0"],
}


