
# This example propogates 1000 cosmic ray CRY flux particles through a 30m world.
# Physics list is changed each time using the GLOBAL config table.
cosmicraysim -g QGSP_BERT.geo -o QGSP_BERT_sample -n 1000
cosmicraysim -g QGSP_BERT_HP.geo -o QGSP_BERT_HP_sample -n 1000
cosmicraysim -g Shielding.geo -o Shielding_sample -n 1000

