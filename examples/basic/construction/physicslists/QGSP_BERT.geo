// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  world: "cresta"
  action: "cresta"
  flux: "cry",
  physics: "QGSP_BERT",
  batchcommands: "",
}

// ---------------------------------------------------
// World Geometry : 20 x 20 x 30 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["30.*m", "30.0*m", "2.*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

// ---------------------------------------------------
// Flux Generator Source Location : Default is Shukla
{
  name: "FLUX",
  index: "source_box",
  size: ["30.0*m", "30.0*m", "0.05*m"],
  position: ["0.0","0.0", "0.975*m"],
}
