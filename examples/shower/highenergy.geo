// ---------------------------------------------------
// Global Config
{
  name: "GLOBAL",
  index: "config",
  world: "cresta",
  flux: "gaussianbeamsource",
  physics: "Shielding",
  batchcommands: "",
  action: "cresta"
}

{
  name: "VARIABLE"
  index: "config"
  IONZ: "26"
  IONA: "52"
}

{
  name: "GaussianBeamSource",
  index: "config",
  direction: ["0.0","0.0","-1.0"]
  position: ["0*m","0.0","+0.45*m"]
  fwhmposition: ["0.0*cm","0.0*cm","0.0"]
  particle_energy_pdf: "1"
  particle_energy_min: "15000.0*GeV"
  particle_energy_max: "15001.0*GeV"
  particle_ids: ["generic_ion_0"]
  generic_ion_0_Z: "IONZ"
  generic_ion_0_A: "IONA"
  generic_ion_0_E: "0*MeV"
  particle_probs: ["1.0"]
}

// ---------------------------------------------------
// World Geometry : 20 x 20 x 30 AIR
// Then air and carbon base
{
  name: "GEO",
  index: "world",
  material: "G4_AIR",
  size: ["0.15*m", "30.0*m", "0.9*m"],
  type: "box",
  color: [1.0,1.0,1.0],
}

{
  name: "DETECTOR"
  index: "trueparticlevector"
  type: "trueparticlevector"
}

{
  name: "GEO",
  index: "box",
  mother: "world"
  material: "G4_POLYSTYRENE",
  size: ["0.15*m", "30.0*m", "0.9*m"],
  type: "box",
  color: [1.0,1.0,1.0],
  sensitive: "trueparticlevector"
}