from ROOT import *
import sys



hist = TH2D("hist","hist",600,-3.0,3.0,4000,-20.0,20.0)
filename = sys.argv[1]

infile = TFile(filename + "/" + filename + ".0.0.root","READ")
infile.ls()

events = infile.Get("detectorevents")
events.Show(0)

for event in events:
    print "EVENT"
    for i in range(event.box_x.size()):
        if i % 1000 == 0: print i
        x = event.box_x[i]/1000.0
        z = event.box_z[i]/1000.0
        e = event.box_edep[i]

        #hist.Fill(x,z,1.0)
        hist.Fill(x,z,e)
        #continue

        for j in range(10):
            hist.Fill(x + gRandom.Gaus(0.0,0.01), z + gRandom.Gaus(0.0,0.01), e)
            
        offx = gRandom.Gaus(0.0,0.03)
        offz = gRandom.Gaus(0.0,0.03)
        width = e/100.0 
        for j in range(10):
            hist.Fill(x + offx + gRandom.Gaus(0.0,width), z + offz + gRandom.Gaus(0.0,width), e*0.2)
            offx +=  gRandom.Gaus(0.0,width*0.1)
            offz +=  gRandom.Gaus(0.0,width*0.1)

    break

for i in range(30):

    x = gRandom.Uniform(-3.0,3.0)
    z = gRandom.Uniform(-20.0,20.0)
    e = gRandom.Uniform(0.0,0.4)

    hist.Fill(x,z,1.0)

    for j in range(100):
            hist.Fill(x + gRandom.Gaus(0.0,0.02), z + gRandom.Gaus(0.0,0.02), e)

    offx = gRandom.Gaus(0.0,0.05)
    offz = gRandom.Gaus(0.0,0.05)
    width = gRandom.Uniform(0.0,0.1)
    for j in range(100):
        hist.Fill(x + offx + gRandom.Gaus(0.0,width), z + offz + gRandom.Gaus(0.0,width), e*0.5)
        offx +=  gRandom.Gaus(0.0,width*0.1)
        offz +=  gRandom.Gaus(0.0,width*0.1)

    break

hist.GetZaxis().SetRangeUser(0.0,10.0)
hist.Draw("COLZ")
gPad.Update()
raw_input("WAIT")
