from ROOT import *
import sys

infile = TFile(sys.argv[1],"READ")
detectorevents.Show(0)

import json
print "Opening", sys.argv[2]
jsonfile = open(sys.argv[2])
data = json.load(jsonfile)
abslengthx = data['WLSABSLENGTH_X']
abslengthy = data['WLSABSLENGTH_Y']

g1 = TGraph()
for i in range(len(abslengthx)):
    if float(abslengthx[i].strip("*eV")) == 0: continue
    if float(abslengthy[i].strip("*m")) == 0: continue

    g1.SetPoint( g1.GetN(), 1000.0*1.2398/float(abslengthx[i].strip("*eV")), exp(-0.1/float(abslengthy[i].strip("*m"))))
g1.Draw("APL")
gPad.Update()
raw_input("WAIT")

created_h = TH1D("created_h","created_h", 200, 100.0, 1000.0)
detected_h = TH1D("detected_h","detected_h", 200, 100.0, 1000.0)

for event in detectorevents:
    
    created_e = event.iso_E / 1E-6 # eV

    if (event.template_wsf_pmt1_E.size() > 0): detected_e = event.template_wsf_pmt1_E[0] / 1E-6 # eV
    else: detected_e = -1.0 

    if detected_e == -1.0: continue
    if created_e == detected_e: continue
    
    #if (
    created_h.Fill(1000.0*1.2398/created_e)
    detected_h.Fill(1000.0*1.2398/detected_e)

detected_h.Scale(1.0/detected_h.GetMaximum())
created_h.Scale(1.0/created_h.GetMaximum())

detected_h.Draw("HIST")
created_h.Draw("SAME HIST")
g1.Draw("SAME PL")
gPad.Update()
raw_input("WAIT")

    


