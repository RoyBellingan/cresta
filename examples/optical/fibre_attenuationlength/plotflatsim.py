from ROOT import *
import sys

infile = TFile(sys.argv[1],"READ")
infile.ls()
detectorevents.Show(0)

countfoil = 0
countlight = 0

hist = TH1D("hist","hist",10,0.0,3000.0)


for event in detectorevents:
    
    allpmts = [
        event.wsf_p50_pmt1_t,
        event.wsf_p49_pmt1_t,
        event.wsf_p48_pmt1_t,
        event.wsf_p47_pmt1_t,
        event.wsf_p46_pmt1_t,
        event.wsf_p45_pmt1_t,
        event.wsf_p44_pmt1_t,
        event.wsf_p43_pmt1_t,
        event.wsf_p42_pmt1_t,
        event.wsf_p41_pmt1_t,
        event.wsf_p40_pmt1_t,
        event.wsf_p39_pmt1_t,
        event.wsf_p38_pmt1_t,
        event.wsf_p37_pmt1_t,
        event.wsf_p36_pmt1_t,
        event.wsf_p35_pmt1_t,
        event.wsf_p34_pmt1_t,
        event.wsf_p33_pmt1_t,
        event.wsf_p32_pmt1_t,
        event.wsf_p31_pmt1_t,
        event.wsf_p30_pmt1_t,
        event.wsf_p29_pmt1_t,
        event.wsf_p28_pmt1_t,
        event.wsf_p27_pmt1_t,
        event.wsf_p26_pmt1_t,
        event.wsf_p25_pmt1_t,
        event.wsf_p24_pmt1_t,
        event.wsf_p23_pmt1_t,
        event.wsf_p22_pmt1_t,
        event.wsf_p21_pmt1_t,
        event.wsf_p20_pmt1_t,
        event.wsf_p19_pmt1_t,
        event.wsf_p18_pmt1_t,
        event.wsf_p17_pmt1_t,
        event.wsf_p16_pmt1_t,
        event.wsf_p15_pmt1_t,
        event.wsf_p14_pmt1_t,
        event.wsf_p13_pmt1_t,
        event.wsf_p12_pmt1_t,
        event.wsf_p11_pmt1_t,
        event.wsf_p10_pmt1_t,
        event.wsf_p9_pmt1_t,
        event.wsf_p8_pmt1_t,
        event.wsf_p7_pmt1_t,
        event.wsf_p6_pmt1_t,
        event.wsf_p5_pmt1_t,
        event.wsf_p4_pmt1_t,
        event.wsf_p3_pmt1_t,
        event.wsf_p2_pmt1_t,
        event.wsf_p1_pmt1_t
        ]

    nphotons = 0
    for pmt in allpmts:
        for i in range(pmt.size()):
            if pmt[i] > 0: nphotons += 1

            #print nphotons


    if nphotons > 100 and event.foil_bottom_t > 0: countlight += 1
    if event.foil_bottom_t > 0: countfoil += 1

    if nphotons > 0:
        print nphotons, event.foil_bottom_t

    hist.Fill(nphotons)

print countlight, countfoil

hist.Draw("HIST")
gPad.Update()
raw_input("WAIT")
