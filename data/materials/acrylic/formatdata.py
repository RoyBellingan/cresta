import sys
from ROOT import *
from math import log

transmissiondata = TGraph()

for line in open(sys.argv[1],"r"):
    xval = float(line.strip().split(",")[0])
    yval = float(line.strip().split(",")[1])

    transmissiondata.SetPoint(transmissiondata.GetN(), xval, yval)

n = transmissiondata.GetN()
minx = transmissiondata.GetX()[0]
maxx = transmissiondata.GetX()[n-1]
miny = transmissiondata.GetY()[0]
maxy = transmissiondata.GetY()[n-1]


#transmissiondata.Draw("APL")
#gPad.Update()
#raw_input("WAIT")

hist1 = TH1D("hist","hist",200,0.5,10.0)

for i in range(hist1.GetNbinsX()):
    c = hist1.GetXaxis().GetBinCenter(i+1)

    x = 1239.84193/c

    if x < minx: x = minx
    if x > maxx: x = maxx

    eval = transmissiondata.Eval(x)

    if eval <= 0: eval = 0.000001
    if eval > 100: eval = 100.0

    #print c, x, eval
    t = eval / (maxy*1.001)  # All curves give 95 regardless of thickness. Possible due to surface reflection, so removing.
    d = 3.175

    abslength = - d / log(t)
 
    hist1.SetBinContent(i+1,abslength)

#hist1.Draw("HIST")
#gPad.SetLogy(1)
#gPad.Update()
#raw_input("WAIT")

x = []
y = []
r = []
for i in range(hist1.GetNbinsX()):
    x.append( hist1.GetXaxis().GetBinCenter(i+1) )
    y.append( hist1.GetBinContent(i+1) )
    r.append( 1.502 )

print "PROPERTIES_X:[",
for i in xrange(len(x)):
    if i != 0: print ',',
    print '"' + str(x[i]) + '*eV"',
print "]"

print "ABSLENGTH_Y:[",
for i in xrange(len(y)):
    if i != 0: print ',',
    print '"' + str(y[i]) + '*mm"',
print "]"
    
print "RINDEX_Y:[",
for i in xrange(len(y)):
    if i != 0: print ',',
    print '"' + str(r[i]) + '"',
print "]"
