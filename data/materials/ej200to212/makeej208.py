import sys
from ROOT import *
from math import log

emissiondata = TGraph()

for line in open("ej208_emission.txt","r"):
    xval = float(line.strip().split(",")[0])
    yval = float(line.strip().split(",")[1])

    emissiondata.SetPoint(emissiondata.GetN(), xval, yval)

emissiondata.Draw("APL")
gPad.Update()

hist1 = TH1D("hist","hist",600,100.0,700.0)
hist2 = TH1D("hist2","hist2",600,100.0,700.0)

for i in range(hist1.GetNbinsX()):
    x = hist1.GetXaxis().GetBinCenter(i+1)

    eval = emissiondata.Eval(x)

    if eval < 0: eval = 0.0
    if eval > 10000: eval = 10000

    hist1.SetBinContent(i+1,eval)

hist1.Draw("HIST")
gPad.Update()
#raw_input("WAIT")

hist1.Scale(1.0/hist1.Integral())

EMISSION_X = []
EMISSION_Y = []

for i in range(hist1.GetNbinsX()):
    x = hist1.GetXaxis().GetBinCenter(i+1)

    ev = 1.2398 / (x/1000.0)
    emission = round(hist1.GetBinContent(i+1),4)
    
    if (emission > 0):
        EMISSION_X.append(str(x) + "*eV")
        EMISSION_Y.append(str(emission))

EMISSION_Y = [x for _,x in sorted(zip(EMISSION_X,EMISSION_Y))]
EMISSION_X = [x for _,x in sorted(zip(EMISSION_X,EMISSION_X))]


print "\{"

print "name: \"MATERIAL\""
print "index: \"EJ208\""

print "color: [0.05,0.1,1.0,0.6]"
print "drawstyle: \"solid\""

print "density: \"1.023*g/cm3\""
print "element_names: [\"H\",\"C\"]"
print "element_counts: [0.085,0.915]"

print "FASTCOMPONENT_X :", EMISSION_X
print "FASTCOMPONENT_Y :", EMISSION_Y

print "SLOWCOMPONENT_X :", EMISSION_X
print "SLOWCOMPONENT_Y :", EMISSION_Y

print "FASTTIMECONSTANT : \"3.3*ns\""
print "SLOWTIMECONSTANT : \"4.2*ns\""
print "YIELDRATIO : 1"
print "RESOLUTIONSCALE : 1.0"
print "SCINTILLATIONYIELD : \"9200./MeV\""
print "ABSLENGTH_X : [\"1.0*eV\",\"6.0*eV\"]"
print "ABSLENGTH_Y : [\"4000*mm\",\"4000*mm\"]"

print "RINDEX_X: [\"1.0*eV\",\"6.0*eV\"]"
print "RINDEX_Y: [\"1.58\",\"1.58\"]"

print "}"
