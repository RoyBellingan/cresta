import sys
from ROOT import *
from math import log

absorptiondata = TGraph()
emissiondata = TGraph()

for line in open(sys.argv[1],"r"):
    xval = float(line.strip().split(",")[0])
    yval = float(line.strip().split(",")[1])

    absorptiondata.SetPoint(absorptiondata.GetN(), xval, yval)

for line in open(sys.argv[2],"r"):
    xval = float(line.strip().split(",")[0])
    yval = float(line.strip().split(",")[1])

    emissiondata.SetPoint(emissiondata.GetN(), xval, yval)

#print "GOT ABS DATA"
#raw_input("WAIT")

absorptiondata.Draw("APL")
emissiondata.Draw("SAME PL")
gPad.Update()
#raw_input("WAIT")

hist1 = TH1D("hist","hist",600,100.0,700.0)
hist2 = TH1D("hist2","hist2",600,100.0,700.0)

for i in range(hist1.GetNbinsX()):
    x = hist1.GetXaxis().GetBinCenter(i+1)

    aval = absorptiondata.Eval(x)
    eval = emissiondata.Eval(x)

    if aval <= 0: aval = 0.00025
    if eval < 0: eval = 0.0
    if aval > 1: aval = 1
    if eval > 1: eval = 1

    aval = 1.0/aval
    if aval > 400: aval = 4000
    if x < 300 or x > 480: aval = 4000
    #print x, aval
    hist1.SetBinContent(i+1,aval)
    hist2.SetBinContent(i+1,eval)

#print "Drawing"
hist1.Draw("HIST")
#hist2.Draw("SAME HIST")
gPad.Update()
#raw_input("WAIT")

# Required Format
#PROPERTIES_X: ["1*eV","2*eV","3*eV"]
#WLSABSLENGTH_Y: ["1000*cm","500*cm","0.5*cm"]
#WLSCOMPONENT_Y: ["1E3","0.5E3","0E3"]

PROPERTIES_X = []

WLSABSLENGTH_X = []
WLSABSLENGTH_Y = []

WLSCOMPONENT_X = []
WLSCOMPONENT_Y = []

RINDEX_X = []
RINDEX_Y = []

average = 400
calcavg = 0.0
calccnt = 0.0
for i in range(hist1.GetNbinsX()):
    if (hist1.GetBinContent(i+1) > 0): 
        calcavg += hist1.GetBinContent(i+1)
        calccnt += 1
calcavg /= float(calccnt)
scaler = float(average)/calcavg

for i in range(hist1.GetNbinsX()):
    x = hist1.GetXaxis().GetBinCenter(i+1)

    ev = 1.2398 / (x/1000.0)

    absprop = round(hist1.GetBinContent(i+1),4)
    
    PROPERTIES_X.append( str(round(ev,4)) + "*eV" )
    
    RINDEX_X.append( str(round(ev,4)) + "*eV" )
    RINDEX_Y.append( 1.58 )

    if (absprop != 4000):
        WLSABSLENGTH_X.append( str(round(ev,4)) + "*eV" )
        WLSABSLENGTH_Y.append( str(absprop) + "*mm" )
        
    if round(hist2.GetBinContent(i+1),4) > 0:
        WLSCOMPONENT_X.append( str(round(ev,4)) + "*eV" )
        WLSCOMPONENT_Y.append( str(round(hist2.GetBinContent(i+1), 4)) )


WLSABSLENGTH_Y = [x for _,x in sorted(zip(WLSABSLENGTH_X,WLSABSLENGTH_Y))]
WLSABSLENGTH_X = [x for _,x in sorted(zip(WLSABSLENGTH_X,WLSABSLENGTH_X))]

WLSCOMPONENT_Y = [x for _,x in sorted(zip(WLSCOMPONENT_X,WLSCOMPONENT_Y))]
WLSCOMPONENT_X = [x for _,x in sorted(zip(WLSCOMPONENT_X,WLSCOMPONENT_X))]

RINDEX_X = [x for _,x in sorted(zip(RINDEX_X,RINDEX_Y))]
RINDEX_X = [x for _,x in sorted(zip(RINDEX_X,RINDEX_X))]


print "WLSCOMPONENT_X :",WLSCOMPONENT_X
print "WLSCOMPONENT_Y :",WLSCOMPONENT_Y

print "WLSABSLENGTH_X :",WLSABSLENGTH_X
print "WLSABSLENGTH_Y :",WLSABSLENGTH_Y

print "RINDEX_X :",RINDEX_X
print "RINDEX_Y :",RINDEX_Y
