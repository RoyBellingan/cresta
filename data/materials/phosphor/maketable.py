import sys
from ROOT import *
from math import log

emissiondata = TGraph()

for line in open(sys.argv[1],"r"):
    xval = float(line.strip().split(",")[0])
    yval = float(line.strip().split(",")[1])

    emissiondata.SetPoint(emissiondata.GetN(), xval, yval)

emissiondata.Draw("APL")
gPad.Update()

hist1 = TH1D("hist","hist",600,100.0,700.0)
hist2 = TH1D("hist2","hist2",600,100.0,700.0)

for i in range(hist1.GetNbinsX()):
    x = hist1.GetXaxis().GetBinCenter(i+1)

    eval = emissiondata.Eval(x)

    if eval < 0: eval = 0.0
    if eval > 10000: eval = 10000

    hist1.SetBinContent(i+1,eval)

hist1.Draw("HIST")
gPad.Update()
#raw_input("WAIT")

hist1.Scale(1.0/hist1.Integral())

EMISSION_X = []
EMISSION_Y = []

for i in range(hist1.GetNbinsX()):
    x = hist1.GetXaxis().GetBinCenter(i+1)

    ev = (1.2398 / (x/1000.0))
    emission = round(hist1.GetBinContent(i+1),4)
    
    if (emission > 0):
        EMISSION_X.append(str(ev) + "*eV")
        EMISSION_Y.append(str(emission))

EMISSION_Y = [x for _,x in sorted(zip(EMISSION_X,EMISSION_Y))]
EMISSION_X = [x for _,x in sorted(zip(EMISSION_X,EMISSION_X))]

print "FASTCOMPONENT_X :", EMISSION_X
print "FASTCOMPONENT_Y :", EMISSION_Y

print "SLOWCOMPONENT_X :", EMISSION_X
print "SLOWCOMPONENT_Y :", EMISSION_Y

print "FASTTIMECONSTANT : \"100*ns\""
print "SLOWTIMECONSTANT : \"2000*ns\""
print "YIELDRATIO : 1"
print "RESOLUTIONSCALE : 1.0"
print "SCINTILLATIONYIELD : \"47000./MeV\""
print "ABSLENGTH_X : [\"1.0*eV\",\"6.0*eV\"]"
print "ABSLENGTH_Y : [\"0.15*mm\",\"0.15*mm\"]"

print "RINDEX_X: [\"1.0*eV\",\"6.0*eV\"]"
print "RINDEX_Y: [\"2.36\",\"2.36\"]"




