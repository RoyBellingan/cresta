#!/bin/sh                                                                                                                                                                                            
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $DIR/4.10.05-install/bin/geant4.sh
export G4MAKE_MODULES="-DCMAKE_MODULE_PATH=$DIR/4.10.05-install/lib64/Geant4-10.4.2/Modules/"
export G4LIB="$DIR/4.10.05-install/lib64/"
export G4MAKE="-DGeant4_DIR=$G4LIB"
alias g4make="cmake $G4MAKE $G4MAKE_MODULES CMAKE_CXX_STANDARD=11 "
alias g4rm="rm CMakeCache.txt && rm -r CMakeFiles/ && rm cmake_install.cmake && rm Makefile"
alias g4build="g4rm; g4make"