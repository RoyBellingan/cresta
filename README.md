# CRESTA: Cosmic Rays for Engineering, Scientifc, and Technology Applications  

## Description

CRESTA is an application built upon the GEANT4 framework, that allows the simulation and calculation of particle detector systems 


## Pre-requisites



## Installation 

Install the software via the usual clone, build dir, build chain via cmake i.e.  

* Clone the repository

**git clone https://gitlab.com/geoptic/cresta** 

* Change directory to the CRESTA base directory 

**cd cresta** 
 
* Make the build directory 

**mkdir build**

* Get the cmake files from the source directory

**cmake ../**

cmake ../ -DCMAKE_BUILD_TYPE=DEBUG -DCMAKE_PREFIX_PATH=~/geant/install-v11.1.1/lib64/ ~/geant/cresta -DCMAKE_CXX_STANDARD=23 -DUSE_PUMAS=0 -DGeant4_DIR=/home/roy/geant/install-v11.1.1/lib64/cmake/Geant4/

Patch in local root https://github.com/root-project/root/issues/13042

* Build (assuming there's no errors above)









Cosmic Generator
ssds
